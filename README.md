# README pour le Projet Open-Xchange Infra as Code

Les fichiers de configurations et les secrets pour la DiNum sont [ici](https://gitlab.mim-libre.fr/dimail/dimail-infra-config)

## SOMMAIRE

1. [Introduction](#introduction)
2. [Arborescence du Projet](#arborescence-du-projet)
3. [Accès et Configuration](#accès-et-configuration)
4. [Déploiement et Configuration](#déploiement-et-configuration)
   1. [Déploiement Terraform](#déploiement-terraform)
   2. [Installation et Configuration Ansible](#installation-et-configuration-ansible)
5. [Environnements](#environnements)
6. [Liens Utiles](#liens-utiles)

## Introduction

Open-Xchange est une suite logicielle libre dédiée à la messagerie collaborative sous Linux, incluant des fonctionnalités telles que la gestion des e-mails, des calendriers et des disques virtuels. Ce projet utilise Terraform et Ansible pour déployer et configurer l'infrastructure nécessaire à Open-Xchange.

## Arborescence du Projet

- **10_backend_ovh**: Déploiement du backend Terraform (S3 bucket).
- **20_network**: Éléments réseau "durables" (IPS, A record DNS).
- **30_vms**: Infrastructure réseau et machines.
- **40_ansible_single**: Installation et configuration du serveur mail Open-Xchange (infrastructure single server).
- **Makefile**: Commandes Terraform selon l'environnement sourcé.
- **.editorconfig**: Configuration pour l'édition des fichiers du repo.

## Accès et Configuration

> La documentation sur les accès est [ici](./docs/admin/docs/access)

## Déploiement et Configuration

### Déploiement Terraform

- **Init**: `make init`
- **Plan**: `make plan`
- **Apply**:
  - `make apply_backend`
  - `make apply_network`
  - `make apply_vms`
- **Autres Commandes**: `make check` vérifie l'environnement avant toute opération.

### Installation et Configuration Ansible

Utilisez le dossier `40_ansible_single` pour lancer les playbooks Ansible :

```bash
# Il est important de noter que, pour lancer le playbook Ansible, il faut un plan Terraform à jour.
ansible-playbook playbook.yml
```

## Environnements

Pour se positionner dans l'environnement de développement :
- Lancer `. ./$FICHIER_ENV_DEV` puis `make init`

Pour se positionner dans l'environnement de production :
- Lancer `. ./$FICHIER_ENV_PROD` puis `make init`


## Rôles ansible

### cert_http

#### Description

Ce rôle manipule les configurations du proxy http (Nginx en règle général) afin que les challenges HTTP Let's Encrypt puissent fonctionner avec des sous-domaines qui pointent sur les machines cibles, en coordination avec le cert_manager.

#### Dépendance

Ce rôle fait appel au rôle :
  * `nginx_server`

### cert_manager

#### Description

Ce rôle permet de centraliser les certificats dans un serveur et d'ouvrir une configuration `rsync` en mode serveur pour que ces derniers se synchronisent dans les machines clientes. 

#### Dépendance

Ce rôle fait appel au rôle :
* `rsync_server`

### cert_user

#### Description

Ce rôle permet de configurer un cron qui récupère régulièrement les certificats produits par `cert_manager`.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### db_server

#### Description

Ce rôle permet de créer et de configurer le serveur de bases de données MariaDB sur la cible (primary + replica).

#### Dépendance

Ce rôle fait appel au rôle :
* `rclone_access`

### icinga

#### Description

Ce rôle permet l'installation et la configuration de l'outil `icinga` afin de superviser le système.
#### Dépendance

Ce rôle fait appel au rôle :
* `nginx_server`

### imap_master

#### Description

Ce rôle permet de centraliser les configurations `imap` dans un serveur et d'ouvrir une configuration `rsync` en mode serveur pour que ces derniers se synchronisent dans les machines clientes.


#### Dépendance

Ce rôle fait appel au rôle :
* `rsync_server`

### imap_server

#### Description

Ce rôle permet l'installation et la configuration de dovecot.

#### Dépendance

Ce rôle fait appel au rôle :
* `nginx_server`
* `rclone_access`

### imap_slave

#### Description

Ce rôle permet de configurer la réplication par rsync+cron des données des mails sur le serveur de backup. 

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### nginx_server

#### Description

Ce rôle installe et configure `nginx` proxy http.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### ox_server

#### Description

Ce rôle permet l'installation et la configuration d'OpenXchange.

#### Dépendance

Ce rôle fait appel au rôle :
* `nginx_server`

### prometheus

#### Description

Ce rôle installe et configure `Prometheus` pour monitorer le système. 
  * Si le serveur a un rôle de monitoring, il installera la partie serveur
  * Sinon, il installera les exporters.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### rclone_access

#### Description

Ce rôle installe et configure `rclone` pour avoir accès au bucket S3.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### rsync_server

#### Description

Ce rôle installe `rsync` et configure ce dernier en attente d'autres configurations.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### secure_vms

#### Description

Ce rôle renforce la sécurité des serveurs avec différents réglages, sur le système et OpenSSH notamment.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### setup_disk

#### Description

Ce rôle s'assure que les volumes (raw disk) sont montés sur les bons chemins.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### setup_logs

#### Description

Ce rôle configure les dossiers de `logs` des systèmes.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### setup_repo

#### Description

Ce rôle configure les repositories Debian nécessaires aux installations de dépendances.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### setup_systeme

#### Description

Ce rôle permet d'installer et de configurer les logiciels requis pour le bon fonctionnement du système.

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.

### smtp_server

#### Description

Ce rôle installe et configure postfix.

#### Dépendance

Ce rôle fait appel au rôle :
* `nginx_server`

### test_data

#### Description

Ce rôle teste les données présentes sur le serveur. 

#### Dépendance

Ce rôle fait appel à aucun autre rôle ni collection.


## Liens Utiles

### Liens pratiques pour l'infra :
- [OVH Cloud](https://www.ovh.com/manager/#/hub)
- [OVH Horizon (Openstack)](https://horizon.cloud.ovh.net/auth/login/)
- [Doc Terraform OVH](https://registry.terraform.io/providers/ovh/ovh/latest/docs)
- [Doc Terraform Openstack](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs?ref=breadnet.co.uk)

### Documentation Open-Xchange :

- [Documentation technique des process OX](https://documentation.open-xchange.com/)
- [Tutoriel pour construire une architecture OX single server sur Debian 10](https://oxpedia.org/wiki/index.php?title=AppSuite:Open-Xchange_Installation_Guide_for_Debian_10.0)
- [Tutoriel pour construire une architecture OX pour 10K users](https://oxpedia.org/wiki/index.php?title=OX_HE_Tutorial_10K)
- [Tutoriel pour construire une architecture pour 100K users](https://oxpedia.org/wiki/index.php?title=AppSuite:OX_Tutorial_100K#Tutorial:_High_Available_OX_AppSuite_Setup_for_up_to_100.000_users)

### Autres liens utiles :
- [Mail tester (pour tester la note attribuée aux mails envoyés)](https://www.mail-tester.com/)
