# Null ressources to triger replacement on DNS records when changing the zone
resource "null_resource" "trigger_zone" {
  for_each  = { "host": var.host_domain, "tech": var.tech_domain }
  triggers = {
    key = each.value
  }
}

# Le domaine host
# Associé à l'IP publique, et au nom court de la machine (backup)

resource "ovh_domain_zone_record" "host_A" {
  for_each  = { for key, value in local.servers : "${value.hostname}" => value }
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = each.value.hostname
  fieldtype = "A"
  ttl       = var.ttl
  target    = each.value.public_ip
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

# Le domaine host.priv
# Associé à l'IP privée secondaire (172.21.x.x, celle qui est cross-région), et au nom
# court de la machine (backup)

resource "ovh_domain_zone_record" "private_host_A" {
  for_each  = { for key, value in local.servers : "${key}-${local.env_suffix}" => value }
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "${each.value.hostname}.priv"
  fieldtype = "A"
  ttl       = var.ttl
  target    = openstack_networking_port_v2.backnet[each.key].all_fixed_ips[0]
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

# Le domain priv.host
# Associé à l'IP privée primaire (192.168.x.x), et au nom complet de la machine (backup-single-dev)

resource "ovh_domain_zone_record" "priv_host_A" {
  for_each  = { for key, value in openstack_compute_instance_v2.vm : "${value.name}" => value }
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = each.value.name
  fieldtype = "A"
  ttl       = var.ttl
  target    = each.value.access_ip_v4
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

# Les records smtp.priv.host

locals {
  roles = toset(flatten([for name, server in var.servers : server.roles ]))
  servers_by_role = {
    for role in local.roles:
       role => [for name, server in openstack_compute_instance_v2.vm: name if contains(server.tags, "role-${role}")]
  }
}

resource "ovh_domain_zone_record" "priv_smtp_A" {
  for_each  = toset([ for server_name in  (can(local.servers_by_role["smtp"]) ? local.servers_by_role["smtp"] : []): 
    server_name if !contains(can(local.servers_by_role["backup_role"]) ? local.servers_by_role["backup_role"] : [], server_name) 
  ])
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "smtp.priv"
  fieldtype = "A"
  ttl       = var.ttl
  target    = openstack_networking_port_v2.backnet[each.value].all_fixed_ips[0]
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

# les records spam.priv.host
resource "ovh_domain_zone_record" "priv_spam_A" {
  for_each  = toset([ for server_name in  (can(local.servers_by_role["mail_filter"]) ? local.servers_by_role["mail_filter"] : []): server_name ])
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "spam.priv"
  fieldtype = "A"
  ttl       = var.ttl
  target    = openstack_networking_port_v2.backnet[each.value].all_fixed_ips[0]
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

# Le domaine technique

locals {
  tech_names_per_role = {
    "smtp"       = ["smtp","mx"]
    "imap"       = ["imap"]
    "webfront"   = ["webmail"]
  }
  records_per_server_tech = flatten([
    for name, server in local.servers:
      [ for role in (contains(server.roles,"backup_role") ? [] : server.roles):
        [ for a_name in can(local.tech_names_per_role[role]) ? local.tech_names_per_role[role] : []:
          {
            name      = "${server.hostname}-${a_name}"
            public_ip = server.public_ip
            subdomain = a_name
          }
        ]
      ]
  ])
  host_names_per_role = {
    "webfront"   = ["api","auth"]
  }
  records_per_server_host = flatten([
    for name, server in local.servers:
      [ for role in (contains(server.roles,"backup_role") ? [] : server.roles):
        [ for a_name in can(local.host_names_per_role[role]) ? local.host_names_per_role[role] : []:
          {
            name      = "${server.hostname}-${a_name}"
            public_ip = server.public_ip
            subdomain = a_name
          }
        ]
      ]
  ])
}

resource "ovh_domain_zone_record" "technical_A_record" {
  for_each  = {for item in local.records_per_server_tech: item.name => item }
  zone      = data.ovh_domain_zone.tech_domain.name
  subdomain = each.value.subdomain
  fieldtype = "A"
  ttl       = var.ttl
  target    = each.value.public_ip
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["tech"]
    ]
  }
}

resource "ovh_domain_zone_record" "host_A_record" {
  for_each  = {for item in local.records_per_server_host: item.name => item }
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = each.value.subdomain
  fieldtype = "A"
  ttl       = var.ttl
  target    = each.value.public_ip
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

# SPF Configuration pour le domaine technique

resource "ovh_domain_zone_record" "technical_SPF_include" {
  zone      = data.ovh_domain_zone.tech_domain.name
  subdomain = "_spf"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = "\"v=spf1 ip4:${data.openstack_networking_floatingip_v2.main_ip.address} ip4:${data.openstack_networking_floatingip_v2.backup_ip.address} -all\""
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["tech"]
    ]
  }
}


# On traite testing.<host_domain> comme un domaine client, avec webmail

resource "ovh_domain_zone_record" "client_testing_CNAME_record" {
  for_each  = toset(["imap", "smtp", "webmail"])
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "${each.value}.testing"
  fieldtype = "CNAME"
  ttl       = var.ttl
  target    = "${each.value}.${data.ovh_domain_zone.tech_domain.name}."
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

resource "ovh_domain_zone_record" "client_testing_MX_record" {
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "testing"
  fieldtype = "MX"
  ttl       = var.ttl
  target    = "10 mx.${data.ovh_domain_zone.tech_domain.name}."
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

resource "ovh_domain_zone_record" "client_testing_SPF_record" {
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "testing"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = "\"v=spf1 include:_spf.${data.ovh_domain_zone.tech_domain.name} -all\""
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

data "local_file" "dkim_testing" {
  filename = "${var.config_path}/assets/dkim_keys/testing.${var.host_domain}_ox2024.txt"
}

locals {
  raw_dkim_testing = data.local_file.dkim_testing.content
  strings_dkim_testing = flatten(regexall("\"(.*?)\"", local.raw_dkim_testing))
  dkim_testing = join("", local.strings_dkim_testing)
}

resource "ovh_domain_zone_record" "client_testing_DKIM_record" {
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "ox2024._domainkey.testing"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = local.dkim_testing
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

# On traite system.<host_domain> comme un domaine client, sans webmail

resource "ovh_domain_zone_record" "client_system_CNAME_record" {
  for_each  = toset(["imap", "smtp"])
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "${each.value}.system"
  fieldtype = "CNAME"
  ttl       = var.ttl
  target    = "${each.value}.${data.ovh_domain_zone.tech_domain.name}."
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

resource "ovh_domain_zone_record" "client_system_MX_record" {
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "system"
  fieldtype = "MX"
  ttl       = var.ttl
  target    = "10 mx.${data.ovh_domain_zone.tech_domain.name}."
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

resource "ovh_domain_zone_record" "client_system_SPF_record" {
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "system"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = "\"v=spf1 include:_spf.${data.ovh_domain_zone.tech_domain.name} -all\""
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}

data "local_file" "dkim_system" {
  filename = "${var.config_path}/assets/dkim_keys/system.${var.host_domain}_ox2024.txt" 
}

locals {
  raw_dkim_system = data.local_file.dkim_system.content
  strings_dkim_system = flatten(regexall("\"(.*?)\"", local.raw_dkim_system))
  dkim_system = join("", local.strings_dkim_system)
}

resource "ovh_domain_zone_record" "client_system_DKIM_record" {
  zone      = data.ovh_domain_zone.host_domain.name
  subdomain = "ox2024._domainkey.system"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = local.dkim_system
  lifecycle {
    replace_triggered_by = [
      null_resource.trigger_zone["host"]
    ]
  }
}
