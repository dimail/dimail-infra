locals {
  architecture = "mecol"
  type         = "single"
  env_suffix   = "${local.type}-${var.env_name}"
  regions      = [
    for region, name in var.regions:
      name
  ]
  images = {
    "bookworm" = "Debian 12"
  }
  public_ips = {
    "ox-app"  = data.openstack_networking_floatingip_v2.main_ip.address
    "backup"  = data.openstack_networking_floatingip_v2.backup_ip.address
    "monitor" = data.openstack_networking_floatingip_v2.monitor_ip.address
    "ldap"    = data.openstack_networking_floatingip_v2.ldap_ip.address
  }

  servers = {
    for name, server in var.servers :
    name => {
      image = server.image
      dimensionnement = server.dimensionnement
      public_ip = local.public_ips[name]
      sql_server_id = can(server.sql_server_id) ? server.sql_server_id : ""
      ox_name = can(server.ox_name) ? server.ox_name : ""
      ox_store = can(server.ox_store) ? server.ox_store : ""
      ox_params = can(server.ox_params) ? server.ox_params : []
      hostname = server.hostname
      region = var.regions[server.region]
      roles = server.roles
      volumes = server.volumes
    }
  }

  volumes = {
    for name, vol in var.volumes :
    name => {
      region = var.regions[vol.region]
      mount  = vol.mount
      size   = vol.size
      type   = can(vol.type) ? vol.type : "classic"
    }
  }

  attachments = distinct(flatten([
    for ser_name, server in local.servers : [
      for volume in server.volumes : {
        server_name = ser_name,
        volume_name = volume.name,
        region      = server.region,
      }
    ]
  ]))

  rules_icmp = [
    { # ICMP - allow for all instances on the backend network
      port        = -1
      ip_protocol = "icmp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
    { # ICMP - Allow from monitor
      port        = -1
      ip_protocol = "icmp"
      cidr        = "${local.servers["monitor"].public_ip}/32"
    },
    { # Node exporter, allowed for monitor on the backend network
      port        = 9100
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_ssh = [
    { # ssh
      port        = 22
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # Accept NRPE from monitor (public IP)
      port        = 5666
      ip_protocol = "tcp"
      cidr        = "${local.servers["monitor"].public_ip}/32"
    },
    { # Accept NRPE from all (private IP)
      port        = 5666
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_rsync = [
    { # rsync
      port        = 873
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    }
  ]
  rules_ox = [
    { # http
      port        = 80
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # https
      port        = 443
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # OX appsuite
      port        = 8009
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_imap = [
    { # imap
      port        = 143
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # imaps
      port        = 993
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # pop3
      port        = 110
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # pop3s
      port        = 995
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # manage sieve
      port        = 4190
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # dovecot prom exporter
      port        = 9900
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_smtp = [
    { # smtp
      port        = 25
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # submission
      port        = 465
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # STARTTLS
      port        = 587
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # postfix prom exporter
      port        = 9154
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_sqlmaster = [
    {
      port        = 3306
      ip_protocol = "tcp"
      # Should be the list of the public IPs of all the servers having the role "sqlslave"
      cidr = "${local.servers["backup"].public_ip}/32"
    },
    { # for monitoring
      port        = 3306
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
    { # mysql prom exporter
      port        = 9306
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_sqlslave = [
    { # for monitoring
      port        = 3306
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
    { # mysql prom exporter
      port        = 9306
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_monitor = [
    { # http
      port        = 80
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # https
      port        = 443
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # nginx prom exporter
      port        = 9080
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_ldap = [
    {
      port        = 443
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    {
      port        = 80
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # LDAP
      port        = 389
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # LDAPs
      port        = 636
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # nginx prom exporter
      port        = 9080
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_webfront = [
    {
      port        = 443
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    {
      port        = 80
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    },
    { # nginx prom exporter
      port        = 9080
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_api = [
    {
      port        = 8000
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  rules_mail_filter = [
    {
      port        = 783
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
    {
      port        = 10024
      ip_protocol = "tcp"
      cidr        = data.openstack_networking_subnet_v2.backnet[var.regions["main"]].cidr
    },
  ]
  vm_secgroup_rules = {
    icmp      = local.rules_icmp
    smtp      = local.rules_smtp
    imap      = local.rules_imap
    main      = local.rules_ox
    bastion   = local.rules_ssh
    certs     = local.rules_rsync
    sql_master = local.rules_sqlmaster
    sql_slave  = local.rules_sqlslave
    monitor   = local.rules_monitor
    ldap      = local.rules_ldap
    webfront  = local.rules_webfront
    api       = local.rules_api
    mail_filter = local.rules_mail_filter
    backup_role = local.rules_ox
    ox = []
  }
}
