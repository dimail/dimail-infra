# DEV

# module.single.data.openstack_compute_flavor_v2.size["normal-GRA7"]
# module.single.data.openstack_compute_flavor_v2.size["normal-SBG7"]
# module.single.data.openstack_images_image_v2.debian["bookworm-GRA7"]
# module.single.data.openstack_images_image_v2.debian["bookworm-SBG7"]
# module.single.data.openstack_images_image_v2.debian["bullseye-GRA7"]
# module.single.data.openstack_images_image_v2.debian["bullseye-SBG7"]
# module.single.data.openstack_images_image_v2.debian["buster-GRA7"]
# module.single.data.openstack_images_image_v2.debian["buster-SBG7"]
# module.single.data.openstack_networking_floatingip_v2.backup_ip
# module.single.data.openstack_networking_floatingip_v2.main_ip
# module.single.data.openstack_networking_floatingip_v2.monitor_ip
# module.single.data.openstack_networking_network_v2.backnet["GRA7"]
# module.single.data.openstack_networking_network_v2.backnet["SBG7"]
# module.single.data.openstack_networking_network_v2.ext-net["GRA7"]
# module.single.data.openstack_networking_network_v2.ext-net["SBG7"]
# module.single.data.openstack_networking_network_v2.frontnet["GRA7"]
# module.single.data.openstack_networking_network_v2.frontnet["SBG7"]
# module.single.data.openstack_networking_subnet_v2.backnet["GRA7"]
# module.single.data.openstack_networking_subnet_v2.backnet["SBG7"]
# module.single.openstack_compute_keypair_v2.chrnin_keypair["GRA7"]
# module.single.openstack_compute_keypair_v2.chrnin_keypair["SBG7"]
# module.single.openstack_compute_keypair_v2.dumo_keypair["GRA7"]
# module.single.openstack_compute_keypair_v2.dumo_keypair["SBG7"]
# module.single.openstack_compute_keypair_v2.geo_keypair["GRA7"]
# module.single.openstack_compute_keypair_v2.geo_keypair["SBG7"]
# module.single.openstack_compute_keypair_v2.jo_keypair_1["GRA7"]
# module.single.openstack_compute_keypair_v2.jo_keypair_1["SBG7"]
# module.single.openstack_compute_keypair_v2.jo_keypair_2["GRA7"]
# module.single.openstack_compute_keypair_v2.jo_keypair_2["SBG7"]
# module.single.openstack_compute_keypair_v2.jo_keypair_3["GRA7"]
# module.single.openstack_compute_keypair_v2.jo_keypair_3["SBG7"]
# module.single.openstack_compute_keypair_v2.jo_keypair_4["GRA7"]
# module.single.openstack_compute_keypair_v2.jo_keypair_4["SBG7"]
# module.single.openstack_compute_keypair_v2.jopa_keypair["GRA7"]
# module.single.openstack_compute_keypair_v2.jopa_keypair["SBG7"]
# module.single.openstack_compute_keypair_v2.yart_keypair["GRA7"]
# module.single.openstack_compute_keypair_v2.yart_keypair["SBG7"]
# module.single.openstack_compute_keypair_v2.yart_keypair_2["GRA7"]
# module.single.openstack_compute_keypair_v2.yart_keypair_2["SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["backup-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["backup-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["bastion-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["bastion-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["certs-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["certs-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["icmp-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["icmp-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["imap-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["imap-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["main-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["main-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["monitor-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["monitor-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["smtp-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["smtp-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["sqlmaster-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["sqlmaster-SBG7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["sqlslave-GRA7"]
# module.single.openstack_compute_secgroup_v2.vm_secgroup["sqlslave-SBG7"]
# module.single.openstack_networking_router_interface_v2.frontnet["GRA7"]
# module.single.openstack_networking_router_interface_v2.frontnet["SBG7"]
# module.single.openstack_networking_router_v2.frontnet["GRA7"]
# module.single.openstack_networking_router_v2.frontnet["SBG7"]
# module.single.openstack_networking_subnet_v2.frontnet["GRA7"]
# module.single.openstack_networking_subnet_v2.frontnet["SBG7"]
# module.single.ovh_cloud_project_network_private.frontnet["GRA7"]
# module.single.ovh_cloud_project_network_private.frontnet["SBG7"]

moved {
  from = module.single.data.openstack_compute_flavor_v2.size["normal-GRA7"]
  to   = data.openstack_compute_flavor_v2.size["normal-SBG7"]
}

moved {
  from = module.single.data.openstack_compute_flavor_v2.size["normal-SBG7"]
  to   = data.openstack_compute_flavor_v2.size["normal-GRA7"]
}

moved {
  from = module.single.data.openstack_images_image_v2.debian["bookworm-GRA7"]
  to   = data.openstack_images_image_v2.debian["bookworm-SBG7"]
}

moved {
  from = module.single.data.openstack_images_image_v2.debian["bookworm-SBG7"]
  to   = data.openstack_images_image_v2.debian["bookworm-GRA7"]
}

moved {
  from = module.single.data.openstack_images_image_v2.debian["bullseye-GRA7"]
  to   = data.openstack_images_image_v2.debian["bullseye-SBG7"]
}

moved {
  from = module.single.data.openstack_images_image_v2.debian["bullseye-SBG7"]
  to   = data.openstack_images_image_v2.debian["bullseye-GRA7"]
}

moved {
  from = module.single.data.openstack_images_image_v2.debian["buster-GRA7"]
  to   = data.openstack_images_image_v2.debian["buster-SBG7"]
}

moved {
  from = module.single.data.openstack_images_image_v2.debian["buster-SBG7"]
  to   = data.openstack_images_image_v2.debian["buster-GRA7"]
}

moved {
  from = module.single.data.openstack_networking_floatingip_v2.backup_ip
  to   = data.openstack_networking_floatingip_v2.backup_ip
}

moved {
  from = module.single.data.openstack_networking_floatingip_v2.main_ip
  to   = data.openstack_networking_floatingip_v2.main_ip
}

moved {
  from = module.single.data.openstack_networking_floatingip_v2.monitor_ip
  to   = data.openstack_networking_floatingip_v2.monitor_ip
}

moved {
  from = module.single.data.openstack_networking_network_v2.backnet["GRA7"]
  to   = data.openstack_networking_network_v2.backnet["SBG7"]
}

moved {
  from = module.single.data.openstack_networking_network_v2.backnet["SBG7"]
  to   = data.openstack_networking_network_v2.backnet["GRA7"]
}

moved {
  from = module.single.data.openstack_networking_network_v2.ext-net["GRA7"]
  to   = data.openstack_networking_network_v2.ext-net["SBG7"]
}

moved {
  from = module.single.data.openstack_networking_network_v2.ext-net["SBG7"]
  to   = data.openstack_networking_network_v2.ext-net["GRA7"]
}

moved {
  from = module.single.data.openstack_networking_network_v2.frontnet["GRA7"]
  to   = data.openstack_networking_network_v2.frontnet["SBG7"]
}

moved {
  from = module.single.data.openstack_networking_network_v2.frontnet["SBG7"]
  to   = data.openstack_networking_network_v2.frontnet["GRA7"]
}

moved {
  from = module.single.data.openstack_networking_subnet_v2.backnet["GRA7"]
  to   = data.openstack_networking_subnet_v2.backnet["SBG7"]
}

moved {
  from = module.single.data.openstack_networking_subnet_v2.backnet["SBG7"]
  to   = data.openstack_networking_subnet_v2.backnet["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.chrnin_keypair["GRA7"]
  to   = openstack_compute_keypair_v2.chrnin_keypair["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.chrnin_keypair["SBG7"]
  to   = openstack_compute_keypair_v2.chrnin_keypair["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.dumo_keypair["GRA7"]
  to   = openstack_compute_keypair_v2.dumo_keypair["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.dumo_keypair["SBG7"]
  to   = openstack_compute_keypair_v2.dumo_keypair["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.geo_keypair["GRA7"]
  to   = openstack_compute_keypair_v2.geo_keypair["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.geo_keypair["SBG7"]
  to   = openstack_compute_keypair_v2.geo_keypair["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jo_keypair_1["GRA7"]
  to   = openstack_compute_keypair_v2.jo_keypair_1["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jo_keypair_1["SBG7"]
  to   = openstack_compute_keypair_v2.jo_keypair_1["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jo_keypair_2["GRA7"]
  to   = openstack_compute_keypair_v2.jo_keypair_2["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jo_keypair_2["SBG7"]
  to   = openstack_compute_keypair_v2.jo_keypair_2["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jo_keypair_3["GRA7"]
  to   = openstack_compute_keypair_v2.jo_keypair_3["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jo_keypair_3["SBG7"]
  to   = openstack_compute_keypair_v2.jo_keypair_3["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jo_keypair_4["GRA7"]
  to   = openstack_compute_keypair_v2.jo_keypair_4["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jo_keypair_4["SBG7"]
  to   = openstack_compute_keypair_v2.jo_keypair_4["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jopa_keypair["GRA7"]
  to   = openstack_compute_keypair_v2.jopa_keypair["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.jopa_keypair["SBG7"]
  to   = openstack_compute_keypair_v2.jopa_keypair["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.yart_keypair["GRA7"]
  to   = openstack_compute_keypair_v2.yart_keypair["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.yart_keypair["SBG7"]
  to   = openstack_compute_keypair_v2.yart_keypair["SBG7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.yart_keypair_2["GRA7"]
  to   = openstack_compute_keypair_v2.yart_keypair_2["GRA7"]
}

moved {
  from = module.single.openstack_compute_keypair_v2.yart_keypair_2["SBG7"]
  to   = openstack_compute_keypair_v2.yart_keypair_2["SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["backup-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["backup-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["backup-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["backup-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["bastion-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["bastion-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["bastion-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["bastion-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["certs-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["certs-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["certs-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["certs-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["icmp-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["icmp-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["icmp-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["icmp-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["imap-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["imap-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["imap-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["imap-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["main-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["main-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["main-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["main-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["monitor-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["monitor-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["monitor-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["monitor-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["smtp-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["smtp-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["smtp-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["smtp-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["sqlmaster-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["sqlmaster-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["sqlmaster-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["sqlmaster-SBG7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["sqlslave-GRA7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["sqlslave-GRA7"]
}

moved {
  from = module.single.openstack_compute_secgroup_v2.vm_secgroup["sqlslave-SBG7"]
  to   = openstack_compute_secgroup_v2.vm_secgroup["sqlslave-SBG7"]
}

moved {
  from = module.single.openstack_networking_router_interface_v2.frontnet["GRA7"]
  to   = openstack_networking_router_interface_v2.frontnet["GRA7"]
}

moved {
  from = module.single.openstack_networking_router_interface_v2.frontnet["SBG7"]
  to   = openstack_networking_router_interface_v2.frontnet["SBG7"]
}

moved {
  from = module.single.openstack_networking_router_v2.frontnet["GRA7"]
  to   = openstack_networking_router_v2.frontnet["GRA7"]
}

moved {
  from = module.single.openstack_networking_router_v2.frontnet["SBG7"]
  to   = openstack_networking_router_v2.frontnet["SBG7"]
}

moved {
  from = module.single.openstack_networking_subnet_v2.frontnet["GRA7"]
  to   = openstack_networking_subnet_v2.frontnet["GRA7"]
}

moved {
  from = module.single.openstack_networking_subnet_v2.frontnet["SBG7"]
  to   = openstack_networking_subnet_v2.frontnet["SBG7"]
}

moved {
  from = module.single.ovh_cloud_project_network_private.frontnet["GRA7"]
  to   = ovh_cloud_project_network_private.frontnet["GRA7"]
}

moved {
  from = module.single.ovh_cloud_project_network_private.frontnet["SBG7"]
  to   = ovh_cloud_project_network_private.frontnet["SBG7"]
}
