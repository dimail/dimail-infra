# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.5.2"
  hashes = [
    "h1:JlMZD6nYqJ8sSrFfEAH0Vk/SL8WLZRmFaMUF9PJK5wM=",
    "zh:136299545178ce281c56f36965bf91c35407c11897f7082b3b983d86cb79b511",
    "zh:3b4486858aa9cb8163378722b642c57c529b6c64bfbfc9461d940a84cd66ebea",
    "zh:4855ee628ead847741aa4f4fc9bed50cfdbf197f2912775dd9fe7bc43fa077c0",
    "zh:4b8cd2583d1edcac4011caafe8afb7a95e8110a607a1d5fb87d921178074a69b",
    "zh:52084ddaff8c8cd3f9e7bcb7ce4dc1eab00602912c96da43c29b4762dc376038",
    "zh:71562d330d3f92d79b2952ffdda0dad167e952e46200c767dd30c6af8d7c0ed3",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:805f81ade06ff68fa8b908d31892eaed5c180ae031c77ad35f82cb7a74b97cf4",
    "zh:8b6b3ebeaaa8e38dd04e56996abe80db9be6f4c1df75ac3cccc77642899bd464",
    "zh:ad07750576b99248037b897de71113cc19b1a8d0bc235eb99173cc83d0de3b1b",
    "zh:b9f1c3bfadb74068f5c205292badb0661e17ac05eb23bfe8bd809691e4583d0e",
    "zh:cc4cbcd67414fefb111c1bf7ab0bc4beb8c0b553d01719ad17de9a047adff4d1",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.2.3"
  hashes = [
    "h1:+AnORRgFbRO6qqcfaQyeX80W0eX3VmjadjnUFUJTiXo=",
    "zh:22d062e5278d872fe7aed834f5577ba0a5afe34a3bdac2b81f828d8d3e6706d2",
    "zh:23dead00493ad863729495dc212fd6c29b8293e707b055ce5ba21ee453ce552d",
    "zh:28299accf21763ca1ca144d8f660688d7c2ad0b105b7202554ca60b02a3856d3",
    "zh:55c9e8a9ac25a7652df8c51a8a9a422bd67d784061b1de2dc9fe6c3cb4e77f2f",
    "zh:756586535d11698a216291c06b9ed8a5cc6a4ec43eee1ee09ecd5c6a9e297ac1",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9d5eea62fdb587eeb96a8c4d782459f4e6b73baeece4d04b4a40e44faaee9301",
    "zh:a6355f596a3fb8fc85c2fb054ab14e722991533f87f928e7169a486462c74670",
    "zh:b5a65a789cff4ada58a5baffc76cb9767dc26ec6b45c00d2ec8b1b027f6db4ed",
    "zh:db5ab669cf11d0e9f81dc380a6fdfcac437aea3d69109c7aef1a5426639d2d65",
    "zh:de655d251c470197bcbb5ac45d289595295acb8f829f6c781d4a75c8c8b7c7dd",
    "zh:f5c68199f2e6076bce92a12230434782bf768103a427e9bb9abee99b116af7b5",
  ]
}

provider "registry.terraform.io/ovh/ovh" {
  version     = "0.51.0"
  constraints = "~> 0.51.0"
  hashes = [
    "h1:/RwSmGxkUoMeEkX7UqyC/JYA5Fioqt7A5Qvq1NrDgTQ=",
    "h1:7dOFLaLlkGzi+xsxSXV/CzmZP7n1kk/Fi3kiUuqwXAI=",
    "h1:DUVoJDMeVHXRkZIH7KaVGb/LAnI6pYaJsPd28Xa6Smg=",
    "h1:DepnYVFcpMFYViDeU3wvEqjNYSngQ1bbGQXLT6Py9Mw=",
    "h1:F+zEYn8hO9uHgBw5VkNUznHBQuC90pLP7xT3Z7yjkDw=",
    "h1:Q/S8VXeVgTj1KB98HNo0gabAJBxgmVxRLO0jSKFMO4E=",
    "h1:S7r5AFUQ8kl5MgsRNwFekD4qrnYnwVRcdSTeewp3T8Y=",
    "h1:WM9PK2XT3/6RxEADkPOPGCL6by7CO1PawcSLzlwO4HE=",
    "h1:hSJH2YseGHYVoGSrRgdBqwwz6oZVGssnbJ2hjtS/Gqs=",
    "h1:mfUEYqr5G9UW/cobcRneQhUFvJvGNjcZHnHCdF2fAlA=",
    "h1:nj9ug1tGCA4TMj1wYSZc+W8Qyt/DXQLhCbdtWmcc1Xo=",
    "h1:oFnnUFkxcAx3MoJ7uumnudwzVaNP033bG2O9Lh0LbiY=",
    "h1:pzwHQ4/iiNeMbvqeT+Mo8N3C5yiLMobVAP9xAbSiI6g=",
    "h1:qKVyUEhgej/ccqQy1+z+SSjreAYAmFaIRx5ApKweLz0=",
    "zh:13c2e4ea4a0f8348f7556862df5ff17733d06cf898c2a646f0c235e4fa87d210",
    "zh:52db2b0371ffaf52cfdacbf8fa6e931c35b5b2c8a65e8f3cb70155f33beda85f",
    "zh:5824a22dd83699f78a4d7689eb905f895999491debb730ba2d15e0f0082c66b0",
    "zh:67426b082025ccf08685f757cca94fc34b8b930e97d17d896b7a4da38ccad00d",
    "zh:6e7b2a62118c969acef1c867dd97db880398f44243145cd5feb2e1935696d0c9",
    "zh:84248f5e3b53fdd175c5c232fcb5b4a6e59f4d33a161b90d70f38bccae6e2823",
    "zh:937c3d56f32e7aaa2fe05d58fc54ff8c348a8ab3718462dc2ea1f9a9ba739bd9",
    "zh:9630cf1e8c65efb7e7a9ccab3a152c03f002ba47a8ce058bd98be34419137746",
    "zh:b7d51f82ea1f73dbdabdd82d346015e9c1cde0dd334ed49150b35ca1843ba0b7",
    "zh:bd7771ff7da38f6a7772cf774a573a184010a01de170d8bb30740c7d3d45ddd3",
    "zh:c4bffcfc33b82765003e8f9ce22b3bc100ec28c2380eacb5a38a272c6bd2636e",
    "zh:cd08c5165ed1366d2c84b8e01005b7e42cceb3b4229d3cf31ea9da910029fe15",
    "zh:ea2424e1f8848da2283db82e93b5c8fb5f44170a0e62ae991a935d4aa38ce4b0",
    "zh:f9bf537f8be3626858d944b25995adfa513f9fc9f73003a7ce0461dbec0e78b4",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "2.1.0"
  constraints = "~> 2.1.0"
  hashes = [
    "h1:22g1pMQanm66Es0z0HbsVmlGNYyFVENjfu2fXbun0c0=",
    "h1:2TcmfEzBOGQPALErrXTaL6v+k/WAL40adao4izRYmdw=",
    "h1:4z5ScrSTf1Wm7OhryWNlrvRv2INuqP8VTPh9rskzs18=",
    "h1:5HjQsHlEiWYQhNjZJr/LLpYCD3cNk1n6X/XGj9n8SyE=",
    "h1:6NO8gcM/3gwU4PnFuyFgDbXQzrHe3F87F89Ml/VgA1E=",
    "h1:9IIowz12oanc7m8NUwtniu7F7KCX20KfAZCiv7kQBog=",
    "h1:B9c6RC7LWC2DmnuLX1l9aNe5MV54v8od5Kmvm4CSAAw=",
    "h1:FFgxjgOlyRstaP7vYdPpgai9q1U0T0OF9B4FF7ZknrM=",
    "h1:GsSwkiqVMUqqxSmqVRow5FinbFro2LIzWp+U8g8coGU=",
    "h1:KLcN04L26sZU6BFBo9rswno//hXSfX/59r/A5CKPU6s=",
    "h1:MGK7NYaE2Gt1sTueR4fO59P2jtpp0oARNWfNS66JL9k=",
    "h1:S2wiY5MzUa7gl9ctfbabAZ0RxfRZFScmA+V3LKhwM+I=",
    "h1:aB8eX1lHJGUqtkxxFm8StkAKznKSRaVOYpjFf7ZUeP8=",
    "h1:vQUzEnkCW69kMT5a/MiqstwOLp68KFjqlzbENZvyKWU=",
    "zh:113661750398bf21c8fe36aade9fb6f5eb82b5bcd3bcd30bd37ac805d83398f4",
    "zh:1b3c26347b9cd61e413ee93c2f422cc3278a77f55fd3516eaabb3e2a85f65281",
    "zh:1b751bbf1e4152829a643b532fd3f5967a2e89a41fac381257e0b41665be3306",
    "zh:1b967bbfd9b344419c0e0df0c3a15fcbd731e91f19a18955a55aace8d9ec039a",
    "zh:1bc0fc7c0a21e568db043b654501ce668ba19bf7628d37a7d2aaa512fd6e5aeb",
    "zh:425cbf61757d4b503e7bf0f409ea59835ca3afbd2432d56ad552c2e5d234a572",
    "zh:67d4f059cb4d73bf6c060313ec32962c4e5bd8dc7be2542a6f2098ab32575cd9",
    "zh:7fe841ac5b68a4f52fb3cf45070828f3845de44746679d434e4349f3c23e3ef2",
    "zh:ac1ed4c6ef0b6a3410568a05d3f9933d184497f065988503c43da0b2f0786ab2",
    "zh:c5c0d14c86fabd9ab6a5d555e6a8d511942665fb5fa948dd452b0d1934068344",
    "zh:c9ae5c210192275185d6823566a9421983e8e64c2665a4cae00b92dd0706bd19",
    "zh:ee9865ccc053e7f345e532654fb628d1cf1e81cd2e929643c1691bebffcf7b98",
    "zh:f3416d2f666095e740522c4964e436470bb9ec17bd53aaae8169ad93297d07bd",
    "zh:fbca85457dd49e17168989d64f7cfc4a519d55ef4e00e89cea2859e87ad87f83",
  ]
}
