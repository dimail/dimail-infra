terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 2.1.0"
    }

    ovh = {
      source  = "ovh/ovh"
      version = "~> 0.51.0"
    }
  }

  backend "s3" {
    key = "30_vms/terraform.tfstate"
  }
}


provider "openstack" {
  auth_url    = "https://auth.cloud.ovh.net/v3/"
  domain_name = "default"
  region      = var.regions["main"]
  tenant_name = var.ovh_openstack_tenant_name
  tenant_id   = var.ovh_openstack_tenant_id
}

provider "ovh" {
  endpoint = "ovh-eu"
}
