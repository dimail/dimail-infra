resource "openstack_blockstorage_volume_v3" "volumes" {
  for_each             = { for key, value in local.volumes : "${key}-${local.env_suffix}" => value }
  name                 = each.key
  size                 = each.value.size
  region               = each.value.region
  volume_type          = can(each.value.type) ? each.value.type : "classic"
  enable_online_resize = true
  metadata = {
    mount_dir = each.value.mount
  }
}
resource "openstack_compute_volume_attach_v2" "volumes" {
  for_each    = { for att in local.attachments : "${att.server_name}-${local.env_suffix}--${att.volume_name}-${local.env_suffix}" => att }
  instance_id = openstack_compute_instance_v2.vm["${each.value.server_name}-${local.env_suffix}"].id
  volume_id   = openstack_blockstorage_volume_v3.volumes["${each.value.volume_name}-${local.env_suffix}"].id
  region      = each.value.region
}
