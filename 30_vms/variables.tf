variable "env_name" {
  description = "Name of the current environment"
  type        = string
}

variable "tech_domain" {
  description = "Name of the technical domain"
  type        = string
}

variable "host_domain" {
  description = "Name of the host domain"
  type        = string
}

variable "ttl" {
  type = number
}

variable "regions" {
}
variable "sizes" {
}
variable "volumes" {
}

variable "ovh_openstack_tenant_name" {
  description = "Name of the project"
  type        = string
}

variable "ovh_openstack_tenant_id" {
  description = "ID of the project"
  type        = string
}

variable "ovh_public_cloud_project_id" {
  description = "ID of the public cloud project"
  type        = string
}

variable "private_network_name" {
  description = "Name of the private network"
  type        = string
}

variable "os_k8s_ip_range" {
  description = "The IP range of kube on OS"
  type        = string
}

variable "dns_nameservers" {
  description = "List of DNS servers"
  type        = list(string)
  default     = ["213.186.33.99"] # OVH default DNS
}

variable "servers" {
}

variable "config_path" {
  type        = string
  description = "absolute path of dimail-infra-config"
}
