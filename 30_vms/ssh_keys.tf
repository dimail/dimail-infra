locals {
  list_ssh_keys_names = fileset("${var.config_path}/assets/ssh_keys/", "*")
  list_ssh_keys = [for item in local.list_ssh_keys_names : file("${var.config_path}/assets/ssh_keys/${item}")]
  ssh_authorized_keys = zipmap(
    local.list_ssh_keys_names,
    local.list_ssh_keys,
  )
}

resource "openstack_compute_keypair_v2" "authorized_key" {
  for_each = {
    for pair in setproduct(local.regions, [for k, v in local.ssh_authorized_keys : [k, v]]) : "${pair[1][0]}-${pair[0]}" => {
      region     = pair[0]
      name       = pair[1][0]
      public_key = pair[1][1]
    }
  }

  name       = each.value.name
  region     = each.value.region
  public_key = each.value.public_key
}
