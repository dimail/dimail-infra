resource "openstack_compute_instance_v2" "vm" {
  for_each        = { for key, value in local.servers : "${key}-${local.env_suffix}" => value }
  name            = each.key
  region          = each.value.region
  image_id        = data.openstack_images_image_v2.debian["${each.value.image}-${each.value.region}"].id
  flavor_id       = data.openstack_compute_flavor_v2.size["${each.value.dimensionnement}-${each.value.region}"].id
  key_pair        = [for key in openstack_compute_keypair_v2.authorized_key : key.name if key.region == each.value.region][0]
  security_groups = [for r in each.value.roles : openstack_compute_secgroup_v2.vm_secgroup["${r}-${each.value.region}"].name if length(local.vm_secgroup_rules[r]) != 0]

  metadata = {
    "hostname"             = "${each.value.hostname}"
    ssh                    = "${each.value.hostname}.${var.host_domain}"
    "priv_fqdn"            = "${each.value.hostname}.priv.${var.host_domain}"
    "ovh-monthly-instance" = "${var.env_name}" == "prod" ? 1 : 0
    "sql_server_id"        = can(each.value.sql_server_id) ? each.value.sql_server_id : null
    "sql_master"           = can(each.value.sql_master) ? each.value.sql_master : null
    "ox_name"              = can(each.value.ox_name) ? each.value.ox_name : null
    "ox_store"             = can(each.value.ox_store) ? each.value.ox_store : null
    "ox_params"            = can(each.value.ox_params) ? join(",",each.value.ox_params) : null
  }
  user_data = <<EOF
#cloud-config
ssh_pwauth: false # disable password authentication
disable_root: true # disable root login
hostname: ${each.value.hostname}
fqdn: "${each.value.hostname}.${var.host_domain}"
prefer_fqdn_over_hostname: true
ssh_authorized_keys:%{for name, ssh_public_key in local.ssh_authorized_keys}
- ${ssh_public_key}
%{endfor}

EOF

  tags = concat([for r in each.value.roles : "role-${r}"], ["archi-${local.architecture}", "env-${var.env_name}"])
  network {
    port = openstack_networking_port_v2.frontnet[each.key].id
  }

  lifecycle {
    ignore_changes = [
      user_data,
      image_id,
      key_pair,
      network,
    ]
  }

  depends_on = [
    openstack_networking_port_v2.frontnet
  ]
}
