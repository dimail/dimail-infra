resource "openstack_compute_secgroup_v2" "vm_secgroup" {
  # create secgroups in each region for each secgroup type
  for_each = { for t in setproduct(keys(local.vm_secgroup_rules), local.regions) :
    join("-", [t[0], t[1]]) => {
      name   = t[0],
      rules  = local.vm_secgroup_rules[t[0]],
      region = t[1]
    } if length(local.vm_secgroup_rules[t[0]]) != 0 # comment if you need to remove a secgroup, in order to allow secgroup reference removal from dependencies before destroy.
  }

  region      = each.value.region
  name        = "${each.value.name}-${var.env_name}"
  description = "Security Group ${each.value.name} for VMs"

  dynamic "rule" {
    for_each = each.value.rules
    content {
      from_port   = rule.value.port
      to_port     = rule.value.port
      ip_protocol = rule.value.ip_protocol
      cidr        = try(rule.value.cidr, null)
      self        = try(rule.value.self, null)
    }
  }
}

## openstack_compute_secgroup_v2 is deprecated
# so we need to use openstack_networking_secgroup_v2
# work in progress to convert the code to use the new resource :

# resource "openstack_networking_secgroup_v2" "vm_secgroup" {
#   for_each = { for t in setproduct(keys(local.vm_secgroup_rules), local.regions) :
#     join("-", [t[0], t[1]]) => {
#       name   = t[0],
#       rules  = local.vm_secgroup_rules[t[0]],
#       region = t[1]
#     }
#   }

#   region      = each.value.region
#   name        = "${each.value.name}-${var.env_name}"
#   description = "Security Group ${each.value.name} for VMs"
#   delete_default_rules = true
# }

# resource "openstack_networking_secgroup_rule_v2" "vm_secgroup_rules" {
#   for_each = { for t in setproduct(keys(local.vm_secgroup_rules), local.regions) :
#     join("-", [t[0], t[1]]) => {
#       name   = t[0],
#       rules  = local.vm_secgroup_rules[t[0]],
#       region = t[1]
#     }
#   }

#   region = each.value.region
#   for rule in each.value.rules {
#     type        = "ingress"
#     direction   = "ingress"
#     ethertype   = "IPv4"
#     security_group_id = openstack_networking_secgroup_v2.vm_secgroup[each.key].id
#     protocol    = rule.ip_protocol
#     port_range_min = rule.port
#     port_range_max = rule.port
#     remote_ip_prefix = rule.cidr
#   }
# }
