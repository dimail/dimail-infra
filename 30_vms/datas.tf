
data "openstack_networking_floatingip_v2" "main_ip" {
  tags   = ["role-main_server", "archi-mecol", "${var.env_name}"]
  region = var.regions["main"]
}

data "openstack_networking_floatingip_v2" "backup_ip" {
  tags   = ["role-backup_server", "archi-mecol", "${var.env_name}"]
  region = var.regions["backup"]
}

data "openstack_networking_floatingip_v2" "monitor_ip" {
  tags   = ["role-monitor", "archi-mecol", "${var.env_name}"]
  region = var.regions["main"]
}

data "openstack_networking_floatingip_v2" "ldap_ip" {
  tags   = ["role-ldap", "archi-mecol", "${var.env_name}"]
  region = var.regions["main"]
}

data "ovh_domain_zone" "tech_domain" {
  name = var.tech_domain
}

data "ovh_domain_zone" "host_domain" {
  name = var.host_domain
}

