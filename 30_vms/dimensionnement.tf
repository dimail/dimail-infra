
# Fetch all known sizes in all usefull regions
data "openstack_compute_flavor_v2" "size" {
  for_each = { for t in setproduct(keys(var.sizes), local.regions) :
    join("-", [t[0], t[1]]) => {
      name   = var.sizes[t[0]],
      region = t[1]
    }
  }
  region = each.value.region
  name   = each.value.name
}
