## Private inter-region network

data "openstack_networking_network_v2" "backnet" {
  for_each = toset(local.regions)

  name   = var.private_network_name
  region = each.value
}

data "openstack_networking_subnet_v2" "backnet" {
  for_each = toset(local.regions)

  name       = "${var.private_network_name}-${lower(each.value)}-subnet"
  region     = each.value
  network_id = data.openstack_networking_network_v2.backnet[each.value].id
}

#### Network interfaces for the Private network

resource "openstack_networking_port_v2" "backnet" {
  for_each = { for key, value in local.servers : "${key}-${local.env_suffix}" => value }

  name   = "${each.key}-private-port"
  region = each.value.region

  network_id     = data.openstack_networking_network_v2.backnet[each.value.region].id
  admin_state_up = "true"

  fixed_ip {
    subnet_id = data.openstack_networking_subnet_v2.backnet[each.value.region].id
  }

  security_group_ids = [
    for role in each.value.roles :
    openstack_compute_secgroup_v2.vm_secgroup["${role}-${each.value.region}"].id
    if length(local.vm_secgroup_rules[role]) != 0
  ]
}

#### Attach the inter-region network port to the VMs

resource "openstack_compute_interface_attach_v2" "backnet" {
  for_each = { for key, value in local.servers : "${key}-${local.env_suffix}" => value }

  region      = each.value.region
  instance_id = openstack_compute_instance_v2.vm[each.key].id
  port_id     = openstack_networking_port_v2.backnet[each.key].id
}

