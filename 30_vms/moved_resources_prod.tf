# module.single.openstack_blockstorage_volume_v3.volumes["certificates-single-prod"]
# module.single.openstack_blockstorage_volume_v3.volumes["certificates2-single-prod"]
# module.single.openstack_blockstorage_volume_v3.volumes["certificates3-single-prod"]
# module.single.openstack_blockstorage_volume_v3.volumes["logs-single-prod"]
# module.single.openstack_blockstorage_volume_v3.volumes["logs2-single-prod"]
# module.single.openstack_blockstorage_volume_v3.volumes["maildir-single-prod"]
# module.single.openstack_blockstorage_volume_v3.volumes["maildir2-single-prod"]
# module.single.openstack_compute_floatingip_associate_v2.public_ip_association["backup-single-prod"]
# module.single.openstack_compute_floatingip_associate_v2.public_ip_association["monitor-single-prod"]
# module.single.openstack_compute_floatingip_associate_v2.public_ip_association["ox-app-single-prod"]
# module.single.openstack_compute_instance_v2.vm["backup-single-prod"]
# module.single.openstack_compute_instance_v2.vm["monitor-single-prod"]
# module.single.openstack_compute_instance_v2.vm["ox-app-single-prod"]
# module.single.openstack_compute_interface_attach_v2.backnet["backup-single-prod"]
# module.single.openstack_compute_interface_attach_v2.backnet["monitor-single-prod"]
# module.single.openstack_compute_interface_attach_v2.backnet["ox-app-single-prod"]
# module.single.openstack_compute_volume_attach_v2.volumes["backup-single-prod--certificates2-single-prod"]
# module.single.openstack_compute_volume_attach_v2.volumes["backup-single-prod--logs2-single-prod"]
# module.single.openstack_compute_volume_attach_v2.volumes["backup-single-prod--maildir2-single-prod"]
# module.single.openstack_compute_volume_attach_v2.volumes["monitor-single-prod--certificates3-single-prod"]
# module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--certificates-single-prod"]
# module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--logs-single-prod"]
# module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--maildir-single-prod"]
# module.single.openstack_networking_port_v2.backnet["backup-single-prod"]
# module.single.openstack_networking_port_v2.backnet["monitor-single-prod"]
# module.single.openstack_networking_port_v2.backnet["ox-app-single-prod"]
# module.single.openstack_networking_port_v2.frontnet["backup-single-prod"]
# module.single.openstack_networking_port_v2.frontnet["monitor-single-prod"]
# module.single.openstack_networking_port_v2.frontnet["ox-app-single-prod"]

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["certificates-single-prod"]
  to   = openstack_blockstorage_volume_v3.volumes["certificates-single-prod"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["certificates2-single-prod"]
  to   = openstack_blockstorage_volume_v3.volumes["certificates2-single-prod"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["certificates3-single-prod"]
  to   = openstack_blockstorage_volume_v3.volumes["certificates3-single-prod"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["logs-single-prod"]
  to   = openstack_blockstorage_volume_v3.volumes["logs-single-prod"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["logs2-single-prod"]
  to   = openstack_blockstorage_volume_v3.volumes["logs2-single-prod"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["maildir-single-prod"]
  to   = openstack_blockstorage_volume_v3.volumes["maildir-single-prod"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["maildir2-single-prod"]
  to   = openstack_blockstorage_volume_v3.volumes["maildir2-single-prod"]
}

moved {
  from = module.single.openstack_compute_floatingip_associate_v2.public_ip_association["backup-single-prod"]
  to   = openstack_compute_floatingip_associate_v2.public_ip_association["backup-single-prod"]
}

moved {
  from = module.single.openstack_compute_floatingip_associate_v2.public_ip_association["monitor-single-prod"]
  to   = openstack_compute_floatingip_associate_v2.public_ip_association["monitor-single-prod"]
}

moved {
  from = module.single.openstack_compute_floatingip_associate_v2.public_ip_association["ox-app-single-prod"]
  to   = openstack_compute_floatingip_associate_v2.public_ip_association["ox-app-single-prod"]
}

moved {
  from = module.single.openstack_compute_instance_v2.vm["backup-single-prod"]
  to   = openstack_compute_instance_v2.vm["backup-single-prod"]
}

moved {
  from = module.single.openstack_compute_instance_v2.vm["monitor-single-prod"]
  to   = openstack_compute_instance_v2.vm["monitor-single-prod"]
}

moved {
  from = module.single.openstack_compute_instance_v2.vm["ox-app-single-prod"]
  to   = openstack_compute_instance_v2.vm["ox-app-single-prod"]
}

moved {
  from = module.single.openstack_compute_interface_attach_v2.backnet["backup-single-prod"]
  to   = openstack_compute_interface_attach_v2.backnet["backup-single-prod"]
}

moved {
  from = module.single.openstack_compute_interface_attach_v2.backnet["monitor-single-prod"]
  to   = openstack_compute_interface_attach_v2.backnet["monitor-single-prod"]
}

moved {
  from = module.single.openstack_compute_interface_attach_v2.backnet["ox-app-single-prod"]
  to   = openstack_compute_interface_attach_v2.backnet["ox-app-single-prod"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["backup-single-prod--certificates2-single-prod"]
  to   = openstack_compute_volume_attach_v2.volumes["backup-single-prod--certificates2-single-prod"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["backup-single-prod--logs2-single-prod"]
  to   = openstack_compute_volume_attach_v2.volumes["backup-single-prod--logs2-single-prod"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["backup-single-prod--maildir2-single-prod"]
  to   = openstack_compute_volume_attach_v2.volumes["backup-single-prod--maildir2-single-prod"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["monitor-single-prod--certificates3-single-prod"]
  to   = openstack_compute_volume_attach_v2.volumes["monitor-single-prod--certificates3-single-prod"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--certificates-single-prod"]
  to   = openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--certificates-single-prod"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--logs-single-prod"]
  to   = openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--logs-single-prod"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--maildir-single-prod"]
  to   = openstack_compute_volume_attach_v2.volumes["ox-app-single-prod--maildir-single-prod"]
}

moved {
  from = module.single.openstack_networking_port_v2.backnet["backup-single-prod"]
  to   = openstack_networking_port_v2.backnet["backup-single-prod"]
}

moved {
  from = module.single.openstack_networking_port_v2.backnet["monitor-single-prod"]
  to   = openstack_networking_port_v2.backnet["monitor-single-prod"]
}

moved {
  from = module.single.openstack_networking_port_v2.backnet["ox-app-single-prod"]
  to   = openstack_networking_port_v2.backnet["ox-app-single-prod"]
}

moved {
  from = module.single.openstack_networking_port_v2.frontnet["backup-single-prod"]
  to   = openstack_networking_port_v2.frontnet["backup-single-prod"]
}

moved {
  from = module.single.openstack_networking_port_v2.frontnet["monitor-single-prod"]
  to   = openstack_networking_port_v2.frontnet["monitor-single-prod"]
}

moved {
  from = module.single.openstack_networking_port_v2.frontnet["ox-app-single-prod"]
  to   = openstack_networking_port_v2.frontnet["ox-app-single-prod"]
}
