locals {
  vms_with_public_ip         = { for key, value in local.servers : "${key}-${local.env_suffix}" => value if can(value.public_ip) }
  frontnet_subnets_by_region = module.frontnet_subnets_addrs.network_cidr_blocks
  frontnet_network_id_by_region = {
    for region in local.regions : upper(region) => [for network in ovh_cloud_project_network_private.frontnet[region].regions_attributes : network.openstackid if network.region == region][0]
  }
}

### Private network for floating IP routing

resource "ovh_cloud_project_network_private" "frontnet" {
  for_each = {
    (var.regions["main"])   = 0
    (var.regions["backup"]) = 1
  }
  service_name = var.ovh_public_cloud_project_id
  vlan_id      = each.value
  name         = "frontend-network"
  regions      = [each.key]

  lifecycle {
    prevent_destroy = false
  }
}

module "frontnet_subnets_addrs" {
  source = "hashicorp/subnets/cidr"

  base_cidr_block = "192.168.0.0/16"
  networks = [
    for region in local.regions : {
      name     = "${region}"
      new_bits = 4
    }
  ]
}

resource "openstack_networking_subnet_v2" "frontnet" {
  for_each = {
    (var.regions["main"])   = "192.168.12.0/24"
    (var.regions["backup"]) = "192.168.13.0/24"
  }

  name   = "frontend-network-${var.env_name}-${lower(each.key)}-subnet"
  region = upper(each.key)

  network_id = local.frontnet_network_id_by_region[upper(each.key)]
  cidr       = each.value
  ip_version = 4

  enable_dhcp     = true
  no_gateway      = false
  dns_nameservers = var.dns_nameservers

  allocation_pool {
    start = cidrhost(each.value, 10)
    end   = cidrhost(each.value, -2)
  }
}

### External Network

data "openstack_networking_network_v2" "ext-net" {
  for_each = toset(local.regions)

  name   = "Ext-Net"
  region = each.value
}

### Gateway for routing the floating IPs

resource "openstack_networking_router_v2" "frontnet" {
  for_each = toset(local.regions)

  name                = "frontnet-${var.env_name}-${lower(each.value)}-gateway"
  region              = each.value
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext-net[each.value].id
}

resource "openstack_networking_router_interface_v2" "frontnet" {
  for_each = toset(local.regions)

  region    = each.value
  router_id = openstack_networking_router_v2.frontnet[each.value].id
  subnet_id = openstack_networking_subnet_v2.frontnet[each.value].id
}

### Create one port on the floating IPs network for each VM

resource "openstack_networking_port_v2" "frontnet" {
  for_each = local.vms_with_public_ip

  name   = "frontnet-${each.key}-port"
  region = each.value.region

  network_id     = local.frontnet_network_id_by_region[each.value.region]
  admin_state_up = "true"

  security_group_ids = [
    for role in each.value.roles :
    openstack_compute_secgroup_v2.vm_secgroup["${role}-${each.value.region}"].id
     if length(local.vm_secgroup_rules[role]) != 0
  ]

  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.frontnet[each.value.region].id
  }

}

# ### Attach the floating ips network to the VMs

resource "openstack_compute_floatingip_associate_v2" "public_ip_association" {
  for_each = { for key, value in local.servers : "${key}-${local.env_suffix}" => value if can(value.public_ip) }

  floating_ip = each.value.public_ip
  region      = each.value.region
  instance_id = openstack_compute_instance_v2.vm[each.key].id
}
