# Fetch all known images in all usefull regions
data "openstack_images_image_v2" "debian" {
  for_each = { for t in setproduct(keys(local.images), local.regions) :
    join("-", [t[0], t[1]]) => {
      name   = local.images[t[0]],
      region = t[1]
    }
  }
  region      = each.value.region
  name        = each.value.name
  most_recent = true
}

