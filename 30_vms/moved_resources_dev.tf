# module.single.openstack_blockstorage_volume_v3.volumes["certificates-single-dev"]
# module.single.openstack_blockstorage_volume_v3.volumes["certificates2-single-dev"]
# module.single.openstack_blockstorage_volume_v3.volumes["certificates3-single-dev"]
# module.single.openstack_blockstorage_volume_v3.volumes["logs-single-dev"]
# module.single.openstack_blockstorage_volume_v3.volumes["logs2-single-dev"]
# module.single.openstack_blockstorage_volume_v3.volumes["maildir-single-dev"]
# module.single.openstack_blockstorage_volume_v3.volumes["maildir2-single-dev"]
# module.single.openstack_compute_floatingip_associate_v2.public_ip_association["backup-single-dev"]
# module.single.openstack_compute_floatingip_associate_v2.public_ip_association["monitor-single-dev"]
# module.single.openstack_compute_floatingip_associate_v2.public_ip_association["ox-app-single-dev"]
# module.single.openstack_compute_instance_v2.vm["backup-single-dev"]
# module.single.openstack_compute_instance_v2.vm["monitor-single-dev"]
# module.single.openstack_compute_instance_v2.vm["ox-app-single-dev"]
# module.single.openstack_compute_interface_attach_v2.backnet["backup-single-dev"]
# module.single.openstack_compute_interface_attach_v2.backnet["monitor-single-dev"]
# module.single.openstack_compute_interface_attach_v2.backnet["ox-app-single-dev"]
# module.single.openstack_compute_volume_attach_v2.volumes["backup-single-dev--certificates2-single-dev"]
# module.single.openstack_compute_volume_attach_v2.volumes["backup-single-dev--logs2-single-dev"]
# module.single.openstack_compute_volume_attach_v2.volumes["backup-single-dev--maildir2-single-dev"]
# module.single.openstack_compute_volume_attach_v2.volumes["monitor-single-dev--certificates3-single-dev"]
# module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--certificates-single-dev"]
# module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--logs-single-dev"]
# module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--maildir-single-dev"]
# module.single.openstack_networking_port_v2.backnet["backup-single-dev"]
# module.single.openstack_networking_port_v2.backnet["monitor-single-dev"]
# module.single.openstack_networking_port_v2.backnet["ox-app-single-dev"]
# module.single.openstack_networking_port_v2.frontnet["backup-single-dev"]
# module.single.openstack_networking_port_v2.frontnet["monitor-single-dev"]
# module.single.openstack_networking_port_v2.frontnet["ox-app-single-dev"]

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["certificates-single-dev"]
  to   = openstack_blockstorage_volume_v3.volumes["certificates-single-dev"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["certificates2-single-dev"]
  to   = openstack_blockstorage_volume_v3.volumes["certificates2-single-dev"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["certificates3-single-dev"]
  to   = openstack_blockstorage_volume_v3.volumes["certificates3-single-dev"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["logs-single-dev"]
  to   = openstack_blockstorage_volume_v3.volumes["logs-single-dev"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["logs2-single-dev"]
  to   = openstack_blockstorage_volume_v3.volumes["logs2-single-dev"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["maildir-single-dev"]
  to   = openstack_blockstorage_volume_v3.volumes["maildir-single-dev"]
}

moved {
  from = module.single.openstack_blockstorage_volume_v3.volumes["maildir2-single-dev"]
  to   = openstack_blockstorage_volume_v3.volumes["maildir2-single-dev"]
}

moved {
  from = module.single.openstack_compute_floatingip_associate_v2.public_ip_association["backup-single-dev"]
  to   = openstack_compute_floatingip_associate_v2.public_ip_association["backup-single-dev"]
}

moved {
  from = module.single.openstack_compute_floatingip_associate_v2.public_ip_association["monitor-single-dev"]
  to   = openstack_compute_floatingip_associate_v2.public_ip_association["monitor-single-dev"]
}

moved {
  from = module.single.openstack_compute_floatingip_associate_v2.public_ip_association["ox-app-single-dev"]
  to   = openstack_compute_floatingip_associate_v2.public_ip_association["ox-app-single-dev"]
}

moved {
  from = module.single.openstack_compute_instance_v2.vm["backup-single-dev"]
  to   = openstack_compute_instance_v2.vm["backup-single-dev"]
}

moved {
  from = module.single.openstack_compute_instance_v2.vm["monitor-single-dev"]
  to   = openstack_compute_instance_v2.vm["monitor-single-dev"]
}

moved {
  from = module.single.openstack_compute_instance_v2.vm["ox-app-single-dev"]
  to   = openstack_compute_instance_v2.vm["ox-app-single-dev"]
}

moved {
  from = module.single.openstack_compute_interface_attach_v2.backnet["backup-single-dev"]
  to   = openstack_compute_interface_attach_v2.backnet["backup-single-dev"]
}

moved {
  from = module.single.openstack_compute_interface_attach_v2.backnet["monitor-single-dev"]
  to   = openstack_compute_interface_attach_v2.backnet["monitor-single-dev"]
}

moved {
  from = module.single.openstack_compute_interface_attach_v2.backnet["ox-app-single-dev"]
  to   = openstack_compute_interface_attach_v2.backnet["ox-app-single-dev"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["backup-single-dev--certificates2-single-dev"]
  to   = openstack_compute_volume_attach_v2.volumes["backup-single-dev--certificates2-single-dev"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["backup-single-dev--logs2-single-dev"]
  to   = openstack_compute_volume_attach_v2.volumes["backup-single-dev--logs2-single-dev"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["backup-single-dev--maildir2-single-dev"]
  to   = openstack_compute_volume_attach_v2.volumes["backup-single-dev--maildir2-single-dev"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["monitor-single-dev--certificates3-single-dev"]
  to   = openstack_compute_volume_attach_v2.volumes["monitor-single-dev--certificates3-single-dev"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--certificates-single-dev"]
  to   = openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--certificates-single-dev"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--logs-single-dev"]
  to   = openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--logs-single-dev"]
}

moved {
  from = module.single.openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--maildir-single-dev"]
  to   = openstack_compute_volume_attach_v2.volumes["ox-app-single-dev--maildir-single-dev"]
}

moved {
  from = module.single.openstack_networking_port_v2.backnet["backup-single-dev"]
  to   = openstack_networking_port_v2.backnet["backup-single-dev"]
}

moved {
  from = module.single.openstack_networking_port_v2.backnet["monitor-single-dev"]
  to   = openstack_networking_port_v2.backnet["monitor-single-dev"]
}

moved {
  from = module.single.openstack_networking_port_v2.backnet["ox-app-single-dev"]
  to   = openstack_networking_port_v2.backnet["ox-app-single-dev"]
}

moved {
  from = module.single.openstack_networking_port_v2.frontnet["backup-single-dev"]
  to   = openstack_networking_port_v2.frontnet["backup-single-dev"]
}

moved {
  from = module.single.openstack_networking_port_v2.frontnet["monitor-single-dev"]
  to   = openstack_networking_port_v2.frontnet["monitor-single-dev"]
}

moved {
  from = module.single.openstack_networking_port_v2.frontnet["ox-app-single-dev"]
  to   = openstack_networking_port_v2.frontnet["ox-app-single-dev"]
}
