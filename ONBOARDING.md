# Guide d'Onboarding

Bienvenue dans notre projet ! Ce guide vous aidera à configurer votre environnement et à comprendre les bases du projet.

## Prérequis

1. **Git** - Assurez-vous d'avoir Git installé sur votre machine.
2. **Terraform** - Installez Terraform pour gérer l'infrastructure.
3. **Ansible** - Installez Ansible pour l'automatisation des configurations.
4. **Nix** (optionnel) - Installez Nix pour la gestion des dépendances.

## Configuration de l'Environnement

1. **Cloner le dépôt**

    ```sh
    git clone <URL_DU_DEPOT>
    cd <NOM_DU_DEPOT>
    ```

2. **Configurer les fichiers d'environnement**

Pour configurer votre environnment il vous faut concevoir un dossier sur votre machine nommé `dimail-infra-config`.

Ce dernier contiendra vos dossier pour chaque plateforme que vous souhaitez initialiser grâce à `dimail-infra`.

3. **Installer les dépendances avec Nix (optionnel)**

    Si vous avez installé Nix, vous pouvez l'utiliser pour gérer les dépendances :

    ```sh
    nix-shell
    ```

## Ajouter votre clé SSH

Pour pouvoir vous connecter aux serveurs générés par Terraform, vous devez ajouter votre clé SSH au projet.

1. **Générer une clé SSH (si vous n'en avez pas déjà une)**

    ```sh
    ssh-keygen -t rsa -b 4096 -C "votre_email@example.com"
    ```

    Suivez les instructions et enregistrez la clé dans le répertoire par défaut (`~/.ssh/id_rsa`).

2. **Ajouter votre clé SSH publique au projet**

    Copiez le contenu de votre clé publique :

    ```sh
    cat ~/.ssh/id_rsa.pub
    ```

    Ajoutez cette clé au répertoire [ssh_keys](./assets/ssh_keys/) :

    ```sh
    echo "votre_clé_publique_ici" > assets/ssh_keys/votre_nom
    ```

## Structure du Projet

- [.github](./.github) : Contient les workflows GitHub Actions.
- [10_backend_ovh](./10_backend_ovh/), [20_network](./20_network/), [30_outscale](./30_outscale/), [30_vms](./30_vms/) : Contiennent le code lié à Terraform.
- [40_ansible](./40_ansible/) : Contient le code pour Ansible.
- [docs](./docs/) : Documentation du projet.
- [tools](./tools/) : Scripts et outils utiles pour le projet.

## Commandes Utiles

- **Construire le projet**

    ```sh
    make build
    ```

- **Déployer l'infrastructure**

    ```sh
    make deploy
    ```

- **Exécuter les tests**

    ```sh
    make test
    ```

## Documentation

Pour plus de détails, consultez le fichier [README.md](./README.md).
