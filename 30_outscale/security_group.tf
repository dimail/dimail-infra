locals {
  servers_roles = {
    for name, server in var.servers :
    name => {
      "roles" = [
        for role in server.roles : split(":", role)[0]
      ]
    }
  }
  security_groups = {
    "all" = [
      { name = "ssh", proto = "tcp", from = "bastion", port = "22" },
      { name = "ssh", proto = "tcp", from = "monitor", port = "22" },
      { name = "ssh", proto = "tcp", from = "api_server", port = "22" },
      { name = "nrpe", proto = "tcp", from = "monitor", port = "5666" },
      { name = "ping", proto = "icmp", from = "monitor", port = "-1" },
      { name = "node_exporter", proto = "tcp", from = "monitor", port = "9100" }
    ],
    "api_server" = [
      { name = "api", proto = "tcp", from = "webfront", port = "8000" },
      { name = "api", proto = "tcp", from = "monitor", port = "8000" },
    ],
    "bastion" = [
      { name = "ssh", proto = "tcp", from = "0.0.0.0/0", port = 22 },
    ],
    "cert_manager" = [
      # Rsync pour copier les certificats, http(s) pour letsenscrypt
      { name = "rsync", proto = "tcp", from = "cert_user", port = "873" },
      { name = "http", proto = "tcp", from = "0.0.0.0/0", port = "80" },
      { name = "https", proto = "tcp", from = "0.0.0.0/0", port = "443" },
      { name = "nginx_prom", proto = "tcp", from = "monitor", port = "9080" },
    ],
    "cert_user" = [
      # Pour les challenges letsenscrypt
      { name = "http", proto = "tcp", from = "0.0.0.0/0", port = "80" },
      { name = "https", proto = "tcp", from = "0.0.0.0/0", port = "443" },
      { name = "nginx_prom", proto = "tcp", from = "monitor", port = "9080" },
    ],
    "imap_master" = [
      { name = "imap", proto = "tcp", from = "0.0.0.0/0", port = "143" },
      { name = "imaps", proto = "tcp", from = "0.0.0.0/0", port = "993" },
      { name = "pop", proto = "tcp", from = "0.0.0.0/0", port = "110" },
      { name = "pops", proto = "tcp", from = "0.0.0.0/0", port = "995" },
      { name = "sieve", proto = "tcp", from = "0.0.0.0/0", port = "4190" },
      { name = "rsync", proto = "tcp", from = "imap_slave", port = "873" },
      { name = "dovecot_prom", proto = "tcp", from = "monitor", port = "9900" },
    ],
    "imap_slave" = [
      { name = "imap", proto = "tcp", from = "0.0.0.0/0", port = "143" },
      { name = "imaps", proto = "tcp", from = "0.0.0.0/0", port = "993" },
      { name = "pop", proto = "tcp", from = "0.0.0.0/0", port = "110" },
      { name = "pops", proto = "tcp", from = "0.0.0.0/0", port = "995" },
      { name = "sieve", proto = "tcp", from = "0.0.0.0/0", port = "4190" },
      { name = "dovecot_prom", proto = "tcp", from = "monitor", port = "9900" },
    ],
    "ox8" = [
      { name = "http", proto = "tcp", from = "0.0.0.0/0", port = "80" },
      { name = "https", proto = "tcp", from = "0.0.0.0/0", port = "443" },
      { name = "nginx_prom", proto = "tcp", from = "monitor", port = "9080" },
    ],
    "keycloak" = [
      { name = "keycloak", proto = "tcp", from = "webfront", port = "8443" },
    ],
    "ldap_server" = [
      { name = "http", proto = "tcp", from = "0.0.0.0/0", port = "80" },
      { name = "https", proto = "tcp", from = "0.0.0.0/0", port = "443" },
      { name = "ldap", proto = "tcp", from = "0.0.0.0/0", port = "389" },
      { name = "ldaps", proto = "tcp", from = "0.0.0.0/0", port = "636" },
      { name = "nginx_prom", proto = "tcp", from = "monitor", port = "9080" },
    ],
    "mail_filter" = [
      { name = "amavis", proto = "tcp", from = "smtp", port = "10024" },
      { name = "postfix-spam", proto = "tcp", from = "smtp", port = "783" },
      { name = "ox-spam", proto = "tcp", from = "ox", port = "783" },
    ],
    "monitor" = [
      { name = "http", proto = "tcp", from = "0.0.0.0/0", port = "80" },
      { name = "https", proto = "tcp", from = "0.0.0.0/0", port = "443" },
      { name = "nginx_prom", proto = "tcp", from = "monitor", port = "9080" },
    ],
    "ox" = [
      { name = "http", proto = "tcp", from = "0.0.0.0/0", port = "80" },
      { name = "https", proto = "tcp", from = "0.0.0.0/0", port = "443" },
      { name = "ox_appsuite", proto = "tcp", from = "webfront", port = "8009" },
      { name = "hazelcast", proto = "tcp", from = "ox", port = "5705" },
      { name = "nginx_prom", proto = "tcp", from = "monitor", port = "9080" },
    ],
    "smtp" = [
      { name = "smtp", proto = "tcp", from = "0.0.0.0/0", port = "25" },
      { name = "submission", proto = "tcp", from = "0.0.0.0/0", port = "465" },
      { name = "starttls", proto = "tcp", from = "0.0.0.0/0", port = "587" },
      { name = "postfix_prom", proto = "tcp", from = "monitor", port = "9154" },
      { name = "amavis_return", proto = "tcp", from = "mail_filter", port = "10025" },
    ],
    "sql_master" = [
      { name = "mysql_ox", proto = "tcp", from = "ox", port = "3306" },
      { name = "mysql_replica", proto = "tcp", from = "sql_slave", port = "3306" },
      { name = "mysql_monitor", proto = "tcp", from = "monitor", port = "3306" },
      { name = "mysql_k8s", proto = "tcp", from = "ox8", port = "3306" },
      { name = "mysql_prom", proto = "tcp", from = "monitor", port = "9306" },
    ],
    "sql_slave" = [
      { name = "mysql_monitor", proto = "tcp", from = "monitor", port = "3306" },
      { name = "mysql_prom", proto = "tcp", from = "monitor", port = "9306" },
    ],
    "webfront" = [
      { name = "http", proto = "tcp", from = "0.0.0.0/0", port = "80" },
      { name = "https", proto = "tcp", from = "0.0.0.0/0", port = "443" },
      { name = "nginx_prom", proto = "tcp", from = "monitor", port = "9080" },
    ],
    "backup_role" = [],
  }

  # On merge les security groups génériques avec les global_security_groups
  merged_security_groups = {
    for role, rules in local.security_groups :
    role => concat(rules, contains(keys(var.global_security_groups), role) ? var.global_security_groups[role] : [])
  }

  # Map un role vers la liste des rules, comme au dessus, on a remplace 'from = "role"' en from = [ ips ]
  rules_with_ip = {
    for role, rules in local.merged_security_groups :
    role => [
      for rule in rules : {
        port  = rule.port
        proto = rule.proto
        ip_ranges = can(regex("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}/\\d{1,2}", rule.from)) ? [rule.from] : flatten([
          for name, server in var.servers :
          contains(local.servers_roles[name].roles, rule.from) ?
          flatten(["${outscale_vm.hosts[name].private_ip}/32",
            var.networks[var.servers[name].network].is_public ? ["${outscale_vm.hosts[name].public_ip}/32"] : []
          ]) : []
        ])
      }
    ]
  }
  # Map un nom de serveur vers la liste de toutes les rules applicables
  server_rules_1 = {
    for name, server in var.servers :
    name => flatten([
      local.rules_with_ip["all"], [
        for role in local.servers_roles[name].roles :
        [
          local.rules_with_ip[role]
        ]
      ]
    ])
  }
  # Map un nom de serveur vers une liste des rules, pour chaque rule on reprend toutes les IP sources admises. Et on deduplique partout.
  server_rules = {
    for name, rules in local.server_rules_1 :
    name => distinct([
      for rule in rules : {
        port = rule.port
        ip_ranges = distinct(flatten([
          for y in rules :
          y.proto == rule.proto && y.port == rule.port ? [y.ip_ranges] : []
        ]))
        proto = rule.proto
      }
    ])
  }
}

resource "outscale_security_group" "sg_server" {
  for_each    = { for name, server in var.servers : name => server }
  description = "security group for virtual machine ${each.key}"
  net_id      = outscale_net.network.id
  dynamic "tags" {
    for_each = var.global_tags
    content {
      key   = tags.key
      value = tags.value
    }
  }
}

resource "outscale_security_group_rule" "server_rules" {
  for_each          = tomap(local.server_rules)
  flow              = "Inbound"
  security_group_id = outscale_security_group.sg_server[each.key].security_group_id
  dynamic "rules" {
    for_each = { for idx, rule in each.value : tostring(idx) => rule }
    content {
      from_port_range = rules.value.port
      to_port_range   = rules.value.port
      ip_protocol     = rules.value.proto
      ip_ranges       = sort(rules.value.ip_ranges)
    }
  }
}


