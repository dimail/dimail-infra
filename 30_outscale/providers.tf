terraform {
  required_providers {
    outscale = {
      source  = "outscale/outscale"
      version = "1.0.1"
    }
    ovh = {
      source  = "ovh/ovh"
      version = "~> 0.51.0"
    }
  }

  backend "s3" {
    key     = "30_outscale/terraform.tfstate"
    encrypt = true
  }
}

# le provider outscale prend les identifiants d'accès à l'API 3DS avec les variables OUTSCALE_ACCESSKEYID et OUTSCALE_ACCESSKEYID
provider "outscale" {
  region = var.root_region

  endpoints {
    api = "api.${var.root_region}.outscale.com"
  }
}

provider "ovh" {
  endpoint = "ovh-eu"
}

