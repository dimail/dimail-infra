locals {
  servers = [for name, server in var.servers :
    {
      name                     = name
      image_id                 = var.image_ids[server.image]
      vm_type                  = local.vm_type[server.size]
      subnet_id                = outscale_subnet.subnet[server.network].id
      placement_subregion_name = local.regions[var.networks[server.network].region]
      security_group_ids       = [outscale_security_group.sg_server[name].security_group_id]
      local_volumes            = server.local_volumes
      tags = merge(var.global_tags, {
        "Name" : name,
        "Roles" : join(",", server.roles),
        "priv_fqdn" : "${name}.priv.${var.host_domain}",
        "tech_fqdn" : "${name}.${var.host_domain}",
        },
        var.networks[server.network].is_public ? {
          "osc.fcu.eip.auto-attach" : outscale_public_ip.public_ips[name].public_ip,
          "Is_public" : "yes",
          } : {
          "Is_public" : "no",
        },
        server.sql_server_id != null ? {
          "sql_server_id" : server.sql_server_id,
        } : {},
      )
      user_data : base64encode(<<EOF
        #cloud-config
        ssh_pwauth: false # disable password authentication
        disable_root: true # disable root login
        hostname: "${name}"
        fqdn: "${name}.${var.host_domain}"
        prefer_fqdn_over_hostname: true
        ssh_authorized_keys:%{for name, ssh_public_key in local.ssh_authorized_keys}
           - ${ssh_public_key}
        %{endfor}
        EOF
      )
  }]
}

resource "outscale_vm" "hosts" {
  for_each                 = { for server in local.servers : server.name => server }
  image_id                 = each.value.image_id
  vm_type                  = each.value.vm_type
  subnet_id                = each.value.subnet_id
  placement_subregion_name = each.value.placement_subregion_name
  security_group_ids       = each.value.security_group_ids
  user_data                = each.value.user_data

  dynamic "block_device_mappings" {
    for_each = each.value.local_volumes
    content {
      device_name = block_device_mappings.value.device_name
      bsu {
        volume_size = block_device_mappings.value.size
        volume_type = block_device_mappings.value.type
      }
    }
  }

  dynamic "tags" {
    for_each = each.value.tags
    content {
      key   = tags.key
      value = tags.value
    }
  }

  lifecycle {
    ignore_changes = [
      user_data,
      image_id,
      # key_pair,
      # network,
    ]
  }

}

