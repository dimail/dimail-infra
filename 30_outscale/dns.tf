data "ovh_domain_zone" "host" {
  name = var.host_domain
}

data "ovh_domain_zone" "technical" {
  name = var.tech_domain
}

locals {
  has_oidc = anytrue(flatten([ for name, server in var.servers: [for role in server.roles: strcontains(role, "oidc") ]]))

  tech_names_per_role = {
    "smtp"        = ["smtp", "mx"]
    "imap_master" = ["imap"]
    "webfront"    = concat(["webmail"], local.has_oidc ? ["oidc"] : [])
  }

  records_per_server_tech = flatten([
    for name, server in var.servers :
    [for role in (server.network == "backup" ? [] : server.roles) :
      [for a_name in can(local.tech_names_per_role[role]) ? local.tech_names_per_role[role] : [] :
        {
          name      = "${name}-${a_name}"
          public_ip = outscale_public_ip.public_ips[name].public_ip
          subdomain = a_name
        }
      ]
    ]
  ])
  host_names_per_role = {
    "webfront" = ["api", "auth"]
  }
  records_per_server_host = flatten([
    for name, server in var.servers :
    [for role in(server.network == "backup" ? [] : server.roles) :
      [for a_name in can(local.host_names_per_role[role]) ? local.host_names_per_role[role] : [] :
        {
          name      = "${name}-${a_name}"
          public_ip = outscale_public_ip.public_ips[name].public_ip
          subdomain = a_name
        }
      ]
    ]
  ])
  roles = toset(flatten([for name, server in var.servers : server.roles]))
  servers_by_role = {
    for role in local.roles :
    role => [for name, server in var.servers : name if contains(var.servers[name].roles, role)]
  }

  spf_ips = concat(var.foreign_spf_ips, [for server in local.servers_by_role["smtp"] : outscale_public_ip.public_ips[server].public_ip])
  spf     = "\"v=spf1 ${join(" ", [for ip in local.spf_ips : "ip4:${ip}"])} -all\""
}

resource "ovh_domain_zone_record" "tech_record" {
  for_each  = { for item in local.records_per_server_tech : item.name => item }
  zone      = data.ovh_domain_zone.technical.name
  subdomain = each.value.subdomain
  fieldtype = "A"
  ttl       = var.ttl
  target    = each.value.public_ip
}

resource "ovh_domain_zone_record" "spf_record" {
  zone      = data.ovh_domain_zone.technical.name
  subdomain = "_spf"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = local.spf
}

resource "ovh_domain_zone_record" "host_record" {
  for_each  = toset([for name, server in var.servers : name if var.networks[server.network].is_public])
  zone      = data.ovh_domain_zone.host.name
  subdomain = each.value
  fieldtype = "A"
  ttl       = var.ttl
  target    = outscale_public_ip.public_ips[each.value].public_ip
}

resource "ovh_domain_zone_record" "host_record_service" {
  for_each  = { for item in local.records_per_server_host : item.name => item }
  zone      = data.ovh_domain_zone.host.name
  subdomain = each.value.subdomain
  fieldtype = "A"
  ttl       = var.ttl
  target    = each.value.public_ip
}

resource "ovh_domain_zone_record" "private_record" {
  for_each  = toset(keys(var.servers))
  zone      = data.ovh_domain_zone.host.name
  subdomain = "${each.value}.priv"
  fieldtype = "A"
  ttl       = var.ttl
  target    = outscale_vm.hosts[each.value].private_ip
}

resource "ovh_domain_zone_record" "private_smtp_record" {
  for_each = toset([
    for name in(can(local.servers_by_role["smtp"]) ? local.servers_by_role["smtp"] : []) :
    name if !contains(var.servers[name].roles, "backup_role")
  ])
  zone      = data.ovh_domain_zone.host.name
  subdomain = "smtp.priv"
  fieldtype = "A"
  ttl       = var.ttl
  target    = outscale_vm.hosts[each.value].private_ip
}

resource "ovh_domain_zone_record" "private_spam_record" {
  for_each = toset([
    for name in(can(local.servers_by_role["mail_filter"]) ? local.servers_by_role["mail_filter"] : []) :
    name if !contains(var.servers[name].roles, "backup_role")
  ])
  zone      = data.ovh_domain_zone.host.name
  subdomain = "spam.priv"
  fieldtype = "A"
  ttl       = var.ttl
  target    = outscale_vm.hosts[each.value].private_ip
}

# Me make "testing.<host_domain>" a valid client domain

resource "ovh_domain_zone_record" "as_a_client_CNAME" {
  for_each  = toset(["webmail", "imap", "smtp"])
  zone      = data.ovh_domain_zone.host.name
  subdomain = "${each.value}.testing"
  fieldtype = "CNAME"
  ttl       = var.ttl
  target    = "${each.value}.${data.ovh_domain_zone.technical.name}."
}

resource "ovh_domain_zone_record" "as_a_client_MX" {
  zone      = data.ovh_domain_zone.host.name
  subdomain = "testing"
  fieldtype = "MX"
  ttl       = var.ttl
  target    = "10 mx.${data.ovh_domain_zone.technical.name}."
}

resource "ovh_domain_zone_record" "as_a_client_SPF" {
  zone      = data.ovh_domain_zone.host.name
  subdomain = "testing"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = "\"v=spf1 include:_spf.${data.ovh_domain_zone.technical.name} -all\""
}

resource "ovh_domain_zone_record" "as_a_client_DKIM" {
  zone      = data.ovh_domain_zone.host.name
  subdomain = "ox2024._domainkey.testing"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = local.dkim_testing
}


# Me make "system.<host_domain>" a valid client domain

resource "ovh_domain_zone_record" "system_CNAME" {
  for_each  = toset(["imap", "smtp"])
  zone      = data.ovh_domain_zone.host.name
  subdomain = "${each.value}.system"
  fieldtype = "CNAME"
  ttl       = var.ttl
  target    = "${each.value}.${data.ovh_domain_zone.technical.name}."
}

resource "ovh_domain_zone_record" "system_MX" {
  zone      = data.ovh_domain_zone.host.name
  subdomain = "system"
  fieldtype = "MX"
  ttl       = var.ttl
  target    = "10 mx.${data.ovh_domain_zone.technical.name}."
}

resource "ovh_domain_zone_record" "system_SPF" {
  zone      = data.ovh_domain_zone.host.name
  subdomain = "system"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = "\"v=spf1 include:_spf.${data.ovh_domain_zone.technical.name} -all\""
}

resource "ovh_domain_zone_record" "system_DKIM" {
  zone      = data.ovh_domain_zone.host.name
  subdomain = "ox2024._domainkey.system"
  fieldtype = "TXT"
  ttl       = var.ttl
  target    = local.dkim_system
}

# We make a CNAME in host_domain for k8s

resource "ovh_domain_zone_record" "external" {
  for_each  = toset(var.k8s_fqdn != "" ? ["k8s"] : [])
  zone      = data.ovh_domain_zone.host.name
  subdomain = "k8s"
  fieldtype = "CNAME"
  ttl       = var.ttl
  target    = var.k8s_fqdn
}
