# Add public_ip by creating resource and entry in local.public_ips
# note that a public ip is automagically linked to a server when it shares its name

#moved {
#  from = outscale_public_ip.main
#  to   = outscale_public_ip.public_ips["main"]
#}
#
#moved {
#  from = outscale_public_ip.backup
#  to   = outscale_public_ip.public_ips["backup"]
#}
#
#moved {
#  from = outscale_public_ip.monitor
#  to   = outscale_public_ip.public_ips["monitor"]
#}
#
#moved {
#  from = outscale_public_ip.bastion_main
#  to   = outscale_public_ip.public_ips["bastion1"]
#}
#
#moved {
#  from = outscale_public_ip.bastion_backup
#  to   = outscale_public_ip.public_ips["bastion2"]
#}
#
#moved {
#  from = outscale_public_ip.bastion_backup
#  to   = outscale_public_ip.public_ips["bastion2"]
#}
#
#moved {
#  from = outscale_public_ip.webmail_imap
#  to   = outscale_public_ip.public_ips["webmail-imap"]
#}
#
##moved {
##  from = outscale_public_ip.webmail_oidc
##  to   = outscale_public_ip.public_ips["webmail-oidc"]
##}

resource "outscale_public_ip" "public_ips" {
  for_each = { for name, server in var.servers : name => server if var.networks[server.network].is_public }
  lifecycle {
    prevent_destroy = true
  }
  dynamic "tags" {
    for_each = merge(
      var.global_tags,
      { Name = "public_ip ${each.key} ${var.env}" }
    )
    content {
      key   = tags.key
      value = tags.value
    }
  }

}

moved {
  from = outscale_public_ip.feu_webmail_oidc
  to   = outscale_public_ip.nat_ip
}

resource "outscale_public_ip" "nat_ip" {
  dynamic "tags" {
    for_each = merge(
      var.global_tags,
      { Name = "public_ip.nat_ip ${var.env}" }
    )
    content {
      key   = tags.key
      value = tags.value
    }
  }
}

resource "outscale_public_ip_link" "public_ip_link" {
  for_each  = toset([for name, server in var.servers : name if var.networks[server.network].is_public])
  vm_id     = outscale_vm.hosts[each.value].id
  public_ip = outscale_public_ip.public_ips[each.value].public_ip
}
