locals {
  volumes = flatten([
    for server_name, server in var.servers :
    [
      for volume_name, volume in server.volumes :
      merge({
        name   = "${volume_name}"
        region = local.regions[server.network]
        tags = merge(var.global_tags, {
          Name       = "${volume_name}",
          attachedTo = server_name
          deviceName = volume.device
          mount      = volume.mount
        })
      }, volume)
    ]
  ])
}

resource "outscale_volume" "volumes" {
  for_each       = { for volume in local.volumes : volume.name => volume }
  subregion_name = each.value.region
  size           = each.value.size
  volume_type    = each.value.type
  dynamic "tags" {
    for_each = each.value.tags
    content {
      key   = tags.key
      value = tags.value
    }
  }
  # these 3 lines prevent destroying volumes, comment them only if you know exactly what you're doing
  #lifecycle {
  #  prevent_destroy = true
  #}
}

