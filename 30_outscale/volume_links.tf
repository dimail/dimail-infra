locals {
  volume_links = {
    for volume_name, volume in outscale_volume.volumes :
    volume_name => {
      vm_id       = outscale_vm.hosts[{ for tag in volume.tags : tag.key => tag.value }["attachedTo"]].id
      volume_id   = volume.id
      device_name = { for tag in volume.tags : tag.key => tag.value }["deviceName"]
    }
  }
}

resource "outscale_volume_link" "volumes_link01" {
  for_each    = local.volume_links
  device_name = each.value.device_name
  volume_id   = each.value.volume_id
  vm_id       = each.value.vm_id
}

