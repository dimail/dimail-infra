data "local_file" "dkim_system" {
  filename = "${var.config_path}/assets/dkim_keys/system.${var.host_domain}_ox2024.txt"
}

data "local_file" "dkim_testing" {
  filename = "${var.config_path}/assets/dkim_keys/testing.${var.host_domain}_ox2024.txt"
}

locals {
  raw_dkim_testing     = data.local_file.dkim_testing.content
  raw_dkim_system      = data.local_file.dkim_system.content
  strings_dkim_testing = flatten(regexall("\"(.*?)\"", local.raw_dkim_testing))
  strings_dkim_system  = flatten(regexall("\"(.*?)\"", local.raw_dkim_system))
  dkim_testing         = join("", local.strings_dkim_testing)
  dkim_system          = join("", local.strings_dkim_system)
}
