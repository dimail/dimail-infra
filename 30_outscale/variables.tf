variable "env" {
  type = string
}

variable "env_type" {
  type = string
}

variable "global_tags" {
  type = map(string)
}
variable "image_ids" {
  description = "Nom de l'omi utilisée pour les VM"
  type        = map(string)
  default = {
    "bookworm" = "je dirais qu'il vaut mieux une erreur qu'une VM qu'on avait pas prévue"
  }
}
variable "network_range" {
  type = string
}
variable "networks" {
  type = map(object({
    ip_range  = string
    region    = string
    is_public = bool
  }))
}

# variable "peerings" {
#   type = list(tuple([string, string]))
# }

variable "root_region" {
  type = string
}

variable "servers" {
  type = map(object({
    image         = string
    network       = string
    ox_name       = optional(string)
    roles         = list(string)
    size          = string
    sql_server_id = optional(string)
    local_volumes = list(object({
      device_name = string,
      size        = number,
      type        = string
    }))
    volumes = map(object({
      device = string
      mount  = string
      size   = number
      type   = string # n'est pas utilisé dans outscale
    }))
  }))
}

variable "global_security_groups" {
  type = map(list(object({
    proto = string
    from  = string
    port  = string
    name  = string
  })))
  default = {}
}

variable "foreign_spf_ips" {
  type    = list(string)
  default = []
}

variable "host_domain" {
  type = string
}

variable "tech_domain" {
  type = string
}

variable "ttl" {
  type    = string
  default = "600"
}

variable "k8s_fqdn" {
  type    = string
  default = ""
}

variable "config_path" {
  description = "Secrets/config repository base path"
  type        = string
}

variable "inbound_peerings" {
  description = "external outscale networks that connects to you"
  type = map(object({
    net_peering_id = string
    cidr           = string
  }))
  default = {}
}

