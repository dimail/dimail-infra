# Le réseau privé
resource "outscale_net" "network" {
  ip_range = var.network_range

  dynamic "tags" {
    for_each = merge(
      var.global_tags,
      { Name = "network ${var.env}" },
      { "osc.fcu.enable_lan_security_groups" = "activated" }
    )

    content {
      key   = tags.key
      value = tags.value
    }
  }
}

# La passerelle vers Internet (une seule, rattachée au réseau principal)
resource "outscale_internet_service" "internet" {
  dynamic "tags" {
    for_each = merge(
      var.global_tags,
      { Name = "internet ${var.env}" },
    )
    content {
      key   = tags.key
      value = tags.value
    }
  }
}

# On raccorde la passerelle vers Internet à notre réseau privé.
resource "outscale_internet_service_link" "internet_link" {
  internet_service_id = outscale_internet_service.internet.internet_service_id
  net_id              = outscale_net.network.net_id
}

# Les subnets, tous pris dans le réseau privé, distribués par région
resource "outscale_subnet" "subnet" {
  for_each = tomap(var.networks)

  net_id         = outscale_net.network.net_id
  ip_range       = each.value.ip_range
  subregion_name = local.regions[each.value.region]
  dynamic "tags" {
    for_each = merge(
      var.global_tags,
      { Name = "subnet ${each.key} ${var.env}" },
    )
    content {
      key   = tags.key
      value = tags.value
    }
  }
}

# Une table de routage par subnet
resource "outscale_route_table" "route" {
  for_each = tomap(var.networks)
  net_id   = outscale_net.network.net_id
  dynamic "tags" {
    for_each = merge(
      var.global_tags,
      { Name = "route_table ${each.key} ${var.env}" }
    )
    content {
      key   = tags.key
      value = tags.value
    }
  }
}

# On raccorde chaque table de routage à son subnet
resource "outscale_route_table_link" "route_link" {
  for_each       = tomap(var.networks)
  route_table_id = outscale_route_table.route[each.key].route_table_id
  subnet_id      = outscale_subnet.subnet[each.key].subnet_id
}

# Pour les réseaux avec de l'IP publique, on fixe l'accès Internet comme gateway de sortie
resource "outscale_route" "default_route" {
  for_each             = { for name, net in var.networks : name => net if net.is_public }
  gateway_id           = outscale_internet_service.internet.internet_service_id
  destination_ip_range = "0.0.0.0/0"
  route_table_id       = outscale_route_table.route[each.key].route_table_id
}

# Le service NAT habite dans le subnet "main", mais peut servir à tout le monde
# FIXME : Il faudrait un NAT par région, sinon, quand main tombe, c'est la merde.
resource "outscale_nat_service" "nat_service" {
  for_each     = toset(["main"])
  subnet_id    = outscale_subnet.subnet[each.key].subnet_id
  public_ip_id = outscale_public_ip.nat_ip.public_ip_id
  dynamic "tags" {
    for_each = merge(
      var.global_tags,
      { Name = "nat_service ${var.env}" }
    )
    content {
      key   = tags.key
      value = tags.value
    }
  }
}

# Pour les réseaux sans IP publique, la route de sortie, c'est le service de NAT
resource "outscale_route" "nat_route" {
  for_each             = { for name, net in var.networks : name => net if !net.is_public }
  destination_ip_range = "0.0.0.0/0"
  nat_service_id       = outscale_nat_service.nat_service["main"].nat_service_id
  route_table_id       = outscale_route_table.route[each.key].route_table_id
}
