# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.5.2"
  hashes = [
    "h1:JlMZD6nYqJ8sSrFfEAH0Vk/SL8WLZRmFaMUF9PJK5wM=",
    "h1:p99F1AoV9z51aJ4EdItxz/vLwWIyhx/0Iw7L7sWSH1o=",
    "zh:136299545178ce281c56f36965bf91c35407c11897f7082b3b983d86cb79b511",
    "zh:3b4486858aa9cb8163378722b642c57c529b6c64bfbfc9461d940a84cd66ebea",
    "zh:4855ee628ead847741aa4f4fc9bed50cfdbf197f2912775dd9fe7bc43fa077c0",
    "zh:4b8cd2583d1edcac4011caafe8afb7a95e8110a607a1d5fb87d921178074a69b",
    "zh:52084ddaff8c8cd3f9e7bcb7ce4dc1eab00602912c96da43c29b4762dc376038",
    "zh:71562d330d3f92d79b2952ffdda0dad167e952e46200c767dd30c6af8d7c0ed3",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:805f81ade06ff68fa8b908d31892eaed5c180ae031c77ad35f82cb7a74b97cf4",
    "zh:8b6b3ebeaaa8e38dd04e56996abe80db9be6f4c1df75ac3cccc77642899bd464",
    "zh:ad07750576b99248037b897de71113cc19b1a8d0bc235eb99173cc83d0de3b1b",
    "zh:b9f1c3bfadb74068f5c205292badb0661e17ac05eb23bfe8bd809691e4583d0e",
    "zh:cc4cbcd67414fefb111c1bf7ab0bc4beb8c0b553d01719ad17de9a047adff4d1",
  ]
}

provider "registry.terraform.io/outscale/outscale" {
  version     = "1.0.1"
  constraints = "1.0.1"
  hashes = [
    "h1:IRVHu5hKceGzSmifTKPABvYwmNNzpLUOhjN/5m7S6ug=",
    "h1:YmevZ+92kdo7Ebrp5BOAwaWh0rst9Z/yLHutuYoTwIM=",
    "h1:hI9nOhdA2vBfuRNTEso2CT4zCGKTs3Phw09GJiMnuGI=",
    "h1:ktiQK+9DXjsHqwV66pCbbe6vDtOGOpyZClhxXD8UdTk=",
    "h1:rBzvj6CbiVyMg5AELTuPUqdl2epPUEh8L3IcUAZOZP8=",
    "h1:u0i2zcJwIA1/sRMv0Jt9Hlk0PQuQyOTb3yizYeuhD2E=",
    "zh:4a55854d1f28422575ceacf2995729661160b3c4ba2cbe4543afd402dd8708fd",
    "zh:4db2719919c595a1718e33bb9d8523947512248f024bec5fb078b909a319bef4",
    "zh:54e56a0c79e9f1009868444724e338a1ff611bdcd394943c010160c37c4e1c8c",
    "zh:75eb4f8e73fb50429447dcf92dec8a1a0a4434379ecda3147676407b18c2eb7f",
    "zh:9b46323611afc41759ad7e9726a59e29755b6950a47490088c864f20024025fd",
    "zh:db80304571edf51d9e600d9c630d816a100a8a3b92ed00301c3221edc7ecd0ff",
  ]
}

provider "registry.terraform.io/ovh/ovh" {
  version     = "0.51.0"
  constraints = "~> 0.51.0"
  hashes = [
    "h1:/RwSmGxkUoMeEkX7UqyC/JYA5Fioqt7A5Qvq1NrDgTQ=",
    "h1:7dOFLaLlkGzi+xsxSXV/CzmZP7n1kk/Fi3kiUuqwXAI=",
    "h1:DUVoJDMeVHXRkZIH7KaVGb/LAnI6pYaJsPd28Xa6Smg=",
    "h1:DepnYVFcpMFYViDeU3wvEqjNYSngQ1bbGQXLT6Py9Mw=",
    "h1:F+zEYn8hO9uHgBw5VkNUznHBQuC90pLP7xT3Z7yjkDw=",
    "h1:Q/S8VXeVgTj1KB98HNo0gabAJBxgmVxRLO0jSKFMO4E=",
    "h1:S7r5AFUQ8kl5MgsRNwFekD4qrnYnwVRcdSTeewp3T8Y=",
    "h1:WM9PK2XT3/6RxEADkPOPGCL6by7CO1PawcSLzlwO4HE=",
    "h1:hSJH2YseGHYVoGSrRgdBqwwz6oZVGssnbJ2hjtS/Gqs=",
    "h1:mfUEYqr5G9UW/cobcRneQhUFvJvGNjcZHnHCdF2fAlA=",
    "h1:nj9ug1tGCA4TMj1wYSZc+W8Qyt/DXQLhCbdtWmcc1Xo=",
    "h1:oFnnUFkxcAx3MoJ7uumnudwzVaNP033bG2O9Lh0LbiY=",
    "h1:pzwHQ4/iiNeMbvqeT+Mo8N3C5yiLMobVAP9xAbSiI6g=",
    "h1:qKVyUEhgej/ccqQy1+z+SSjreAYAmFaIRx5ApKweLz0=",
    "zh:13c2e4ea4a0f8348f7556862df5ff17733d06cf898c2a646f0c235e4fa87d210",
    "zh:52db2b0371ffaf52cfdacbf8fa6e931c35b5b2c8a65e8f3cb70155f33beda85f",
    "zh:5824a22dd83699f78a4d7689eb905f895999491debb730ba2d15e0f0082c66b0",
    "zh:67426b082025ccf08685f757cca94fc34b8b930e97d17d896b7a4da38ccad00d",
    "zh:6e7b2a62118c969acef1c867dd97db880398f44243145cd5feb2e1935696d0c9",
    "zh:84248f5e3b53fdd175c5c232fcb5b4a6e59f4d33a161b90d70f38bccae6e2823",
    "zh:937c3d56f32e7aaa2fe05d58fc54ff8c348a8ab3718462dc2ea1f9a9ba739bd9",
    "zh:9630cf1e8c65efb7e7a9ccab3a152c03f002ba47a8ce058bd98be34419137746",
    "zh:b7d51f82ea1f73dbdabdd82d346015e9c1cde0dd334ed49150b35ca1843ba0b7",
    "zh:bd7771ff7da38f6a7772cf774a573a184010a01de170d8bb30740c7d3d45ddd3",
    "zh:c4bffcfc33b82765003e8f9ce22b3bc100ec28c2380eacb5a38a272c6bd2636e",
    "zh:cd08c5165ed1366d2c84b8e01005b7e42cceb3b4229d3cf31ea9da910029fe15",
    "zh:ea2424e1f8848da2283db82e93b5c8fb5f44170a0e62ae991a935d4aa38ce4b0",
    "zh:f9bf537f8be3626858d944b25995adfa513f9fc9f73003a7ce0461dbec0e78b4",
  ]
}
