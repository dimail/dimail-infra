locals {
  regions = {
    main   = "${var.root_region}a"
    backup = "${var.root_region}b"
  }

  vm_type = {
    small   = "tinav7.c1r1p2"
    medium1 = "tinav7.c1r2p2"
    medium  = "tinav7.c2r4p2"
    large   = "tinav7.c4r4p2"
    xlarge  = "tinav7.c8r64p2"
  }

  list_ssh_keys_names = fileset("${var.config_path}/assets/ssh_keys/", "*")
  list_ssh_keys       = [for item in local.list_ssh_keys_names : file("${var.config_path}/assets/ssh_keys/${item}")]
  ssh_authorized_keys = zipmap(
    local.list_ssh_keys_names,
    local.list_ssh_keys,
  )
}
