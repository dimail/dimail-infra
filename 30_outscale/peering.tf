# Accepter les peerings vers l'extérieur
resource "outscale_net_peering_acceptation" "inbound_peerings" {
  for_each       = var.inbound_peerings
  net_peering_id = each.value.net_peering_id
}

# Un peu de calcul pour associer le subnet des peerings et les route tables
locals {
  peering_routes = { for route in setproduct(
    [for name, network in var.networks : name],
    [for name, peering in outscale_net_peering_acceptation.inbound_peerings : [name, peering]],
    ) :
    "${route[1][0]}_${route[0]}" => {
      network_name : route[0],
      net_peering_id : route[1][1].net_peering_id,
      net_peering_cidr : route[1][1].source_net[0].ip_range
    }
  }
}

# Les routes qui permettent aux subnets de discuter avec les peerings.
resource "outscale_route" "external_peering_route" {
  for_each             = local.peering_routes
  route_table_id       = outscale_route_table.route[each.value.network_name].id
  destination_ip_range = each.value.net_peering_cidr
  net_peering_id       = each.value.net_peering_id
}
