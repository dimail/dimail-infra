#!/usr/bin/env python3

import boto3
import sys
import os
import json
import hcl
from pprint import pprint


class Inventory():
  def __init__(self):
    self.inventory = {"all": {"hosts": []}}

  def to_json(self):
    return json.dumps(self.inventory)


def is_role_tag(tag):
  return tag["Key"] == "Roles"


def get_tag(stuff, tag_name):
  for tag in stuff["Tags"]:
    if tag["Key"] == tag_name:
      return tag["Value"]


def connection():
  access_key = os.environ['OUTSCALE_ACCESSKEYID']
  secret_key = os.environ['OUTSCALE_SECRETKEYID']
  region = os.environ['OUTSCALE_REGION']

  return boto3.client('ec2',
                      aws_access_key_id=access_key,
                      aws_secret_access_key=secret_key,
                      region_name=region,
                      endpoint_url='https://fcu.{0}.outscale.com'.format(region)
                      )


def get_instances_config(outscale_fcu):
  filters = [{'Name': 'tag:env', 'Values': [os.environ['DIMAIL_ENV']]}]
  volumes = outscale_fcu.describe_volumes(Filters=filters)
  instances = outscale_fcu.describe_instances(Filters=filters)

  return build_inventory(instances, volumes)


def build_inventory(instances, volumes):
  inventory = Inventory()
  hosts = []

  inventory.inventory["all"] = {
    "vars": {
#      "hosting": "outscale",
    }
  }

  for group in ["mecol", os.environ["DIMAIL_HOSTING"], os.environ["DIMAIL_ENV"]]:
    inventory.inventory[group] = {"hosts": hosts}

  inventory.inventory["_meta"] = {"hostvars": {}}
  #  pprint(volumes)
  volume_ids = {}
  for vol in volumes["Volumes"]:
    vid = vol["VolumeId"]
    volume_ids[vid] = vol

  for reservation in instances["Reservations"]:
    for instance in reservation["Instances"]:
      if instance["State"]["Name"] != "running":
        continue

      name = get_tag(instance, "Name")
      hosts.append(name)
      hostvars = {
        "private_ip": instance["PrivateIpAddress"],
        "ansible_ssh_host": get_tag(instance, "priv_fqdn"),
        "tech_fqdn": get_tag(instance, "tech_fqdn"),
        "priv_fqdn": get_tag(instance, "priv_fqdn"),
      }
      sql_server_id = get_tag(instance, "sql_server_id")
      if sql_server_id != "" and sql_server_id != None:
        hostvars["sql_server_id"] = sql_server_id

      # populate env and hostname
      hostvars["env"] = get_tag(instance, "env")
      hostvars["hostname"] = name

      # populate roles in inventory
      roles = get_tag(instance, "Roles").split(",")
      for role in roles:
        params = []
        if ":" in role:
          role, txt = role.split(":",2)
          params = txt.split("+")
          for param in params:
            if "=" in param:
              key, value = param.split("=",2)
              hostvars[f"{role}_{key}"] = value
        if role not in inventory.inventory:
          inventory.inventory[role] = {"hosts": []}
        if len(params) > 0:
          hostvars[f"{role}_params"] = params
        inventory.inventory[role]["hosts"].append(name)

      # check for public IP
      is_public = get_tag(instance, "Is_public")
      if is_public is not None and is_public == "no":
        hostvars["is_public"] = False
      else:
        hostvars["is_public"] = True

      # populate blocks in inventory from tags
      blocks = instance["BlockDeviceMappings"]
      hostvars["block"] = []
      for block in blocks:
        device_name = block["DeviceName"]
        vid = block["Ebs"]["VolumeId"]
        if vid not in volume_ids:
          continue
        mount_point = get_tag(volume_ids[vid], "mount")

        hostvars["block"].append({
          "device": device_name,
          "mount": mount_point,
        })

      inventory.inventory["_meta"]["hostvars"][name] = hostvars
  return inventory


if __name__ == '__main__':
  DIMAIL_CONFIG_PATH = os.environ['DIMAIL_CONFIG_PATH']
  DIMAIL_ENV = os.environ['DIMAIL_ENV']

  if len(sys.argv) == 1 or sys.argv[1] == '--list':
    outscale_fcu = connection()
    inventory = get_instances_config(outscale_fcu)
    sys.stdout.write(inventory.to_json())
  else:
    sys.stdout.write("{}")
