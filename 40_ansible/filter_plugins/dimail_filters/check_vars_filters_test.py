import pytest
from .check_vars_filters import *
from copy import deepcopy

check_vars = FilterModule()

@pytest.fixture
def client_domain():
    return {
        "name": "test_name",
        "delivery": "virtual",
        "features": ['mailbox'],
        "dkim_selector": "selector",
        "cert_name": "cert",
    }

@pytest.fixture
def platforms():
    platforms={}
    for i in ['pf1', 'pf2', 'pf3', 'pf4']:
        platforms[i] = {
            "tech_domain": {
                "name": f"{i}.tech.example.com",
                "cert_name": f"cert_tech_{i}",
            },
            "host_domain": {
                "name": f"{i}.host.example.com",
            },
        }
    return platforms

@pytest.fixture
def client_contexts():
    return [{
      "id": "1",
      "cluster": "cluster_pf1",
      "name": "dinum",
      "tags": ["early_adopter", "rapid_release"],
      "access_combination": "groupware_premium",
      "admin_username": "ox_admin_username",
      "admin_password": "ox_admin_password",
    }]

def test_empty_client_domain():
    # given
    client_domain = {}

    # when
    with pytest.raises(AnsibleFilterError) as context:
        check_vars.check_client_domain(client_domain)

    assert "The client_domain has no name" in str(context.value)

    client_domain["name"] = "example.com"
    with pytest.raises(AnsibleFilterError) as context:
        check_vars.check_client_domain(client_domain)
    assert "missing 'features'" in str(context.value)

    client_domain["features"] = [ "toto" ]
    with pytest.raises(AnsibleFilterError) as context:
        check_vars.check_client_domain(client_domain)
    assert "missing 'delivery'" in str(context.value)

def test_missing_mailbox_property():
    # given
    client_domain = {
        "name": "test_name",
        "delivery": "virtual",
        "features": ['mailbox'],
    }

    # when
    with pytest.raises(AnsibleFilterError) as context:
        check_vars.check_client_domain(client_domain)

    assert "missing 'dkim_selector'" in str(context.value)

def test_client_domains_has_nothing_wrong():
    # given
    client_domain = {
        "name": "mail.test.example.com",
        "delivery": "virtual",
        "features": ["mailbox", "mx", "webmail"],
        "ox_cluster": "oximap",
        "context_id": "1",
        "cert_name": "client_example_com",
        "dkim_selector": "example_selector"
    }
    expected = client_domain.copy()
    expected["__dimail_check"] = "ok"

    # when
    result = check_vars.check_client_domain(client_domain)

    # then
    assert result == expected

def test_missing_name_tech_domain():
    # given
    tech_domain = {
        "cert_name": "cert_name",
        "dkim_selector": "dkim_selector",
    }

    # when
    with pytest.raises(AnsibleFilterError) as context:
        check_vars.check_tech_domain(tech_domain)

    # then
    assert "missing property for tech_domain: `name`" in str(context.value)

def test_missing_cert_name_tech_domain():
    # given
    tech_domain = {
        "name": "tech.example.com",
        "dkim_selector": "dkim_selector",
    }

    # when
    with pytest.raises(AnsibleFilterError) as context:
        check_vars.check_tech_domain(tech_domain)

    # then
    assert "missing property for tech_domain: `cert_name`" in str(context.value)

def test_missing_dkim_selector_tech_domain():
    # given
    tech_domain = {
        "name": "tech.example.com",
        "cert_name": "cert_name",
    }

    # when
    with pytest.raises(AnsibleFilterError) as context:
        check_vars.check_tech_domain(tech_domain)

    # then
    assert "missing property for tech_domain: `dkim_selector`" in str(context.value)

def test_proxy_dependency_proxy_to(client_domain):
    proxy_to = deepcopy(client_domain)
    proxy_to['proxy_to']='some_platform'

    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_client_domain(proxy_to)
    assert "is missing" in str(exception.value) 

    proxy_to['proxy_context_id']="1"

    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_client_domain(proxy_to)
    assert "is missing" in str(exception.value)

    proxy_from = deepcopy(client_domain)
    proxy_from['proxy_from']='some_platform'
    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_client_domain(proxy_from)
    assert "is missing" in str(exception.value)
    
    proxy_from['proxy_context_id']="1"

    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_client_domain(proxy_from)
    assert "is missing" in str(exception.value)

def test_check_platforms(platforms,client_contexts):
    env_name = 'pf1'

    # check_platforms return the platforms object unmodified
    checked_platforms = check_vars.check_platforms(platforms, client_contexts, env_name)
    assert checked_platforms==platforms 

    # check_platforms detects a missing proxy_to dependency
    pfs = deepcopy(platforms)
    pfs['pf1']['proxy_to']='pf3'
    pfs['pf1']['context_id_map']={"1":"1"} 
    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_platforms(pfs, client_contexts, env_name)
    assert "platform 'pf1' is missing 'ox_cluster_map'" in str(exception.value)

    # check_platforms detects a missing proxy_to platform
    pfs = deepcopy(platforms)
    pfs['pf1']['proxy_to']='pf5' # only pf1 to pf4 exist in fixture
    pfs['pf1']['context_id_map']={"1":"1"}
    pfs['pf1']['ox_cluster_map']={"cluster_pf1":"cluster_pf2"}
    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_platforms(pfs, client_contexts, env_name)
    assert "'pf5' referenced as 'proxy_to' for platform 'pf1' does not exist" in str(exception.value)

    # check_platforms detects a missing proxy_from platform
    pfs = deepcopy(platforms)
    pfs['pf1']['proxy_from']='pf5' # only pf1 to pf4 exist in fixture
    pfs['pf1']['context_id_map']={"1":"1"}
    pfs['pf1']['ox_cluster_map']={"cluster_pf1":"cluster_pf2"}
    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_platforms(pfs,client_contexts, env_name)
    assert "'pf5' referenced as 'proxy_from' for platform 'pf1' does not exist" in str(exception.value)

    # check_platforms detects a missing context_id in client_context with proxy_to
    pfs = deepcopy(platforms)
    pfs['pf2']['proxy_to']='pf1' 
    pfs['pf2']['context_id_map']={"1":"2"}
    pfs['pf2']['ox_cluster_map']={"cluster_pf2":"cluster_pf1"}
    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_platforms(pfs,client_contexts, env_name)
    assert "cluster_id_map for platform 'pf2' contains context_id '2' that does not exist in platform 'pf1'" in str(exception.value)

    # check_platforms detects a missing context_id in client_context with proxy_from
    pfs = deepcopy(platforms)
    pfs['pf1']['proxy_from']='pf2'
    pfs['pf1']['context_id_map']={"1": "2", "2":"1"}
    pfs['pf1']['ox_cluster_map']={"cluster_pf1":"cluster_pf2"}
    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_platforms(pfs,client_contexts, env_name)
    assert "cluster_id_map for platform 'pf1' contains context_id '2' that does not exist in platform 'pf1'" in str(exception.value)

    # check_platforms detects a missing ox_cluster in client_context with proxy_to
    pfs = deepcopy(platforms)
    pfs['pf2']['proxy_to']='pf1'
    pfs['pf2']['context_id_map']={"1":"1"}
    pfs['pf2']['ox_cluster_map']={"cluster_pf2":"another_cluster_pf1"}
    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_platforms(pfs,client_contexts, env_name)
    assert "ox_cluster_map for platform 'pf2' contains ox_cluster 'another_cluster_pf1' that does not exist in platform 'pf1'" in str(exception.value)

    # check_platforms detects a missing ox_cluster in client_context with proxy_from
    pfs = deepcopy(platforms)
    pfs['pf1']['proxy_from']='pf2'
    pfs['pf1']['context_id_map']={"1":"1"}
    pfs['pf1']['ox_cluster_map']={"another_cluster_pf1":"cluster_pf2"}
    with pytest.raises(AnsibleFilterError) as exception:
        check_vars.check_platforms(pfs,client_contexts, env_name)
    assert "ox_cluster_map for platform 'pf1' contains context_id 'another_cluster_pf1' that does not exist in platform 'pf1'" in str(exception.value)
