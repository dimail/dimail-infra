from .cert_info_filters import FilterModule as cert_info_filters
from .check_dkim_keys import FilterModule as check_dkim_keys
from .check_vars_filters import FilterModule as check_vars_filters
from .compute_mail_domain_filters import FilterModule as compute_mail_domain_filters
from .compute_api_hosts import FilterModule as compute_api_hosts

class FilterModule(object):
    def filters(self):
        filters = {}
        filters.update(cert_info_filters().filters())
        filters.update(check_dkim_keys().filters())
        filters.update(check_vars_filters().filters())
        filters.update(compute_mail_domain_filters().filters())
        filters.update(compute_api_hosts().filters())
        return filters

__all__ = [
    FilterModule,
    cert_info_filters,
    check_dkim_keys,
    check_vars_filters,
    compute_api_hosts,
    compute_mail_domain_filters,
]

        
