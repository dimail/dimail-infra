from ansible.errors import AnsibleFilterError
import copy

class FilterModule(object):
    def filters(self):
        return {
            'dimail_compute_mail_domain': self.compute_mail_domain,
        }

    def compute_proxy(self, client_domain, platforms, env_name):
        client_domain = copy.deepcopy(client_domain)
        if 'no_proxy' in client_domain and client_domain['no_proxy']==True:
            return client_domain

        domain_name = client_domain["name"]
        # apply platform proxy_from and proxy_to to client_domain if needed
        for (pfname, platform) in platforms.items():
            if 'proxy_to' in platform and client_domain['platform']==pfname:
                client_domain['proxy_to'] = client_domain.get('proxy_to', platform['proxy_to'])
                if "webmail" in client_domain["features"]:
                    client_domain['proxy_ox_cluster'] = client_domain.get('proxy_ox_cluster', platform['ox_cluster_map'][client_domain['ox_cluster']])
                    client_domain['proxy_context_id'] = client_domain.get('proxy_context_id', platform['context_id_map'][client_domain['context_id']])
            if 'proxy_from' in platform and client_domain['platform']==pfname:
                client_domain['proxy_from'] = client_domain.get('proxy_from', platform['proxy_from'])
                if "webmail" in client_domain["features"]:
                    client_domain['proxy_ox_cluster'] = client_domain.get('proxy_ox_cluster', platform['ox_cluster_map'][client_domain['ox_cluster']])
                    client_domain['proxy_context_id'] = client_domain.get('proxy_context_id', platform['context_id_map'][client_domain['context_id']])
        
        if 'proxy_to' in client_domain or 'proxy_from' in client_domain:
            if 'webmail' in client_domain["features"]:
                if not 'proxy_context_id' in client_domain:
                    raise AnsibleFilterError(f"error: on domain {domain_name}, i have webmail and a proxy, i need 'proxy_context_id'")
                if not 'proxy_ox_cluster' in client_domain:
                    raise AnsibleFilterError(f"error: on domain {domain_name}, i have webmail and a proxy, i need 'proxy_ox_cluster'")

        # we can't do this consistency check before
        if 'proxy_to' in client_domain and 'proxy_from' in client_domain:
            raise AnsibleFilterError(f"error: proxy_to and proxy_from for {client_name} ? Only Chuck Norris can make it happen.")
        
        # if we think about it, proxy_to is a nice shorcut for platform and proxy_from swap.
        if 'proxy_to' in client_domain:
            client_domain['platform'], client_domain['proxy_from'] = client_domain['proxy_to'], client_domain['platform']
            client_domain.pop('proxy_to')
            if 'webmail' in client_domain["features"]:
                client_domain['proxy_context_id'], client_domain['context_id'] = client_domain['context_id'], client_domain['proxy_context_id']
                client_domain['proxy_ox_cluster'], client_domain['ox_cluster'] = client_domain['ox_cluster'], client_domain['proxy_ox_cluster']

        return client_domain

    def master(self, masters, platform):
        for master in masters:
            if master['platform']==platform:
                return master
        raise AnsibleFilterError(f"error: master password is missing for {platform}, see group_vars/all/masters.yml")
 
    def compute_mail_domain(self, client_domain, platforms, masters, env_name):
        if "__dimail_check" not in client_domain:
            raise AnsibleFilterError("error: client_domain has not been filtered with dimail_check_client_domain")

        platform = platforms[env_name]
        name = client_domain['name']
        features = client_domain['features']
        delivery = client_domain["delivery"]
        # groupvars_platform = client_domain["platform"]

        client_domain = self.compute_proxy(client_domain, platforms, env_name)

        # since we process any platform client_domain, unrelated client_domain is to be removed thanks to jinja2 flatten filter
        if not ('proxy_from' in client_domain and client_domain['proxy_from'] == env_name or client_domain['platform'] == env_name):
            return []

        # if client_domain targets env_name n proxy_from, we do relay emails to client_domain's platform
        # print(f"Le client domain: {client_domain}")
        if 'proxy_from' in client_domain and client_domain['proxy_from'] == env_name:
            delivery = "relay"
            transport_domain = platforms[client_domain['platform']]['tech_domain']['name']
            client_domain['transport'] = f'smtp:[smtp.{transport_domain}]'
            if 'webmail' in features:
                client_domain['ox_cluster'] = client_domain['proxy_ox_cluster']
                client_domain['context_id'] = client_domain['proxy_context_id']

        mail_domain = {
            "name": name,
            "delivery": delivery,
            "features": features,
            # "groupvars_platform": groupvars_platform,
        }

        if 'mailbox' in features:
            mail_domain['dkim_selector'] = client_domain['dkim_selector']
            mail_domain['dkim_private_key_file'] = client_domain['dkim_private_key_file']
            mail_domain['dkim_public_key_file'] = client_domain['dkim_public_key_file']
            if 'imap_domain' in client_domain:
                mail_domain['imap_domain'] = client_domain['imap_domain']
            else:
                mail_domain['imap_domain'] = 'imap.' + name
            if 'smtp_domain' in client_domain:
                mail_domain['smtp_domain'] = client_domain['smtp_domain']
            else:
                mail_domain['smtp_domain'] = 'smtp.' + name

        if delivery == "relay":
            mail_domain['transport'] = client_domain['transport']

        if 'webmail' in features:
            mail_domain['ox_cluster'] = client_domain['ox_cluster']
            mail_domain['context_id'] = client_domain['context_id']
            if 'webmail_domain' in client_domain:
                mail_domain['webmail_domain'] = client_domain['webmail_domain']
            else:
                mail_domain['webmail_domain'] = 'webmail.' + name

        if 'cert_name' in client_domain:
            mail_domain['cert_name'] = client_domain['cert_name']

        if client_domain['platform'] == env_name and 'proxy_from' in client_domain:
            platform_from = client_domain['proxy_from']
            master_from = self.master(masters, platform_from)
            proxy = {
                'host': 'imap.' + platforms[master_from['platform']]['tech_domain']['name'],
                'master': master_from['master'],
                'pass': master_from['pass'],
            }
            mail_domain['proxy'] = proxy

        return mail_domain
