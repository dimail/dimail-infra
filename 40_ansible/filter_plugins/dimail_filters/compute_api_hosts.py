from ansible.errors import AnsibleFilterError
import copy

class FilterModule(object):
    def filters(self):
        return {
            'compute_api_hosts': self.compute_api_hosts,
            'compute_api_others': self.compute_api_others,
        }

    def compute_api_others(self, api_passwords, platforms, myself):
        result = {}
        for env in api_passwords.keys():
            if env == myself:
                continue
            login = api_passwords[env]["user"]
            passw = api_passwords[env]["password"]
            host_domain = platforms[env]["host_domain"]["name"]
            result[env] = f"https://{login}:{passw}@api.{host_domain}"
        return result

    def compute_api_hosts(self, useless, hosts):
        result = {}
        for name in hosts:
            host = hosts[name]
            if not "mecol" in host["group_names"]:
                continue
            hostname = host["hostname"]
            ssh_name = host["private_ip"]
            #if "imap_server" in host["group_names"]:
            #    self.add_role(result, hostname, ssh_name, "imap")
            if "imap_master" in host["group_names"]:
                self.add_role(result, hostname, ssh_name, "imap")
            #if "imap_slave" in host["group_names"]:
            #    self.add_role(result, hostname, ssh_name, "imap")
            if "smtp" in host["group_names"]:
                self.add_role(result, hostname, ssh_name, "smtp")
            if "smtp" in host["group_names"] and not "backup_role" in host["group_names"]:
                self.add_role(result, hostname, ssh_name, "dkim")
            if "ox" in host["group_names"] and "prov" in host["ox_params"]:
                cluster = host["ox_name"]
                self.add_role(result, hostname, ssh_name, f"ox={cluster}")
            if "webfront" in host["group_names"]:
                self.add_role(result, hostname, ssh_name, "webmail")
            if "cert_manager" in host["group_names"]:
                self.add_role(result, hostname, ssh_name, "cert_manager")
            if "api_server" in host["group_names"]:
                self.add_role(result, hostname, ssh_name, "api")
            if "monitor" in host["group_names"]:
                self.add_role(result, hostname, ssh_name, "monitor")
            if "cert_user" in host["group_names"]:
                self.add_role(result, hostname, ssh_name, "cert_user")
            # print(f"{result}")

        elements = []
        for hostname in sorted(result.keys()):
            ssh_url = result[hostname]["ssh_url"]
            ssh_args = ",".join(result[hostname]["ssh_args"])
            roles = ",".join(result[hostname]["roles"])
            item = f"{hostname};{ssh_url};{ssh_args};{roles}"
            # print(f"{item}")
            elements.append(item)

        return f"{elements}"

    def add_role(self, result, hostname, ssh_name, role):
        if hostname not in result:
            result[hostname] = {
                "ssh_url": f"ssh://api@{ssh_name}",
                "ssh_args": [],
                "roles": []
            }
        if role not in result[hostname]["roles"]:
            result[hostname]["roles"].append(role)


