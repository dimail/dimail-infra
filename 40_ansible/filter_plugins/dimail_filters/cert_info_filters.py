from ansible.errors import AnsibleFilterError


class FilterModule(object):
    def filters(self):
        return {
            'dimail_cert_info_tech_domain': self.cert_info_tech_domain,
            'dimail_cert_info_host': self.cert_info_host,
            'dimail_cert_info_domain': self.cert_info_domain,
            'dimail_cert_info_webfront': self.cert_info_webfront,
        }

    def cert_info_tech_domain(self, tech_domain, groups, hostvars):
        if '__dimail_check' not in tech_domain:
            raise AnsibleFilterError("error: tech_domain has not been filtered with dimail_check_tech_domain")
        subdomains = []
        if 'imap_master' in groups or 'imap_slave' in groups or 'imap_server' in groups:
            subdomains.append('imap')
        if 'smtp' in groups:
            subdomains.append('smtp')
            subdomains.append('mx')

        oidc = False
        
        for hostvar in hostvars.values():
          if 'ox_params' in hostvar:
            if 'oidc' in hostvar['ox_params']:
              oidc = True
        if oidc:
            subdomains.append('oidc')

        # Il n'y a pas de certificat SSL sur webmail.<tech_domain>
        #if 'ox' in groups:
        #    subdomains.append('webmail')
        return {
            'domains': [subdomain + '.' + tech_domain['name']
                        for subdomain in subdomains],
            'domain': tech_domain['name'],
            'cert_name': tech_domain['cert_name'],
        }

    def cert_info_host(self, cert_host_name, hostvars, host_domain):
        return {
            'domains': [hostvars[cert_host_name]['hostname'] + '.' + host_domain['name']],
            'domain': host_domain['name'],
            'cert_name': 'tech_' + hostvars[cert_host_name]['hostname'],
        }

    def cert_info_webfront(self, cert_host_name, host_domain, appli_name):
        domain = appli_name + '.' + host_domain['name']
        return {
                'domains': [domain],
                'domain': domain,
                'cert_name': 'webfront_' + appli_name,
        }

    def cert_info_domain(self, domain):
        domains = []
        if 'webmail_domain' in domain:
            domains.append(domain['webmail_domain'])

        for prop in [ 'imap_domain', 'smtp_domain' ]:
            domains.append(domain[prop])

        domains = sorted(set(domains))

        return {
            'domain': domain['name'],
            'cert_name': domain['cert_name'],
            'domains': domains,
        }
