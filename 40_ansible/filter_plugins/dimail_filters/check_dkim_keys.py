from ansible.errors import AnsibleFilterError
import os
import re



class FilterModule(object):
    dkim_root = ""
    domain = ""
    dkim_selector = ""
    dimail_env = ""

    def filters(self):
        return {
            'dimail_check_dkim_key': self.check_dkim_key,
        }

    def keyfilename(self):
        return f"{self.dkim_root}/{self.domain}_{self.dkim_selector}.key"

    def txtfilename(self):
        return f"{self.dkim_root}/{self.domain}_{self.dkim_selector}.txt"

    def check_dkim_key(self, client_domain, dkim_root):
        self.dkim_root = dkim_root
        dimail_env = client_domain['platform']
        if 'dkim_selector' in client_domain:
            self.domain = client_domain["name"]
            self.dkim_selector = client_domain["dkim_selector"]
            self.dimail_env = dimail_env

            self.check_dkim_key_is_present(client_domain, dimail_env)
            client_domain["dkim_private_key_file"] = self.keyfilename()
            client_domain["dkim_public_key_file"] = self.txtfilename()
            self.check_dkim_key_is_encrypted(client_domain, dimail_env)
            # TODO: self.check_dkim_key_is_valid(client_domain, dimail_env)
        return client_domain

    def check_dkim_key_is_present(self, client_domain, dimail_env):
            haskey = os.path.isfile(self.keyfilename())
            if not haskey:
                raise AnsibleFilterError(f"dkim key missing, expected file {self.keyfilename()} not found")
            hastxt = os.path.isfile(self.txtfilename())
            if not hastxt:
                raise AnsibleFilterError(f"dkim txt missing, expected file {self.txtfilename()} not found")

    def check_dkim_key_is_encrypted(self, client_domain, dimail_env):
        with open(self.keyfilename()) as file:
            lines = file.readlines()
            head = lines[0]
            body = lines[1:-1]
            if head != '$ANSIBLE_VAULT;1.1;AES256\n':
                raise AnsibleFilterError(f"!!! DANGER !!! key {self.keyfilename()} is not an ansible-vault file (bad header)")
            for line in body:
                if len(line) != 81:
                    raise AnsibleFilterError(f"!!! DANGER !!! key {self.txtfilename()} is not an ansible-vault file (bad body)")
