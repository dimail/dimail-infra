import pytest
from .check_dkim_keys import *

check_dkim_keys = FilterModule()

@pytest.fixture
def good_client_domain():
    return {
        "name": "good.example.com",
        "dkim_selector": "selector",
        "platform": "good_test_env",
    }

@pytest.fixture
def bad_client_domain():
    return {
        "name": "bad.example.com",
        "dkim_selector": "selector",
        "platform": "bad_test_env",
    }

def test_good_dkim_key(good_client_domain):
    check_dkim_keys.check_dkim_key(good_client_domain, 'check_dkim_keys')

def test_wrong_selector_dkim_key(good_client_domain):
    # given
    good_client_domain['dkim_selector']="bad_selector" 
    # when
    with pytest.raises(AnsibleFilterError) as context:
        check_dkim_keys.check_dkim_key(good_client_domain, 'check_dkim_keys')
    # then
    assert "dkim key missing, expected file check_dkim_keys/good.example.com_bad_selector.key not found" in str(context.value)

def test_wrong_domain_dkim_key(good_client_domain):
    # given
    good_client_domain['name']="very_bad.example.com"

    # when
    with pytest.raises(AnsibleFilterError) as context:
        check_dkim_keys.check_dkim_key(good_client_domain, 'check_dkim_keys')
    # then
    assert "dkim key missing, expected file check_dkim_keys/very_bad.example.com_selector.key not found" in str(context.value)

def test_bad_env_dkim_key(bad_client_domain):
    # when
    with pytest.raises(AnsibleFilterError) as context:
        check_dkim_keys.check_dkim_key(bad_client_domain, 'check_dkim_keys')
    # then
    assert "!!! DANGER !!! key check_dkim_keys/bad.example.com_selector.key is not an ansible-vault file (bad header)" in str(context.value)

# class FilterModule(object):
#     def filters(self):
#         return {}
