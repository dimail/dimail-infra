import unittest, pytest
from .compute_mail_domain_filters import *

compute_mail_domain = FilterModule()

@pytest.fixture
def mailbox_client_domain():
    return {
        "__dimail_check": "ok",
        "name": "mail.test.example.com",
        "delivery": "virtual",
        "features": ["mailbox", "mx", "webmail"],
        "ox_cluster": "oximap",
        "context_id": "1",
        "cert_name": "client_example_com",
        "dkim_selector": "example_selector",
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        "platform": "pf1"
    }

@pytest.fixture
def relay_client_domain():
    return {
        "__dimail_check": "ok",
        "name": "mail.test.example.com",
        "delivery": "relay",
        "features": ["mailbox", "mx", "webmail"],
        "transport": "mx.example.com",
        "ox_cluster": "oximap",
        "context_id": "1",
        "cert_name": "client_example_com",
        "dkim_selector": "example_selector",
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        "platform": "pf2"
    } 

@pytest.fixture
def raw_client_domain():
    return {
        "name": "mail.test.example.com",
        "delivery": "virtual",
        "features": ["mailbox", "mx", "webmail"],
        "ox_cluster": "oximap",
        "context_id": "1",
        "cert_name": "client_example_com",
        "dkim_selector": "example_selector",
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        "platform": "pf1"
    }

@pytest.fixture
def platforms():
    return {
        "pf1": {
            "tech_domain": {
                "name": "pf1.tech.example.com",
                "cert_name": "tech_domain_pf1"
            },
            "host_domain": {
                "name": "pf1.host.example.com",
            },
        },
        "pf2": {
            "tech_domain": {
                "name": "pf2.tech.example.com",
                "cert_name": "tech_domain_pf2",
            },
            "host_domain": {
                "name": "pf2.host.example.com",
            }
        }
    }        

@pytest.fixture
def masters():
    return [
        {   
            "platform": "pf1",
            "master": "pf1_master_user",
            "pass": "pf1_master_pass",
        },
        {
            "platform": "pf2",
            "master": "pf2_master_user",
            "pass": "pf2_master_pass",
        },
    ]

@pytest.fixture
def pf1():
    return 'pf1'

@pytest.fixture
def pf2():
    return 'pf2'

def test_mail_domain(mailbox_client_domain, platforms, masters, pf1, pf2):
    mail_domain_on_target_pf = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf1)
    assert mail_domain_on_target_pf == {
        'cert_name': 'client_example_com',
        'context_id': '1',
        'delivery': 'virtual',
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'features': ['mailbox', 'mx', 'webmail'],
        'imap_domain': 'imap.mail.test.example.com',
        'name': 'mail.test.example.com',
        'ox_cluster': 'oximap',
        'smtp_domain': 'smtp.mail.test.example.com',
        'webmail_domain': 'webmail.mail.test.example.com',
    }

    mail_domain_not_on_target_pf = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf2)
    assert mail_domain_not_on_target_pf == []
    

def test_relay_domain(relay_client_domain, platforms, masters, pf1, pf2):
    mail_domain_on_target_pf = compute_mail_domain.compute_mail_domain(relay_client_domain, platforms, masters, pf2)
    assert mail_domain_on_target_pf == {
        'cert_name': 'client_example_com',
        'context_id': '1',
        'delivery': 'relay',
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'features': ['mailbox', 'mx', 'webmail'],
        'imap_domain': 'imap.mail.test.example.com',
        'name': 'mail.test.example.com',
        'ox_cluster': 'oximap',
        'transport': 'mx.example.com',
        'smtp_domain': 'smtp.mail.test.example.com',
        'webmail_domain': 'webmail.mail.test.example.com',
    }

    mail_domain_not_on_target_pf = compute_mail_domain.compute_mail_domain(relay_client_domain, platforms, masters, pf1)
    assert mail_domain_not_on_target_pf == []

def test_proxy_to_domain(mailbox_client_domain, platforms, masters, pf1, pf2):
    mailbox_client_domain['proxy_to'] = pf2
    mailbox_client_domain['proxy_context_id'] = '2'
    mailbox_client_domain['proxy_ox_cluster'] = 'single-server'
    mailbox_client_domain['platform'] = pf1
    from_platform = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf1)
    to_platform = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf2)
    assert from_platform == {
        'name': 'mail.test.example.com',
        'delivery': 'relay',
        'features': ['mailbox', 'mx', 'webmail'],
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'imap_domain': 'imap.mail.test.example.com', # FIXME Added
        'smtp_domain': 'smtp.mail.test.example.com', # FIXME Added
        'cert_name': 'client_example_com',
        'transport': 'smtp:[smtp.pf2.tech.example.com]',
        'ox_cluster': 'oximap',
        'context_id': '1',
        'webmail_domain': 'webmail.mail.test.example.com',
    }
    assert to_platform == {
        'name': 'mail.test.example.com',
        'delivery': 'virtual',
        'features': ['mailbox', 'mx', 'webmail'],
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'imap_domain': 'imap.mail.test.example.com',
        'smtp_domain': 'smtp.mail.test.example.com',
        'cert_name': 'client_example_com',
        'ox_cluster': 'single-server',
        'context_id': '2',
        'webmail_domain': 'webmail.mail.test.example.com',
        'proxy': {
            'host': 'imap.pf1.tech.example.com',
            'master': 'pf1_master_user',
            'pass': 'pf1_master_pass'
        },
    }

def test_proxy_from_domain(mailbox_client_domain, platforms, masters, pf1, pf2):
    mailbox_client_domain['proxy_from'] = pf1
    mailbox_client_domain['proxy_ox_cluster'] = 'single-server'
    mailbox_client_domain['proxy_context_id'] = '2'
    mailbox_client_domain['platform'] = pf2
    from_platform = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf1)
    to_platform = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf2)
    assert from_platform == { # pf1
        'name': 'mail.test.example.com',
        'delivery': 'relay',
        'features': ['mailbox', 'mx', 'webmail'],
        'dkim_selector': 'example_selector',
        'imap_domain': 'imap.mail.test.example.com', # FIXME Added
        'smtp_domain': 'smtp.mail.test.example.com', # FIXME Added
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'cert_name': 'client_example_com',
        'transport': 'smtp:[smtp.pf2.tech.example.com]',
        'ox_cluster': 'single-server',
        'context_id': '2',
        'webmail_domain': 'webmail.mail.test.example.com',
    }
    assert to_platform == { # pf2
        'name': 'mail.test.example.com',
        'delivery': 'virtual',
        'features': ['mailbox', 'mx', 'webmail'],
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'imap_domain': 'imap.mail.test.example.com',
        'smtp_domain': 'smtp.mail.test.example.com',
        'ox_cluster': 'oximap',
        'context_id': '1',
        'webmail_domain': 'webmail.mail.test.example.com',
        'cert_name': 'client_example_com',
        'proxy': {
            'host': 'imap.pf1.tech.example.com',
            'master': 'pf1_master_user',
            'pass': 'pf1_master_pass'
        },
    }
 
def test_proxy_to_platform(mailbox_client_domain, platforms, masters, pf1, pf2):
    platforms[pf1]['proxy_to'] = pf2
    platforms[pf1]['context_id_map'] = {
        "1": "2"
    }
    platforms[pf1]['ox_cluster_map'] = {
        "oximap": "single-server"
    }
    mailbox_client_domain["platform"] = pf1
    from_platform = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf1)
    to_platform = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf2)
    assert from_platform == { # pf1
        'name': 'mail.test.example.com',
        'delivery': 'relay',
        'features': ['mailbox', 'mx', 'webmail'],
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'imap_domain': 'imap.mail.test.example.com', # FIXME Added
        'smtp_domain': 'smtp.mail.test.example.com', # FIXME Added
        'cert_name': 'client_example_com',
        'transport': 'smtp:[smtp.pf2.tech.example.com]',
        'ox_cluster': 'oximap',
        'context_id': '1',
        'webmail_domain': 'webmail.mail.test.example.com',
    }
    assert to_platform == { # pf2
        'name': 'mail.test.example.com',
        'delivery': 'virtual',
        'features': ['mailbox', 'mx', 'webmail'],
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'imap_domain': 'imap.mail.test.example.com',
        'smtp_domain': 'smtp.mail.test.example.com',
        'ox_cluster': 'single-server',
        'context_id': '2',
        'webmail_domain': 'webmail.mail.test.example.com',
        'cert_name': 'client_example_com',
        'proxy': {
            'host': 'imap.pf1.tech.example.com',
            'master': 'pf1_master_user',
            'pass': 'pf1_master_pass'
        },
    }

def test_proxy_from_platform(mailbox_client_domain, platforms, masters, pf1, pf2):
    platforms[pf2]['proxy_from'] = pf1
    platforms[pf2]['ox_cluster_map'] = {
        "oximap": "single-server"
    }
    platforms[pf2]['context_id_map'] = {
        "1": "2"
    }
    mailbox_client_domain['platform'] = pf2
    from_platform = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf1)
    to_platform = compute_mail_domain.compute_mail_domain(mailbox_client_domain, platforms, masters, pf2)
    assert from_platform == { # pf1
        'name': 'mail.test.example.com',
        'delivery': 'relay',
        'features': ['mailbox', 'mx', 'webmail'],
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'imap_domain': 'imap.mail.test.example.com', # FIXME Added
        'smtp_domain': 'smtp.mail.test.example.com', # FIXME Added
        'transport': 'smtp:[smtp.pf2.tech.example.com]',
        'ox_cluster': 'single-server',
        'context_id': '2',
        'webmail_domain': 'webmail.mail.test.example.com',
        'cert_name': 'client_example_com',
    }
    assert to_platform == { # pf2
        'name': 'mail.test.example.com',
        'delivery': 'virtual',
        'features': ['mailbox', 'mx', 'webmail'],
        'dkim_selector': 'example_selector',
        "dkim_private_key_file": "testdata/key",
        "dkim_public_key_file": "testdata/public",
        'imap_domain': 'imap.mail.test.example.com',
        'smtp_domain': 'smtp.mail.test.example.com',
        'ox_cluster': 'oximap',
        'context_id': '1',
        'webmail_domain': 'webmail.mail.test.example.com',
        'cert_name': 'client_example_com',
        'proxy': {
            'host': 'imap.pf1.tech.example.com',
            'master': 'pf1_master_user',
            'pass': 'pf1_master_pass',
        },
    }

def test_raw_mail_domain_rejection(raw_client_domain, platforms, masters, pf1):
    # when
    with pytest.raises(AnsibleFilterError) as context:
        compute_mail_domain.compute_mail_domain(raw_client_domain, platforms, masters, pf1)

    # then
    assert "error: client_domain has not been filtered with dimail_check_client_domain" in str(context.value)

