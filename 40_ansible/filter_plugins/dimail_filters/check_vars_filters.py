from ansible.errors import AnsibleFilterError


class FilterModule(object):
    def filters(self):
        return {
            'dimail_check_tech_domain': self.check_tech_domain,
            'dimail_check_client_domain': self.check_client_domain,
            'dimail_check_platforms': self.check_platforms,
        }

    def check_client_domain_properties(self, client_domain):
        if "name" not in client_domain:
            raise AnsibleFilterError("The client_domain has no name")
        domain_name = client_domain["name"]

        mandatory_properties = ['features', 'delivery']
        for prop in mandatory_properties:
            if prop not in client_domain:
                raise AnsibleFilterError(f"missing '{prop}' mandatory property for client domain '{domain_name}'")
        delivery = client_domain["delivery"]

        feature_dependencies = {
            'webmail': ['ox_cluster', 'context_id', 'cert_name'],
            'mailbox': ['dkim_selector', 'cert_name'],
            'alias': [],
            'mx': [],
        }

        for feature in client_domain['features']:
            if feature not in feature_dependencies:
                raise AnsibleFilterError(f"unknown feature '{feature}' for domain '{domain_name}'")
            for prop in feature_dependencies[feature]:
                if prop not in client_domain:
                    raise AnsibleFilterError(
                        f"client_domain is missing '{prop}' property, required by '{feature}' feature, for domain '{domain_name}'"
                    )

        delivery_dependencies = {
            "virtual": [],
            "alias": [],
            "relay": [ "transport" ],
        }
        for prop in delivery_dependencies[delivery]:
            if prop not in client_domain:
                raise AnsibleFilterError(
                    f"client_domain is missing '{prop}' property, required by the delivery '{delivery}', for domain '{domain_name}'"
                )

        proxy_dependencies = {
            "proxy_from": ["proxy_context_id", "proxy_ox_cluster"],
            "proxy_to": ["proxy_context_id", "proxy_ox_cluster"],
        }
        for prop, deps in proxy_dependencies.items():
            if prop in client_domain:
                for dep in deps:
                    if dep not in client_domain:
                        raise AnsibleFilterError(
                            f"client_domain is missing '{dep}' property, required by '{prop}' property, for domain '{domain_name}'"
                        )

    def check_client_domain_values(self, client_domain):
        domain_name = client_domain["name"]
        valid_clusters = ['oxldap', 'oximap', 'single-server']
        if 'webmail' in client_domain['features'] and client_domain['ox_cluster'] not in valid_clusters:
            ox_cluster = client_domain["ox_cluster"]
            raise AnsibleFilterError(
                f"ox_cluster '{ox_cluster}' for domain '{domain_name}' not in {valid_clusters} values"
            )

    def check_client_domain(self, client_domain):
        self.check_client_domain_properties(client_domain)
        self.check_client_domain_values(client_domain)
        client_domain['__dimail_check'] = 'ok'
        return client_domain

    def check_tech_domain(self, tech_domain):
        for prop in ['name', 'cert_name', 'dkim_selector']:
            if prop not in tech_domain:
                raise AnsibleFilterError(
                    f'missing property for tech_domain: `{prop}`'
                )
        tech_domain['__dimail_check'] = 'ok'
        return tech_domain

    def check_platforms(self, platforms, client_contexts, env_name):
        context_ids = []
        ox_clusters = []
        for client_context in client_contexts:
            context_ids.append(client_context['id'])
            ox_clusters.append(client_context['cluster'])

        for pfname, platform in platforms.items():
            for prop in ['proxy_to', 'proxy_from']:
                if prop in platform:
                    for dep in ['ox_cluster_map', 'context_id_map']:
                        if dep not in platform:
                            raise AnsibleFilterError(
                                f"platform '{pfname}' is missing '{dep}' property"
                            ) 
                    if not platform[prop] in platforms:
                         print(pfname, prop, platform[prop])
                         raise AnsibleFilterError(
                             f"'{platform[prop]}' referenced as '{prop}' for platform '{pfname}' does not exist"
                         )
                    if pfname == env_name:
                        for context_id in context_ids:
                            if context_id not in platform['context_id_map']:
                                raise AnsibleFilterError(
                                    f"context_id '{context_id}' does not exists for platform '{pfname}' in '{env_name}' context_id_map"
                                )  
                        for context_id in platform['context_id_map']:
                            if context_id not in context_ids:
                                raise AnsibleFilterError(
                                    f"cluster_id_map for platform '{pfname}' contains context_id '{context_id}' that does not exist in platform '{env_name}'"
                                )
                        for ox_cluster in platform['ox_cluster_map']:
                            if ox_cluster not in ox_clusters:
                                raise AnsibleFilterError(
                                    f"ox_cluster_map for platform '{pfname}' contains context_id '{ox_cluster}' that does not exist in platform '{env_name}'"
                                )
                    if platform[prop] == env_name:
                        for ox_cluster in platform['ox_cluster_map'].values():
                            if ox_cluster not in ox_clusters:
                                raise AnsibleFilterError(
                                    f"ox_cluster_map for platform '{pfname}' contains ox_cluster '{ox_cluster}' that does not exist in platform '{env_name}'"
                                )
                        for context_id in platform['context_id_map'].values():
                            if context_id not in context_ids:
                                raise AnsibleFilterError(
                                    f"cluster_id_map for platform '{pfname}' contains context_id '{context_id}' that does not exist in platform '{env_name}'"
                                )

        return platforms
