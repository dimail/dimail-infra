import pytest
from . import cert_info_filters
from . import compute_mail_domain_filters 
from ansible.errors import AnsibleFilterError

compute_mail_domain = compute_mail_domain_filters()
cert_info = cert_info_filters()

@pytest.fixture
def raw_tech_domain():
    return {
        "name": "tech.example.com",
        "cert_name": "cert_name",
        "dkim_selector": "tech_selector",
    }

@pytest.fixture
def host_domain():
    return {
        "name": "random.ovh.domain.com"
    }

@pytest.fixture
def checked_tech_domain(): 
    return {
        "__dimail_check": "ok",
        "name": "tech.example.com",
        "cert_name": "cert_name",
        "dkim_selector": "tech_selector",
    }

@pytest.fixture
def checked_client_domain():
    return {
        "__dimail_check": "ok",
        "name": "mail.test.example.com",
        "delivery": "virtual",
        "features": ["mailbox", "mx", "webmail"],
        "ox_cluster": "oximap",
        "context_id": "1",
        "cert_name": "client_example_com",
        "dkim_selector": "example_selector",
        "dkim_private_key_file": "example_file_name",
        "dkim_public_key_file": "example_file_name",
        "platform": "pf1"
    }

@pytest.fixture
def platforms():
    return {
        "pf1": {
            "tech_domain": {
                "name": "pf1.tech.example.com",
                "cert_name": "tech_domain_pf1"
            },
            "host_domain": {
                "name": "pf1.host.example.com",
            },
        },
    }

@pytest.fixture
def masters():
    return [
        {
            "platform": "pf1",
            "master": "pf1_master_user",
            "pass": "pf1_master_pass",
        },
        {
            "platform": "pf2",
            "master": "pf2_master_user",
            "pass": "pf2_master_pass",
        },
    ]

@pytest.fixture
def mail_domain(checked_client_domain, platforms, masters):
    return compute_mail_domain.compute_mail_domain(checked_client_domain, platforms, masters, 'pf1')

@pytest.fixture
def hostvars():
    return {
        "main-example": {
            "hostname": "main-example",
        },
        "backup-example": {
            "hostname": "backup-example",
        }
    }

@pytest.fixture
def cert_user_group(): 
    return ["main-example", "backup-example"]

@pytest.fixture
def groups():
    return ['imap_master', 'smtp']

def test_cert_info_tech_domain(checked_tech_domain, groups, hostvars):
    # test without oidc
    cert_info_tech = cert_info.cert_info_tech_domain(checked_tech_domain, groups, hostvars)
    assert cert_info_tech == {
        'cert_name': 'cert_name',
        'domain': 'tech.example.com',
        'domains': ['imap.tech.example.com',
                    'smtp.tech.example.com',
                    'mx.tech.example.com']
    }
    # test with oidc
    hostvars['main-example']['ox_params'] = ['oidc']
    cert_info_tech_oidc = cert_info.cert_info_tech_domain(checked_tech_domain, groups, hostvars)
    assert cert_info_tech_oidc == {
        'cert_name': 'cert_name',
        'domain': 'tech.example.com',
        'domains': ['imap.tech.example.com',
                    'smtp.tech.example.com',
                    'mx.tech.example.com',
                    'oidc.tech.example.com']
    }

def test_cert_info_host(cert_user_group, hostvars, host_domain):
    hostname = cert_user_group[0]
    cert_info_host = cert_info.cert_info_host(hostname, hostvars, host_domain)
    assert cert_info_host == {'cert_name': 'tech_main-example',
        'domain': 'random.ovh.domain.com',
        'domains': ['main-example.random.ovh.domain.com']
    }

def test_cert_info_domain(mail_domain):
    cert_info_mail_domain = cert_info.cert_info_domain(mail_domain)
    assert cert_info_mail_domain == {'cert_name': 'client_example_com',
        'domain': 'mail.test.example.com',
        'domains': [
            'imap.mail.test.example.com',
            'smtp.mail.test.example.com',
            'webmail.mail.test.example.com',
    ]}

def test_raw_tech_domain_is_detected(cert_user_group, raw_tech_domain, groups, hostvars):
    # given
    hostname = cert_user_group[0]
    print("hello")
    print(hostvars)
    # when
    with pytest.raises(AnsibleFilterError) as context:
        cert_info.cert_info_tech_domain(raw_tech_domain, groups, hostvars)

    # then
    assert "error: tech_domain has not been filtered with dimail_check_tech_domain" in str(context.value)
