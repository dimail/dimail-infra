#!/bin/bash

###################################################################################
# ==================== Apache Common Text scanning scripts ========================
#
# VERSION 1.0.0
#
# AUTHOR  Cyberwatch SAS
#
# PROJECTURI https://cyberwatch.fr
#
# DESCRIPTION
# Scans filesystem for .jar, war, and ear files that contains the Apache Commons Text lib.
# Provides the list of fetched files in the CPE format for Cyberwatch.
# Inspired by https://github.com/CERTCC/CVE-2021-44228_scanner.
#
# SYNOPSIS
# Find Apache Commons Text files and provide the results in the CPE format for Cyberwatch analysis.
###################################################################################

toplevel='/'

# Run find with the sudo command if available, without the sudo command otherwise (can lead to permission issues)
sudo=''
sudo -n find -version > /dev/null && sudo='sudo -n'

# Le script est compatible à la fois Python 2 et Python 3. Prenons ce qui est disponible.
python='python'
if command -v python3 > /dev/null ; then
    python='python3'
elif command -v python2 > /dev/null ; then
    python='python2'
fi

pyscript=$(mktemp --tmpdir cyberwatch-XXXXXX.py)
cat > "$pyscript" <<PYTHON

import io
import os.path
import re
import sys
import zipfile

commonstext_re = re.compile('^commons-text.*-(?P<version>[0-9][0-9.]*[0-9]).*jar$')

def process_archive(archive_file, archive_path):
    try:
        with zipfile.ZipFile(archive_file) as archive:
            for member_path in archive.namelist():
                full_path = os.path.join(archive_path, member_path)
                m = commonstext_re.match(os.path.basename(archive_path))
                if m:
                    print('# ' + full_path)
                    print('NVD_APPLICATION:cpe:2.3:a:apache:commons_text:' + m.group('version') + ':*:*:*:*:*:*:*')
                if os.path.splitext(member_path)[1].lower() in ['.jar', '.war', '.ear', '.zip']:
                    process_archive(io.BytesIO(archive.read(member_path)), full_path)
    except Exception as e:
      print("# Error reading " + archive_path + ": " + str(e))

for line in sys.stdin:
    path = line.rstrip()
    process_archive(path, path)

PYTHON

$sudo find "$toplevel" -xdev -type f \( -iname '*.jar' -o -iname '*.war' -o -iname '*.ear' -o -iname '*.zip' \) |
    $sudo $python -- "$pyscript"

rm -- "$pyscript"
