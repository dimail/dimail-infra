#!/bin/bash

# Détection du mode d’exécution de Docker. Si on a permission d’écriture sur la socket, inutile d’avoir recours à sudo.
# root (ID 0) n’a pas non plus besoin de sudo. Le reste du temps, utilisons sudo dans le doute.
if [ -w /var/run/docker.sock ]; then
  docker='docker'
elif [ "$(id -u)" -eq 0 ]; then
  docker='docker'
else
  docker='sudo -n docker'
fi

$docker ps --format '{{.Image}}' | sort -u | while read -r image_name ; do

echo
echo --cbw-info-part
echo "Docker-Image:$image_name"
echo

$docker inspect --format '{{.Id}}
{{.Created}}' -- "$image_name" | {

read -r image_id
read -r image_creation_date

$docker run -u root -i --rm --entrypoint sh -e "IMAGE_NAME=$image_name" -e "IMAGE_ID=$image_id" -e "IMAGE_CREATION_DATE=$image_creation_date" -- "$image_name" <<'CBW-DOCKER'
echo '# System information and vulnerabilities scan'
sh <<'CBW-SCRIPT'
{
# Some OS (Centos7) don't load the user's path properly for the ip command
export PATH="$PATH:/usr/sbin"

# Machine type detection: whether we're inside a container, a virtual machine, bare-metal, etc.
machine_type= # unknown
[ -n "$IMAGE_NAME" ] && machine_type=docker
echo "MACHINE_TYPE:$machine_type"

echo "ARCH:$(uname -m)"

# System
if [ "$machine_type" = docker ]; then
  echo "HOSTNAME:$IMAGE_NAME"
  echo "META:IMAGE_NAME|$IMAGE_NAME"
  echo "META:IMAGE_DIGEST|$IMAGE_DIGEST"
  echo "META:IMAGE_ID|$IMAGE_ID"
  echo "META:IMAGE_CREATION_DATE|$IMAGE_CREATION_DATE"
  echo "META:DOCKER_REGISTRY|$DOCKER_REGISTRY"

  # Convertit les labels de l’image Docker en métadonnées info.
  env | sed -ne 's/^CBW_LABEL[0-9]*=/META:/p'
else
  if command -v hostname > /dev/null ; then
    echo "HOSTNAME:$(hostname)"
  elif [ -f /etc/hostname ]; then
    echo "HOSTNAME:$(head -n 1 /etc/hostname)"
  fi
  echo "KERNEL_VERSION:$(uname -r)"
  echo "META:kernel-version|$(uname -r)"

  # List all the IPv4 addresses
  # We filter out IP addresses with scope host.
  ip -o -f inet address | awk '!/scope host/ {print "IP:"$4}'
fi

# OS identification
# Rely on /etc/os-release, which should be present on all the modern distributions.
# Use VERSION_ID to get a locale-agnostic version number. See os-release(5).

awk -F= -f - /etc/os-release <<'EOF'
  {
    key = $1
    value = $2
    # Remove enclosing quotes in value.
    sub(/^"/, "", value)
    sub(/"$/, "", value)
    variables[key] = value
  }
  END {
    print "OS_NAME:" variables["NAME"]
    print "OS_VERSION:" variables["VERSION_ID"]
  }
EOF

# Legacy OS identification
# Compute a pretty name based on all kinds of sources.

OS_PRETTYNAME=$(awk '/PRETTY_NAME/' /etc/*-release | sed 's/PRETTY_NAME=//' | sed 's/\"//g')
if [ -z "${OS_PRETTYNAME}" ]; then
  [ -e "/etc/redhat-release" ] && OS_PRETTYNAME="$(head -n 1 "/etc/redhat-release")"
  [ -e "/etc/SuSE-release" ] && OS_PRETTYNAME="$(head -n 1 "/etc/SuSE-release")"
  [ -n "$(lsb_release -ds 2>/dev/null)" ] && OS_PRETTYNAME=$(lsb_release -ds | tr '"' ' ')
  [ -e "/etc/debian_version" ] && OS_PRETTYNAME="Debian $(cat /etc/debian_version)"
  [ -e "/etc/manjaro-release" ] && OS_PRETTYNAME="$(head -n 1 "/etc/manjaro-release")"
  [ -z "$OS_PRETTYNAME" ] && OS_PRETTYNAME="Not Found"
fi

if [ "$OS_PRETTYNAME" = "Red Hat Enterprise Linux" ]; then
  [ -e "/etc/system-release-cpe" ] && OS_PRETTYNAME="$OS_PRETTYNAME $(awk -F: '{print $5}' /etc/system-release-cpe)"
fi

# Les OS redhat peuvent être "fixés" sur une certaine version mineur
if  [ "${OS_PRETTYNAME#*"Red Hat"}" != "$OS_PRETTYNAME" ] && command -v 'subscription-manager' >  /dev/null ; then
  LTS_RELEASE=$(subscription-manager release 2>/dev/null | sed -nE 's/Release: ([0-9]+\.[0-9]+)/\1/p')
  if [ -n "${LTS_RELEASE}" ]; then OS_PRETTYNAME="Red Hat ${LTS_RELEASE} EUS"; fi
fi

if [ "$OS_PRETTYNAME" = "VMware Photon OS/Linux" ]; then
  [ -e "/etc/photon-release" ] && OS_PRETTYNAME="$(head -n 1 "/etc/photon-release")"
fi

echo "OS_PRETTYNAME:${OS_PRETTYNAME}"

# Boot time, and whether a reboot is required or not.
if [ "$machine_type" != docker ]; then
  REBOOT=false; [ -f '/var/run/reboot-required' ] && REBOOT=true

  # When it exists, check the exit code of needs-restarting
  if command -v needs-restarting > /dev/null ; then
    needs-restarting -r > /dev/null 2>&1
    # CentOS 6 does not support -r and returns 2 in that case, so we accept $? = 1 and nothing else.
    [ "$?" -eq 1 ] && REBOOT=true
  fi

  echo "REBOOT:${REBOOT}"

  boot_time=$(uptime -s 2> /dev/null) && echo "BOOT_TIME:$(date -d "$boot_time" +%FT%T%z)"
fi


# Packages
if command -v dpkg-query > /dev/null ; then
  dpkg-query -W -f='DEB_PACKAGE:${Package}|${Version}|${Status}\n' | sed -n -E 's/\|(hold|install) ok installed$//p'
elif command -v rpm > /dev/null ; then
  rpm -qa --qf='RPM_PACKAGE:%{NAME}.%{ARCH}|%{VERSION}-%{RELEASE}\n'
elif command -v pacman > /dev/null ; then
  pacman -Q | awk '{print "PACMAN_PACKAGE:"$1"|"$2}'
elif command -v tdnf > /dev/null ; then
  tdnf list installed | awk '{print "RPM_PACKAGE:"$1"|"$2}'
elif command -v apk > /dev/null ; then
  apk info -v | sed 's/^\(.*\)-\([^-]*\)-\([^-]*\)$/APK_PACKAGE:\1|\2-\3/; t; s/^/ERROR: Cannot interpret line: /'
else
  echo 'ANOMALY:No package manager.'
fi |\
sed -ne 'p;
s/^[A-Z]*_PACKAGE:omi\(.x86_64\)\?|/LINUX_APPLICATION:omi|/p;
s/^[A-Z]*_PACKAGE:zimbra-patch\(.x86_64\)\?|/LINUX_APPLICATION:zimbra-patch|/p;
s/^[A-Z]*_PACKAGE:zend-server-\(nginx\|php\)[^|]*/LINUX_APPLICATION:zend_server/p'

# Services
if command -v systemctl > /dev/null; then
  systemctl list-unit-files --no-legend -t service | awk '{ sub(/\.service$/, "", $1); print "SERVICE:" $1 "|" $2 }'
fi

# Applicative packages managers.

if command -v pip > /dev/null; then
  # six==1.14.0 → PIP:six|1.14.0
  pip freeze | awk -F == '{ print "PIP:" $1 "|" $2 }'
fi

if command -v gem > /dev/null; then
  # racc (1.5.2, default: 1.5.1)
  gem list --quiet | awk '
    {
      name = $1
      vstring = $0
      sub(/^[^(]*\(/, "", vstring)
      sub(/\)$/, "", vstring)
      gsub(/default: /, "", vstring)
      split(vstring, versions, ", ")
      for (i in versions) {
        print "GEM:" name "|" versions[i]
      }
    }
  '
fi

# Third-party software detection.
if [ -n "$ORACLE_HOME" ]; then
  echo '# ORACLE_HOME is set. Performing Oracle database detection...'
  ("$ORACLE_HOME/OPatch/opatch" lsinventory || sudo -u "${ORACLE_USER:-oracle}" "$ORACLE_HOME/OPatch/opatch" lsinventory) | awk -F'[ \t]{2,}' '
    # Oracle Database 19c           19.0.0.0.0
    /^Oracle Database / {
      product = $1
      version = $2
    }

    # Patch description:  "Database Release Update : 19.9.0.0.201020 (31771877)"
    $1 == "Patch description:" {
      if (match($2, /"Database Release Update : (\S+)/, a)) {
        version = a[1]
      }
    }

    END {
      if (product) {
        # LINUX_APPLICATION:Oracle Database 19c|19.9.0.0.201020
        print "LINUX_APPLICATION:" product "|" version
      } else {
        print "# No Oracle database found."
      }
    }
  '
fi

if path=$(command -v docker) > /dev/null ; then
  # Docker version 20.10.8, build 3967b7d → NVD_APPLICATION:cpe:/a:docker:docker:20.10.8
  # Le produit docker devient un downstream du Moby project, du coup il faut ajouter le cpe du Moby project
  # pour pouvoir supporter les cves comme CVE-2022-24769
  docker --version | sed -ne "s~^Docker version \([^ ,]\+\).*$~NVD_APPLICATION:cpe:/a:docker:docker:\1|$path\nNVD_APPLICATION:cpe:/a:mobyproject:moby:\1|$path~p"
fi

if path=$(command -v redis-server) > /dev/null ; then
  # Redis server v=6.2.6 sha=00000000:0 malloc=jemalloc-5.1.0 bits=64 build=fdd28bd28db05332
  redis-server --version | sed -ne "s~^Redis server v=\([^ ]\+\).*$~NVD_APPLICATION:redis|\1|$path~p"
fi

if path=$(command -v node) > /dev/null ; then
  # v16.14.2
  node --version | sed -ne "s~^v\([^ ]\+\)$~NVD_APPLICATION:cpe:/a:nodejs:node.js:\1|$path~p"
fi

if path=$(command -v yarn) > /dev/null ; then
  # 1.22.18
  yarn --version | sed -ne "s~^\([^ ]\+\)$~NVD_APPLICATION:cpe:/a:yarnpkg:yarn:\1|$path~p"
fi

if path=$(command -v npm) > /dev/null ; then
  # 8.5.0
  npm --version | sed -ne "s~^\([^ ]\+\)$~NVD_APPLICATION:cpe:/a:npmjs:npm:\1|$path~p"
fi

if command -v java > /dev/null ; then
  # Pour Eclipse Temurin, `java -version` affiche :
  #
  #   openjdk version "18.0.2" 2022-07-19
  #   OpenJDK Runtime Environment Temurin-18.0.2+9 (build 18.0.2+9)
  #   OpenJDK 64-Bit Server VM Temurin-18.0.2+9 (build 18.0.2+9, mixed mode, sharing)
  #
  # Pour Java, `java -version` affiche :
  #
  #   java version "1.8.0_102"
  #   Java(TM) SE Runtime Environment (build 1.8.0_102-b14)
  #   Java HotSpot(TM) 64-Bit Server VM (build 25.102-b14, mixed mode)
  #
  # La notation « Nom (build N) » est commune à toutes les distributions de Java. Servons-nous en pour extraire la
  # version de Java, et pour éliminer les lignes qui ne nous intéressent pas. Chaque ligne donnant un build génèrera une
  # APPLICATION. On s’attend à avoir une application pour le JRE, puis une pour la JVM.
  #
  # Ajout d'une exception sur la notation « Nom (build N) » pour le cas ci-dessous
  #   OpenJDK Runtime Environment (IcedTea 2.6.22) (7u261-2.6.22-1~deb8u1) OpenJDK 64-Bit Server VM (build 24.261-b02, mixed mode)
  java -version 2>&1 | sed -rn 's/.*\(([678]u[0-9]+[^)]*).*|.*build ([^,)) ]+).*/LINUX_APPLICATION:\0|\1\2/p'
fi

# Détection ELK
# -------------
#
# On détectait historiquement les versions ELK en lançant des --version, qui est une convention qui marche assez bien.
# Il s’avère cependant que pour la suite ELK, lancer --version demande des droits root et peut faire planter des
# instances ELK en production. Optons donc pour une approche plus passive en explorant le système de fichier.
#
# Deux modes d’installation sont supportés :
#  - l’installation par paquet DEB/RPM,
#  - Docker.
#
# Dans les deux cas, l’installation se trouvera sous /usr/share/. On pourrait mieux gérer les installations moins
# orthodoxes en cherchant le chemin absolu de la commande elasticsearch par exemple, mais il s’avère que les exécutables
# ELK ne sont pas mis dans les PATH par défaut, étant donné qu’ils sont destinés à être lancés par systemd.

# Exemple : /usr/share/elasticsearch/lib/elasticsearch-7.16.1.jar
if [ -d /usr/share/elasticsearch/lib/ ]; then
  find /usr/share/elasticsearch/lib/ -printf '%f\n' | sed -ne 's/^elasticsearch-\([0-9][0-9.]*\)\.jar$/NVD_APPLICATION:cpe:\/a:elastic:elasticsearch:\1/p'
fi

if [ -f /usr/share/kibana/package.json ]; then
  sed -ne 's/^  "version": "\([^"]*\)",\?$/NVD_APPLICATION:cpe:\/a:elastic:kibana:\1/p' /usr/share/kibana/package.json
fi

# Exemple de fragment du Gemfile.lock qui nous intéresse :
#
# ```
# PATH
#   remote: logstash-core
#   specs:
#     logstash-core (7.16.1-java)
# ```
#
if [ -f /usr/share/logstash/Gemfile.lock ]; then
  sed -ne 's/^    logstash-core (\([0-9][0-9.]*\).*$/NVD_APPLICATION:cpe:\/a:elastic:logstash:\1/p' /usr/share/logstash/Gemfile.lock
fi

# filebeat est écrit en Go et n’a aucun de fichier de métadonnées. Comme il semble avoir une sous-commande version
# plutôt efficace, on peut se permettre de l’appeler sans trop d’impacts, contrairement aux autres programmes ELK.
# Le chemin de l’exécutable a changé entre la version 6 et 7, donc ajoutons les deux candidats dans le PATH.
if filebeat=$(PATH="$PATH:/usr/share/filebeat:/usr/share/filebeat/bin" command -v filebeat) ; then
  # filebeat version 7.14.1 (amd64), libbeat 7.14.1 [703d589a09cfdbfd7f84c1d990b50b6b7f62ac29 built 2021-08-26 09:12:57 +0000 UTC]
  "$filebeat" version | sed -ne 's/^filebeat version \([^ ,]\+\).*$/NVD_APPLICATION:cpe:\/a:elastic:filebeat:\1/p'
fi

if [ -f /opt/gitlab/version-manifest.txt ]; then
  # gitlab-ce     14.3.0
  awk '
    $1 == "gitlab-ce" { print "NVD_APPLICATION:cpe:2.3:a:gitlab:gitlab:" $2 ":-:-:-:community:-:-:-" }
    $1 == "gitlab-ee" { print "NVD_APPLICATION:cpe:2.3:a:gitlab:gitlab:" $2 ":-:-:-:enterprise:-:-:-" }
  ' /opt/gitlab/version-manifest.txt
fi

}

CBW-SCRIPT

echo
echo '# Applicative packages scan'
sh <<'CBW-SCRIPT'
#!/bin/sh

{ find "${SEARCH_PATH:-/}" \( -path '/dev' -o -path '/proc' -o -path '/sys' -o -path '/run' -o -path '/mnt' -o -path '/media' -o -path '/var/lib/docker' -o -name 'lost+found' \) -prune \
      -o -type f \( -name 'VERSION' -o -name 'bootstrap.inc' -o -iname 'version.php' -o -name '*.[jw]ar' \
                    -o -name 'package-lock.json' -o -name 'yarn.lock' \
                    -o -name composer.lock \! -path '*/vendor/*' \
                    -o -name 'METADATA' -path '*/dist-packages/*' \
                    -o -executable \)
} | while read -r path
do
  clean_path=${path#"${SEARCH_PATH}"}
  case "$path" in
  # Cyberwatch
  */olympe/VERSION)
    version=$(cat "$path")
    echo "NVD_APPLICATION:cpe:/a:cyberwatch:cyberwatch_application:$version|$clean_path"
    ;;

  # Drupal
  # pour les images des versions récentes (8+), on se base sur les données de Composer
  # pour les anciennes versions (7.x), on se base sur la version déclarée dans bootstrap.inc
  # Dans les images Docker officielles, il n'y a pas de répertoire Drupal racine,
  # c'est installé directement dans /var/www/html
  # on vérifie donc la présence de drupal.js relativement au chemin de bootstrap.inc
  # pour s'assurer que l'on est bien dans Drupal.
  *includes/bootstrap.inc)
    # define('VERSION', '7.94');
    end_path="includes/bootstrap.inc"
    drupaljs="${path%"$end_path"}misc/drupal.js"
    if [ -f "$drupaljs" ]; then
      sed -nE "s/^\s*define\('VERSION', '(([0-9]+\.)?[0-9]+\.[0-9]+)'\);$/cpe:\/a:drupal:drupal:\1/p" "$path" |
        while read -r cpe ; do echo "NVD_APPLICATION:$cpe|$clean_path" ; done
    fi
    ;;

  # Joomla!
  */joomla/libraries/src/Version.php)
  # public const MAJOR_VERSION = 4;
    cpe=$(sed -nE 's/^\s*(public )?const MAJOR_VERSION = ([0-9]+);$/cpe:\/a:joomla:joomla%21:\2/p;
                   s/^\s*(public )?const MINOR_VERSION = ([0-9]+);$/.\2/p;
                   s/^\s*(public )?const PATCH_VERSION = ([0-9]+);$/.\2/p' "$path" | tr -d '\n')
    echo "NVD_APPLICATION:$cpe|$clean_path"
    ;;

  # Nextcloud / owncloud (deprecated) / owncloud/server
  */occ)
    # Nextcloud 25.0.4
    # ownCloud 10.0.10
    # ownCloud version 9.1.8
    if [ -z "$SEARCH_PATH" ]; then
      "$path" -V |
        sed -nE 's/^Nextcloud ([0-9]+\.[0-9]+\.[0-9]+).*$/cpe:\/a:nextcloud:nextcloud_server:\1/p;
                 s/^ownCloud (version )?([0-9]+\.[0-9]+\.[0-9]+)$/cpe:\/a:owncloud:owncloud:\2/p' |
        while read -r cpe ; do echo "NVD_APPLICATION:$cpe|$clean_path" ; done
    fi
    ;;

  # Tomcat
  */tomcat/bin/version.sh)
    # Server version: Apache Tomcat/9.0.73
    if [ -z "$SEARCH_PATH" ]; then
      "$path" |
        sed -nE 's/^Server version: Apache Tomcat\/([0-9]+\.[0-9]+\.[0-9]+).*$/cpe:\/a:apache:tomcat:\1/p' |
        while read -r cpe ; do echo "NVD_APPLICATION:$cpe|$clean_path" ; done
    fi
    ;;

  # Wordpress
  */wp-includes/version.php)
    # $wp_version = '6.1';
    sed -nE "s/^[$]wp_version = '(([0-9]+\.)?[0-9]+\.[0-9]+)';$/cpe:\/a:wordpress:wordpress:\1/p" "$path" |
      while read -r cpe ; do echo "NVD_APPLICATION:$cpe|$clean_path" ; done
    ;;


  # Ignore les package-lock.json et yarn.lock définis sous les node_modules.
  */node_modules/*/package-lock.json) ;;
  */node_modules/*/yarn.lock) ;;

  # NPM
  */package-lock.json)
    awk -F': ' -v clean_dir="$(dirname -- "$clean_path")" '
      # La hiérarchie qui nous intéresse est la suivante :
      #
      # {
      #   "packages": {
      #     "node_modules/@aws-crypto/crc32/node_modules/tslib": {
      #      "version": "1.14.1",
      #
      # Dans ce cas, le nom du paquet est `tslib`, et la version `1.14.1`.

      /^    "node_modules\// {
        path = $1
        sub(/^ *"/, "", path)
        sub(/"$/, "", path)
        package = path
        sub(/(^|.*\/)node_modules\//, "", package)
      }

      package && $1 == "      \"version\"" {
        version = $2
        sub(/^"/, "", version)
        sub(/",?$/, "", version)
        print "NPM:" package "|" version "|" clean_dir "/" path
        package = ""
        path = ""
      }
    ' "$path"
    ;;

  # Yarn (JavaScript)
  # Yarn uses the same packages as NPM, so we generate the same NPM info property.
  */yarn.lock)
    awk -v clean_path="$clean_path" '
      # (Yarn 3) "@babel/code-frame@7.12.11":
      # (Yarn 3) type-check@^0.4.0, type-check@~0.4.0:
      # (Yarn 4) "wrap-ansi-cjs@npm:wrap-ansi@^7.0.0, wrap-ansi@npm:^7.0.0":
      # (Yarn 4) __metadata:
      /^[^ ].+:$/ {
        sub(/^"/, "")
        sub(/"?:$/, "")
        sub(/,.*$/, "")
        sub(/@[^@]*$/, "")
        sub(/@npm:.*$/, "")
        package = $0
      }

      # (Yarn 3)  version "7.12.11"
      # (Yarn 4)  version: 7.12.11
      package && ($1 == "version" || $1 == "version:") {
        version = $2
        gsub(/"/, "", version)
        if (package != "__metadata") {
          print "NPM:" package "|" version "|" clean_path
        }
        package = ""
      }
    ' "$path"
    ;;

  # Composer (PHP)
  */composer.lock)
    awk -F': ' -v clean_path="$clean_path" '
      $1 == "    \"packages\"" { in_packages = 1 }
      $1 == "    }" { in_packages = 0 }

      in_packages && $1 == "            \"name\"" {
        package = $2
        sub(/^"/, "", package)
        sub(/",?$/, "", package)
      }

      in_packages && package && $1 == "            \"version\"" {
        version = $2
        sub(/^"v?/, "", version)
        sub(/",?$/, "", version)
        print "COMPOSER:" package "|" version "|" clean_path
        package = ""
      }
    ' "$path"
    ;;

  # Maven (Java)
  #
  # Cherche les .jar et .war sur le système et convertit leur nom de la forme NOM-VERSION.jar en `MAVEN:NOM|VERSION`.
  # `/usr/share/elasticsearch/lib/lz4-java-1.8.0.jar` devient `MAVEN:lz4-java|1.8.0`. Cette nomenclature correspond au
  # dépôt Maven, quoiqu’on n’a aucune garantie que le JAR en provienne.
  #
  # Le premier `s` de sed élimine les dossiers pour ne garder que le nom de base du fichier. Le deuxième `s` supprime
  # l’extension et scinde sur le premier `-1` où `1` représente n’import quel chiffre. Comme le quantificateur `*` de sed
  # est glouton, c’est la capture du nom qui doit s’arrêter au premier signe d’une version qui commence.
  #
  # Cette détection se plante dans le cas de `google-api-services-storage-v1-rev20220705-2.0.0.jar` où la version est en
  # fait `v1-rev171-1.25.0`, tandis que pour `proto-google-iam-v1-1.17.1` la version est `1.17.1`. C’est inconciliable.
  #
  *.[jw]ar)
    echo "$path" |
      sed -ne 's/^.*\/// ; s/^\([^-]*\(-\|-[^0-9-][^-]*\)*\)-\([0-9].*\)\.[^.]*$/MAVEN:\1|\3/p' |
      sed -e 's/^MAVEN:log4j|1\.2-api-/MAVEN:log4j-1.2-api|/' |
      while read -r package ; do echo "$package|$clean_path" ; done
    ;;

  # Packages Python
  */METADATA)
    package=$(sed -nE 's/^Name: (.+)$/\1/p;
                       s/^Version: (.+)$/|\1/p' "$path" | tr -d '\n')
    echo "PIP:$package|$clean_path"
    ;;

  # Find All executable files on shellless images
  *)
    if [ -n "$SEARCH_PATH" ] && [ -x "$path" ]; then
      product="${path##*/}"
      echo "DOCKER_APPLICATION:$product||$clean_path"

      # Les exécutables Go ont une section ELF nommée .go.buildinfo qui contient des lignes comme :
      #
      #   mod	github.com/docker/compose/v2	(devel)
      #   dep	golang.org/x/crypto	v0.7.0	h1:AvwMYaRytfdeVt3u6mLaxYtErKYjxA2OXjJ1HHq6t3A=
      #   build	-ldflags="-s -w -X main.version=0.11.0 …
      #
      # Ce bloc d’information apparait parfois plusieurs fois, donc on indique à awk de s’arrêter après le premier. Si
      # on trouve une occurrence de `main.version=…`, on génère un GO_MODULE avec le nom extrait de la propriété `mod`.
      #
      # Busybox est lent, mais c’est tout ce qu’on a. busybox grep met 3 secondes à dire si un exécutable de 55 Mo
      # contient `.go.buildinfo`, contre 50 millisecondes avec GNU grep. busybox awk met ensuite 2 secondes à le
      # traiter.
      #
      if grep -Fq .go.buildinfo "$path" ; then
        awk -F'	' -v executable="$clean_path" '
          $1 == "mod" {
            mod = $2
          }

          mod {
            if ($1 == "dep") {
              sub(/^v/, "", $3)
              print "GO_MODULE:" $2 "|" $3 "|" executable
            } else if ($1 == "build") {
              if (mod && match($2, /main.version=\S+/)) {
                version = substr($2, RSTART + 13, RLENGTH - 13)
                print "GO_MODULE:" mod "|" version "|" executable
              }
            } else if ($1 !~ /^\w+$/) {
              exit
            }
          }
        ' "$path"
      fi
    fi
    ;;
  esac
done

CBW-SCRIPT
CBW-DOCKER

}

done

# Fin des images Docker. Retour au contexte racine.
echo
echo --cbw-info-part
echo
