#!/bin/bash

# Les clés pour le processeur ne sont pas exposées sur sysfs, donc utilisons systématiquement dmidecode.
# Les numéros de séries le sont, mais demandent root, donc autant utiliser dmidecode.
dmidecode_keys=(processor-version processor-manufacturer processor-family
                system-serial-number system-uuid baseboard-serial-number)

# /sys/devices/virtual/dmi expose pas mal d’information sans exiger d’installer dmidecode ou bien d’avoir des droits
# root. Pour les systèmes qui ne l’auraient pas, utilisons dmidecode en fallback.
if [ -r /sys/devices/virtual/dmi/id/bios_vendor ] ; then

# Utilise les données du BIOS et du System pour construire un code CPE
# Uniquement pour les machines Dell
if [[ $(< /sys/devices/virtual/dmi/id/bios_vendor) == 'Dell Inc.' ]] ; then
  system_model=$(cat < /sys/devices/virtual/dmi/id/product_name | tr ' ' '_' | tr '[:upper:]' '[:lower:]')
  echo "CPE:cpe:/o:dell:${system_model}_firmware:$(< /sys/devices/virtual/dmi/id/bios_version)"
fi

cat << EOF
META:bios-vendor|$(< /sys/devices/virtual/dmi/id/bios_vendor)
META:bios-version|$(< /sys/devices/virtual/dmi/id/bios_version)
META:bios-release-date|$(< /sys/devices/virtual/dmi/id/bios_date)
META:system-manufacturer|$(< /sys/devices/virtual/dmi/id/sys_vendor)
META:system-product-name|$(< /sys/devices/virtual/dmi/id/product_name)
META:baseboard-manufacturer|$(< /sys/devices/virtual/dmi/id/board_vendor)
META:baseboard-product-name|$(< /sys/devices/virtual/dmi/id/board_name)
META:baseboard-version|$(< /sys/devices/virtual/dmi/id/board_version)
EOF

else

echo '# sysfs does not expose the DMI virtual device. Falling back to dmidecode.'
dmidecode_keys+=(bios-vendor bios-version bios-release-date
                 system-manufacturer system-product-name
                 baseboard-manufacturer baseboard-product-name baseboard-version)

fi

# dmidecode: option requires an argument -- 's'
# String keyword expected
# Valid string keywords are:
#   bios-vendor
#   bios-version
#   bios-release-date
#   system-manufacturer
#   system-product-name
#   system-version
#   system-serial-number
#   system-uuid
#   system-family
#   baseboard-manufacturer
#   baseboard-product-name
#   baseboard-version
#   baseboard-serial-number
#   baseboard-asset-tag
#   chassis-manufacturer
#   chassis-type
#   chassis-version
#   chassis-serial-number
#   chassis-asset-tag
#   processor-family
#   processor-manufacturer
#   processor-version
#   processor-frequency

dmidecode=

# Check if user is root
if [ "$(id -u)" -eq 0 ]; then
  # We leave the error output so we can understand what went wrong
  if dmidecode -s bios-vendor > /dev/null; then
    dmidecode='dmidecode'
  fi
elif sudo -n dmidecode -s bios-vendor > /dev/null; then
  dmidecode='sudo -n dmidecode'
fi

if [ -n "$dmidecode" ]; then

  # Utilise les données du BIOS et du System pour construire un code CPE
  # Uniquement pour les machines Dell
  if [[ $($dmidecode -s "bios-vendor") == 'Dell Inc.' ]] ; then
    system_model=$($dmidecode -s "system-product-name" | tr ' ' '_' | tr '[:upper:]' '[:lower:]')
    echo "cpe:/o:dell:${system_model}_firmware:$($dmidecode -s "bios-version")"
  fi

  for key in "${dmidecode_keys[@]}"
  do
    echo "META:$key|$($dmidecode -s "$key")"
  done
else
  echo "# Cannot get all the BIOS metadata without using dmidecode."
  echo '# Please make sure it is installed and you have the right to use it'
fi

# Get the total disks size
df -h | awk '/\/dev\/[sv]d/ && NR > 2 {print "META:disk-size:"$1"|"$2}'

echo "META:machine-id|$(< /etc/machine-id)"
