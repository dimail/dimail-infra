#!/bin/bash

set -o pipefail

# Ajouter /usr/sbin aux PATH car les Entreprise Linux 6 et 7 ne voient autrement pas ss.
export PATH="$PATH:/usr/sbin"

# Usage: find_package PROGRAM_NAME
# Query the package manager and print the name of the package that owns the specified program.
find_package() {
  process="$1"
  [ -n "$process" ] || return

  if command -v dpkg-query > /dev/null ; then
    process_path=$(command -v "$process") || return

    # dpkg does not dereference symlinks, which is problematic with /etc/alternatives.
    process_path=$(realpath "$process_path") || return

    # dpkg-query returns 0 if it finds at least one file, and 1 if not.
    # Its output looks like "netcat-openbsd: /bin/nc.openbsd".
    # Use sed to keep only the package name part, and exit after the first line.
    # Return if it worked, or otherwise try a fallback.
    dpkg-query -S "$process_path" 2> /dev/null | sed -e 's/:.*$// ; q' && return

    # dpkg may not find an executable even if we specify its canoncial location because of the /bin symlink.
    # https://www.debian.org/doc/manuals/debian-handbook/sect.manipulating-packages-with-dpkg.en.html
    # If the dpkg-query above failed, try again without /usr.
    if [ "${process_path:0:5}" = /usr/ ]; then
      dpkg-query -S "${process_path:4}" 2> /dev/null | sed -e 's/:.*$// ; q' && return
    fi
  elif command -v rpm > /dev/null ; then
    process_path=$(command -v "$process") || return
    # --queryformat includes ARCH to match the PACKAGE names of the general info script.
    rpm --query --file "$process_path" --queryformat='%{NAME}.%{ARCH}\n' 2> /dev/null | head -n 1
  elif command -v pacman > /dev/null ; then
    # pacman's -Qo looks into PATH and is aware of the /bin symlink too.
    # With -q it returns exactly the package name.
    pacman -Qqo "$process" 2> /dev/null
  fi
}

# Test if we can use sudo. If it works, we'll get the processes that own the ports.
# If sudo rejects us, run ss as user. An error message from sudo will let the user know the output could get better.
# We assume sudo is installed because scanning servers as root is far from recommendable.
ss='ss'
sudo -n ss -V > /dev/null && ss='sudo -n ss'

$ss -tulnp '! ( src 127.0.0.1/8 or src [::1] )' \
  | awk '
    NR > 1 {
      proto = toupper($1)
      if (match($7, /^users:\(\("[^" ]+/))
        process = substr($7, 10, RLENGTH - 9)
      else
        process = ""
      if (match($5, /[0-9]+$/)) { # accept only valid ports
        port = substr($5, RSTART)
        print proto "|" port "|" process
      }
    }' \
  | sort -u \
  | while IFS='|' read -r proto port process ; do
      echo "$proto:$port|$(find_package "$process")"
    done
