# Scan des vulnérabités CPU
# https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-devices-system-cpu
sys_path='/sys/devices/system/cpu/vulnerabilities'
content_regex='^Vulnerable:\s(.*)'
if [ -r "$sys_path" ] ; then
  find "$sys_path" | while read -r file
  do
    content=$(< "$file")
    if [[ "$content" =~ ^Vulnerable ]]; then
      echo 'SECURITY_ISSUE:'
      echo "  sid: $(echo "$file" | cut -d'/' -f 7)"
      if [[ "$content" =~ $content_regex ]]; then
        echo "  info: ${BASH_REMATCH[1]}"
      fi
    fi
  done
fi
