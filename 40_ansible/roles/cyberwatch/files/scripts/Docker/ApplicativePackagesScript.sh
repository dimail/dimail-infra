#!/bin/sh

{ find "${SEARCH_PATH:-/}" \( -path '/dev' -o -path '/proc' -o -path '/sys' -o -path '/run' -o -path '/mnt' -o -path '/media' -o -path '/var/lib/docker' -o -name 'lost+found' \) -prune \
      -o -type f \( -name 'VERSION' -o -name 'bootstrap.inc' -o -iname 'version.php' -o -name '*.[jw]ar' \
                    -o -name 'package-lock.json' -o -name 'yarn.lock' \
                    -o -name composer.lock \! -path '*/vendor/*' \
                    -o -name 'METADATA' -path '*/dist-packages/*' \
                    -o -executable \)
} | while read -r path
do
  clean_path=${path#"${SEARCH_PATH}"}
  case "$path" in
  # Cyberwatch
  */olympe/VERSION)
    version=$(cat "$path")
    echo "NVD_APPLICATION:cpe:/a:cyberwatch:cyberwatch_application:$version|$clean_path"
    ;;

  # Drupal
  # pour les images des versions récentes (8+), on se base sur les données de Composer
  # pour les anciennes versions (7.x), on se base sur la version déclarée dans bootstrap.inc
  # Dans les images Docker officielles, il n'y a pas de répertoire Drupal racine,
  # c'est installé directement dans /var/www/html
  # on vérifie donc la présence de drupal.js relativement au chemin de bootstrap.inc
  # pour s'assurer que l'on est bien dans Drupal.
  *includes/bootstrap.inc)
    # define('VERSION', '7.94');
    end_path="includes/bootstrap.inc"
    drupaljs="${path%"$end_path"}misc/drupal.js"
    if [ -f "$drupaljs" ]; then
      sed -nE "s/^\s*define\('VERSION', '(([0-9]+\.)?[0-9]+\.[0-9]+)'\);$/cpe:\/a:drupal:drupal:\1/p" "$path" |
        while read -r cpe ; do echo "NVD_APPLICATION:$cpe|$clean_path" ; done
    fi
    ;;

  # Joomla!
  */joomla/libraries/src/Version.php)
  # public const MAJOR_VERSION = 4;
    cpe=$(sed -nE 's/^\s*(public )?const MAJOR_VERSION = ([0-9]+);$/cpe:\/a:joomla:joomla%21:\2/p;
                   s/^\s*(public )?const MINOR_VERSION = ([0-9]+);$/.\2/p;
                   s/^\s*(public )?const PATCH_VERSION = ([0-9]+);$/.\2/p' "$path" | tr -d '\n')
    echo "NVD_APPLICATION:$cpe|$clean_path"
    ;;

  # Nextcloud / owncloud (deprecated) / owncloud/server
  */occ)
    # Nextcloud 25.0.4
    # ownCloud 10.0.10
    # ownCloud version 9.1.8
    $path -V |
      sed -nE 's/^Nextcloud ([0-9]+\.[0-9]+\.[0-9]+).*$/cpe:\/a:nextcloud:nextcloud_server:\1/p;
               s/^ownCloud (version )?([0-9]+\.[0-9]+\.[0-9]+)$/cpe:\/a:owncloud:owncloud:\2/p' |
      while read -r cpe ; do echo "NVD_APPLICATION:$cpe|$clean_path" ; done
    ;;

  # Tomcat
  */tomcat/bin/version.sh)
    # Server version: Apache Tomcat/9.0.73
    "$path" |
      sed -nE 's/^Server version: Apache Tomcat\/([0-9]+\.[0-9]+\.[0-9]+).*$/cpe:\/a:apache:tomcat:\1/p' |
      while read -r cpe ; do echo "NVD_APPLICATION:$cpe|$clean_path" ; done
    ;;

  # Wordpress
  */wp-includes/version.php)
    # $wp_version = '6.1';
    sed -nE "s/^[$]wp_version = '(([0-9]+\.)?[0-9]+\.[0-9]+)';$/cpe:\/a:wordpress:wordpress:\1/p" "$path" |
      while read -r cpe ; do echo "NVD_APPLICATION:$cpe|$clean_path" ; done
    ;;

  # Zend
  # dans le cas de Zend, l'exe PHP n'est pas dans le PATH et est donc ignoré
  # on ne traite donc ici que le cas où l'exe n'est pas dans le PATH
  */php)
    # PHP 7.2.34
    if ! command -v php > /dev/null ; then
      "$path" -v |
        sed -n 's/^PHP \([0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/p' |
        while read -r version ; do echo "LINUX_APPLICATION:php|$version|$clean_path" ; done
    fi
    ;;

  # Ignore les package-lock.json et yarn.lock définis sous les node_modules.
  */node_modules/*/package-lock.json) ;;
  */node_modules/*/yarn.lock) ;;

  # NPM
  */package-lock.json)
    awk -F': ' -v clean_dir="$(dirname -- "$clean_path")" '
      # La hiérarchie qui nous intéresse est la suivante :
      #
      # {
      #   "packages": {
      #     "node_modules/@aws-crypto/crc32/node_modules/tslib": {
      #      "version": "1.14.1",
      #
      # Dans ce cas, le nom du paquet est `tslib`, et la version `1.14.1`.

      /^    "node_modules\// {
        path = $1
        sub(/^ *"/, "", path)
        sub(/"$/, "", path)
        package = path
        sub(/(^|.*\/)node_modules\//, "", package)
      }

      package && $1 == "      \"version\"" {
        version = $2
        sub(/^"/, "", version)
        sub(/",?$/, "", version)
        print "NPM:" package "|" version "|" clean_dir "/" path
        package = ""
        path = ""
      }
    ' "$path"
    ;;

  # Yarn (JavaScript)
  # Yarn uses the same packages as NPM, so we generate the same NPM info property.
  */yarn.lock)
    awk -v clean_path="$clean_path" '
      # (Yarn 3) "@babel/code-frame@7.12.11":
      # (Yarn 3) type-check@^0.4.0, type-check@~0.4.0:
      # (Yarn 4) "wrap-ansi-cjs@npm:wrap-ansi@^7.0.0, wrap-ansi@npm:^7.0.0":
      # (Yarn 4) __metadata:
      /^[^ ].+:$/ {
        sub(/^"/, "")
        sub(/"?:$/, "")
        sub(/,.*$/, "")
        sub(/@[^@]*$/, "")
        sub(/@npm:.*$/, "")
        package = $0
      }

      # (Yarn 3)  version "7.12.11"
      # (Yarn 4)  version: 7.12.11
      package && ($1 == "version" || $1 == "version:") {
        version = $2
        gsub(/"/, "", version)
        if (package != "__metadata") {
          print "NPM:" package "|" version "|" clean_path
        }
        package = ""
      }
    ' "$path"
    ;;

  # Composer (PHP)
  */composer.lock)
    awk -F': ' -v clean_path="$clean_path" '
      $1 == "    \"packages\"" { in_packages = 1 }
      $1 == "    }" { in_packages = 0 }

      in_packages && $1 == "            \"name\"" {
        package = $2
        sub(/^"/, "", package)
        sub(/",?$/, "", package)
      }

      in_packages && package && $1 == "            \"version\"" {
        version = $2
        sub(/^"v?/, "", version)
        sub(/",?$/, "", version)
        print "COMPOSER:" package "|" version "|" clean_path
        package = ""
      }
    ' "$path"
    ;;

  # Maven (Java)
  #
  # Cherche les .jar et .war sur le système et convertit leur nom de la forme NOM-VERSION.jar en `MAVEN:NOM|VERSION`.
  # `/usr/share/elasticsearch/lib/lz4-java-1.8.0.jar` devient `MAVEN:lz4-java|1.8.0`. Cette nomenclature correspond au
  # dépôt Maven, quoiqu’on n’a aucune garantie que le JAR en provienne.
  #
  # Le premier `s` de sed élimine les dossiers pour ne garder que le nom de base du fichier. Le deuxième `s` supprime
  # l’extension et scinde sur le premier `-1` où `1` représente n’import quel chiffre. Comme le quantificateur `*` de sed
  # est glouton, c’est la capture du nom qui doit s’arrêter au premier signe d’une version qui commence.
  #
  # Cette détection se plante dans le cas de `google-api-services-storage-v1-rev20220705-2.0.0.jar` où la version est en
  # fait `v1-rev171-1.25.0`, tandis que pour `proto-google-iam-v1-1.17.1` la version est `1.17.1`. C’est inconciliable.
  #
  *.[jw]ar)
    echo "$path" |
      sed -ne 's/^.*\/// ; s/^\([^-]*\(-\|-[^0-9-][^-]*\)*\)-\([0-9].*\)\.[^.]*$/MAVEN:\1|\3/p' |
      sed -e 's/^MAVEN:log4j|1\.2-api-/MAVEN:log4j-1.2-api|/' |
      while read -r package ; do echo "$package|$clean_path" ; done
    ;;

  # Packages Python
  */METADATA)
    package=$(sed -nE 's/^Name: (.+)$/\1/p;
                       s/^Version: (.+)$/|\1/p' "$path" | tr -d '\n')
    echo "PIP:$package|$clean_path"
    ;;

  # Find All executable files on shellless images
  *)
    if [ -n "$SEARCH_PATH" ] && [ -x "$path" ]; then
      product="${path##*/}"
      echo "DOCKER_APPLICATION:$product||$clean_path"

      # Les exécutables Go ont une section ELF nommée .go.buildinfo qui contient des lignes comme :
      #
      #   mod	github.com/docker/compose/v2	(devel)
      #   dep	golang.org/x/crypto	v0.7.0	h1:AvwMYaRytfdeVt3u6mLaxYtErKYjxA2OXjJ1HHq6t3A=
      #   build	-ldflags="-s -w -X main.version=0.11.0 …
      #
      # Ce bloc d’information apparait parfois plusieurs fois, donc on indique à awk de s’arrêter après le premier. Si
      # on trouve une occurrence de `main.version=…`, on génère un GO_MODULE avec le nom extrait de la propriété `mod`.
      #
      # Busybox est lent, mais c’est tout ce qu’on a. busybox grep met 3 secondes à dire si un exécutable de 55 Mo
      # contient `.go.buildinfo`, contre 50 millisecondes avec GNU grep. busybox awk met ensuite 2 secondes à le
      # traiter.
      #
      if grep -Fq .go.buildinfo "$path" ; then
        awk -F'	' -v executable="$clean_path" '
          $1 == "mod" {
            mod = $2
          }

          mod {
            if ($1 == "dep") {
              sub(/^v/, "", $3)
              print "GO_MODULE:" $2 "|" $3 "|" executable
            } else if ($1 == "build") {
              if (mod && match($2, /main.version=\S+/)) {
                version = substr($2, RSTART + 13, RLENGTH - 13)
                print "GO_MODULE:" mod "|" version "|" executable
              }
            } else if ($1 !~ /^\w+$/) {
              exit
            }
          }
        ' "$path"
      fi
    fi
    ;;
  esac
done

# Apache server
if command -v apache2 > /dev/null ; then
  # Server version: Apache/2.4.55 (Unix)
  apache2 -v | sed -nE 's/^Server version: Apache\/([0-9]+\.[0-9]+\.[0-9]+).*$/NVD_APPLICATION:cpe:\/a:apache:http_server:\1/p'
fi
if command -v httpd > /dev/null ; then
  # Server version: Apache/2.4.55 (Unix)
  httpd -v | sed -nE 's/^Server version: Apache\/([0-9]+\.[0-9]+\.[0-9]+).*$/NVD_APPLICATION:cpe:\/a:apache:http_server:\1/p'
fi

# Grafana
if command -v grafana-server > /dev/null ; then
  # Version 8.5.21 (commit: f5c82deccc, branch: HEAD)
  grafana-server -v | sed -nE 's/^Version ([0-9]+\.[0-9]+\.[0-9]+).*$/NVD_APPLICATION:cpe:\/a:grafana:grafana:\1/p'
fi

# HAProxy
if command -v haproxy > /dev/null ; then
  # HAProxy version 2.7.3-1065b10 2023/02/14 - https://haproxy.org/
  # HA-Proxy version 2.2.29-c5b927c 2023/02/14 - https://haproxy.org/
  # HAProxy version 2.8-dev12-f48b23f 2023/05/17 - https://haproxy.org/
  haproxy -v | sed -nE 's/^HA\-?Proxy version ([0-9]+\.[0-9]+(\.[0-9]+|-dev)?).*$/NVD_APPLICATION:cpe:\/a:haproxy:haproxy:\1/p'
fi

# MongoDB
if command -v mongod > /dev/null ; then
  # db version v6.0.4
  mongod --version | sed -nE 's/^db version v([0-9]+\.[0-9]+\.[0-9]+)$/NVD_APPLICATION:cpe:\/a:mongodb:mongodb:\1/p'
fi

# NGINX
if command -v nginx > /dev/null ; then
  # nginx version: nginx/1.23.3
  nginx -v 2>&1 | sed -ne 's/^nginx version: nginx\/\([0-9]\+\.[0-9]\+\.[0-9]\+\).*$/NVD_APPLICATION:cpe:\/a:nginx:nginx:\1/p'
fi

# Ruby
if command -v ruby > /dev/null ; then
  # ruby 2.7.7p221 (2022-11-24 revision 168ec2b1e5) [x86_64-linux]
  ruby -v | sed -ne 's/^ruby \([0-9]\+\.[0-9]\+\.[0-9]\+\).*$/NVD_APPLICATION:cpe:\/a:ruby-lang:ruby:\1/p'
fi
