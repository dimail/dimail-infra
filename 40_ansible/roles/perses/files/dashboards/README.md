# Perses dashboards

## Converting from Grafana to Perses

Note that we force JSON output here because the UI only support importing/exporting JSON.

```sh
docker run \
  --entrypoint=percli \
  --volume <ABSOLUTE_PATH_TO_DASHBOARD>:/dashboard.json \
  persesdev/perses \
    migrate \
    --schemas.charts /etc/perses/cue/schemas/panels \
    --schemas.queries /etc/perses/cue/schemas/queries \
    --schemas.variables /etc/perses/cue/schemas/variables \
    --file /dashboard.json \
    --output json
```

## Imported dashboards

* [`node-exporter.json`](https://grafana.com/grafana/dashboards/1860-node-exporter-full/) (revision 37)
* [`postfix.json`](https://grafana.com/grafana/dashboards/10013-postfix/) (revision 2)
* [`nginx.json`](https://github.com/nginxinc/nginx-prometheus-exporter/tree/3219f58928e42c6df732400ff363ae013bbc1ed2/grafana)
* [`mysql.json`](https://grafana.com/grafana/dashboards/14057-mysql/) (revision 1)
* [`dovecot.json`](https://alfaexploit.com/files/dovecot_prometheus/dovecot_prometheus.json), from this [blog post](https://alfaexploit.com/en/posts/monitoring_dovecot_with_prometheus/)
