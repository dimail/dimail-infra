#!/bin/bash

source /usr/lib/admin_mail/function_change_user;

USERNAME="$1"
OLD_DOMAIN="$2"
NEW_DOMAIN="$3"
CONTEXT_ID="$4"

echo "Moving user from $USERNAME@$OLD_DOMAIN to $USERNAME@$NEW_DOMAIN in ox context $CONTEXT_ID"

if ! ox_username_exists_in_context ${CONTEXT_ID} ${USERNAME}; then
	echo "Le username demandé n'existe pas dans open-xchange"
	exit 3
fi

if ! dovecot_username_exists_in_domain ${OLD_DOMAIN} ${USERNAME}; then
	echo "Le username demandé n'existe pas dans dovecot"
	exit 4
fi

if dovecot_username_exists_in_domain ${NEW_DOMAIN} ${USERNAME}; then
	echo 'Le user existe déjà dans le nouveau domaine';
	exit 2
fi


dovecot_disable_user "$OLD_DOMAIN" "$USERNAME";

move_the_maildir_to_a_new_domain "$OLD_DOMAIN" "$USERNAME" "$NEW_DOMAIN";

ox_change_user "$NEW_DOMAIN" "$USERNAME" "$CONTEXT_ID";

dovecot_change_user_domain "$OLD_DOMAIN" "$USERNAME" "$NEW_DOMAIN";
dovecot_enable_user "$NEW_DOMAIN" "$USERNAME";
