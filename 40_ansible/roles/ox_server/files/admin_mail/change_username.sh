#!/bin/bash
USERNAME="$1"
OLD_USERNAME="$2"
DOMAIN="$3"
CONTEXT_ID=$4


source /usr/lib/admin_mail/function_change_user;

if [ "$(username_exists)" != "0" ]; then
  if [ "$(is_new_username_exits_in_domain)" == "" ]; then
    disable_user;
    ox_enable_change_username;
    ox_reload_service;
    ox_update_username;
    dovecot_update_username;
    ox_disable_change_username;
    ox_reload_service;
    create_new_mail_folder;
    transfert_user_data;
    enable_user;
	else
	  echo 'The new username already exists';
	  exit 2
	fi
else
  echo "Requested username does not exist"
  exit 3
fi
