#!/bin/bash

############################################################
# Help                                                     #
############################################################
Help()
{
  # Display Help
  echo "This script is used to change user's password"
  echo
  echo "Syntax: $0 [-p|A|c|d|g|s|e|D|h]"
  echo "options:"

  echo "-A    The password of the database user 'support'"
  echo "-u    The username"
  echo "-D    The mail domain of the user"
  echo "-p    (optional) The password to provide for user's authentication to dovecot and OX"

  echo "Example command:"
  echo "$0 -A thedbpassword -u auser -d mail.numerique.gouv.fr"
  exit 1
}

while getopts "d:u:p:A:" option; do
  case $option in

    u) # username
      username=$OPTARG;;

    d) # domain
      domain=$OPTARG;;

    p) # password
      password=$OPTARG;;

    A) # password of the database "support" user
      dbpassword="$OPTARG";;

    \?) # Invalid option
      echo "Error: Invalid option"
      Help;;
  esac
done

# Check that dbpassword is set
if [ -z "$dbpassword" ]; then
  echo "Error: argument -A is missing (the password of the database user 'support')"
  Help
fi

# Check that domain is set
if [ -z "$domain" ]; then
  echo "Error: argument -d is missing (the domain of the user)"
  Help
fi

# Check that username is set
if [ -z "$username" ]; then
  echo "Error: argument -u is missing (the username of the user)"
  Help
fi

# generate password if empty
if [ -z "$password" ]; then
  password=$(openssl rand -base64 21)
fi

hash_algorithm="ARGON2ID"
encryptedpwd=$(doveadm pw -s "$hash_algorithm" -p "$password" 2>/dev/null)

mysql -h {{ hostvars[groups['sql_master'].0]['priv_fqdn'] }} -u support -p"$dbpassword" -e "UPDATE dovecot.users SET password = '$encryptedpwd' WHERE username = '$username' AND domain = '$domain';"

echo "Password for $username@$domain has been changed to $password"

exit 0
