---
- name: "Check if context was already created : {{ context.name }}"
  ansible.builtin.command: >
    /opt/open-xchange/sbin/existscontext
    --adminuser {{ ox_master_username }}
    --adminpass {{ ox_master_password }}
    --contextid {{ context.id }}
  register: context_exists
  changed_when: false
  failed_when: context_exists.rc not in [0, 1] or 'exist' not in context_exists.stdout
  check_mode: false
  tags: [test_data]

- name: List domains for this context (+ defaultcontext for cid=1)
  ansible.builtin.set_fact:
    context_domains: "{{ mail_domains
                         | selectattr('features', 'contains', 'webmail')
                         | selectattr('ox_cluster', 'equalto', ox_name)
                         | selectattr('context_id', 'equalto', context.id)
                         | map(attribute = 'name')
                         | list + ([] if context.id != '1' else ['defaultcontext']) }}" # noqa jinja[spacing]
  tags: [test_data]

- name: "Create context : {{ context.id + ' - ' + context.name }}"
  run_once: true
  when: context_exists.stdout != "Context "+context.id+" exists"
  ansible.builtin.command: >
    /opt/open-xchange/sbin/createcontext
    --adminuser {{ ox_master_username }}
    --adminpass {{ ox_master_password }}
    --contextid {{ context.id }}
    --contextname {{ context.name }}
    --addmapping {{ context_domains | join(',') }}
    --quota "{{ context.quota | default('1024') }}"
    --access-combination-name="{{ context.access_combination | default('groupware_standard') }}"
    --language "{{ context.language | default('fr_FR') }}"
    --timezone "{{ context.timezone | default('Europe/Paris') }}"
    --username {{ context.admin_username }}
    --password {{ context.admin_password }}
    --displayname "Context Admin"
    --givenname Admin
    --surname Context
    --email oxadmin@{{ context_domains | first }}
  register: context_creation
  changed_when: context_creation is success
  tags: [test_data]

- name: "Ensure context is well configured : {{ context.name }}"
  run_once: true
  when: context_exists.stdout == "Context "+context.id+" exists"
  ansible.builtin.command: >
    /opt/open-xchange/sbin/changecontext
    --adminuser {{ ox_master_username }}
    --adminpass {{ ox_master_password }}
    --contextid {{ context.id }}
    --contextname {{ context.name }}
    --addmapping {{ context_domains | join(',') }}
    --quota "{{ context.quota | default('1024') }}"
    --access-combination-name="{{ context.access_combination | default('groupware_standard') }}"
  register: context_update
  changed_when: context_update is success
  tags: [test_data]
