---
- name: Get context id
  ansible.builtin.set_fact:
    context_id: "{{ mail_domains | selectattr('name', 'equalto', user.domain) | map(attribute='context_id') | first }}"
  tags: [test_data]

- name: Get context informations
  ansible.builtin.set_fact:
    context_info: "{{ client_contexts | selectattr('cluster', 'equalto', ox_name) | selectattr('id', 'equalto', context_id) | first }}"
  tags: [test_data]

- name: Set Open-Xchange credentials
  ansible.builtin.set_fact:
    ox_cli_admin_context_credentials: >
      --adminuser {{ context_info.admin_username }}
      --adminpass {{ context_info.admin_password }}
      --contextid {{ context_id }}
  tags: [test_data]

- name: Set user email as variable for {{ user.username }}
  ansible.builtin.set_fact:
    user_email: "{{ user.username }}@{{ user.domain }}"
  tags: [test_data]

- name: "Check if user was already created : {{ user_email }}"
  run_once: true
  ansible.builtin.command: >
    /opt/open-xchange/sbin/existsuser
    {{ ox_cli_admin_context_credentials }}
    --username {{ user.username }}
  register: user_exists
  changed_when: false
  failed_when: user_exists.rc not in [0, 1] or 'exist' not in user_exists.stdout
  check_mode: false # because it should run even in check mode
  tags: [test_data]

- name: "Create user in Open-Xchange : {{ user_email }}"
  when: user_exists.rc == 1
  run_once: true
  ansible.builtin.command: >
    /opt/open-xchange/sbin/createuser
    {{ ox_cli_admin_context_credentials }}
    --username {{ user.username }}
    --password "{{ user.password }}"
    --displayname "{{ user.displayName }}"
    --givenname "{{ user.givenName }}"
    --surname "{{ user.surname }}"
    --email {{ user_email }}
    --language {{ user.language | default('fr_FR') }}
    --timezone {{ user.timezone | default('Europe/Paris') }}
  register: user_created
  changed_when: true
  failed_when: user_created.rc not in [0, 1] or 'created' not in user_created.stdout
  tags: [test_data]

- name: "Update user in Open-Xchange : {{ user_email }}"
  when: user_exists.rc == 0
  run_once: true
  ansible.builtin.command: >
    /opt/open-xchange/sbin/changeuser
    {{ ox_cli_admin_context_credentials }}
    --username {{ user.username }}
    --password "{{ user.password }}"
    --displayname "{{ user.displayName }}"
    --givenname "{{ user.givenName }}"
    --surname "{{ user.surname }}"
    --email {{ user_email }}
    --language {{ user.language | default('fr_FR') }}
    --timezone {{ user.timezone | default('Europe/Paris') }}
  register: user_updated
  changed_when: true
  failed_when: user_updated.rc not in [0, 1] or 'changed' not in user_updated.stdout
  tags: [test_data]

- name: "Encrypt user password with doveadm pw with ARGON2ID : {{ user_email }}"
  run_once: true
  ansible.builtin.command:
    cmd: doveadm pw -s ARGON2ID -p "{{ user.password }}"
  changed_when: false
  register: encrypted_password
  delegate_to: "{{ groups['imap_master'].0 }}"
  tags: [test_data]

- name: "Insert or update user in dovecot database : {{ user_email }}"
  run_once: true
  community.mysql.mysql_query:
    login_user: dovecot
    login_password: "{{ mysql_dovecot_pwd }}"
    login_host: "{{ hostvars[groups['sql_master'].0].priv_fqdn }}"
    query: >
      INSERT INTO dovecot.users
        (username, domain, password, proxy, active)
      VALUES
        (
          %(username)s,
          %(domain)s,
          %(encryptedpassword)s,
          %(proxy)s,
          %(active)s
        )
      ON DUPLICATE KEY UPDATE
        password = VALUES(password),
        proxy = VALUES(proxy),
        active = VALUES(active)
      ;
    named_args:
      username: "{{ user.username }}"
      domain: "{{ user.domain }}"
      encryptedpassword: "{{ encrypted_password.stdout }}"
      proxy: "N"
      active: "Y"
  register: user_inserted
  changed_when: user_inserted.rowcount[0] == 1
  tags: [test_data]
