---
title: Outlook Mobile
sidebar_position: 2
sidebar_label: Outlook
---

import DomainpagesFeatures from '@site/src/components/DomainpagesFeatures';


## Sommaire

1. [Installation](#1-installation)
2. [Configuration](#2-configuration)

## 1. Installation

### Sur Apple

Ce produit est censé être disponible dans l'app Store.

## 2. Configuration

### Automatique

1. Appuyer sur le bouton `Ajouter un compte`.

![email mobile](/img/outlook_mobile/outlook1.png)

2. Entrer votre adresse mail personnelle.

![email mobile](/img/outlook_mobile/outlook2.png)

3. Appuyer sur le bouton `Continuer`.
4. Sélectionner `IMAP` dans la liste proposée. 

![email mobile](/img/outlook_mobile/outlook3.png)

5. Entrer dans le formulaire
  * Adresse de courrier: Votre adresse mail personnelle
  * Nom complet
  * Description
6. Appuyer sur `Paramètres avancés`
7. Remplir dans le serveur de courrier entrant IMAP: 
   * Nom d'hôte IMAP: voir tableau ci-dessous
   * Port: voir tableau ci-dessous
   <DomainpagesFeatures></DomainpagesFeatures>
   * Type de sécurité: sélectionner `SSL/TLS` dans la liste. 

![email mobile](/img/outlook_mobile/outlook4.png)


![email mobile](/img/outlook_mobile/outlook5.png)

8. Entrer votre adresse mail personnelle dans `Nom d'utilisateur IMAP`.
9. Ecrire votre mot de passe personnel.
10. Remplir le serveur de courrier sortant SMTP:
  * Nom d'hôte SMTP: Voir tableau ci-dessous
  * Port: Voir tableau ci-dessous
<DomainpagesFeatures></DomainpagesFeatures>

![email mobile](/img/outlook_mobile/outlook6.png)

11. Dans type de sécurité, sélectionner `SSL/TLS` dans la liste.

![email mobile](/img/outlook_mobile/outlook7.png)

12. Enfin, mettre dans le formulaire: 
  * Nom d'utilisateur SMTP: votre adresse mail personnelle
  * Mot de passe SMTP: votre mot de passe personnel

![email mobile](/img/outlook_mobile/outlook8.png)

13. Appuyer sur le sigle en "v", en haut à droite pour finir la procédure. 
14. Appuyer sur le bouton `Peut-être plus tard`.

![email mobile](/img/outlook_mobile/outlook9.png)



