---
title: Gmail Mobile
sidebar_position: 3
sidebar_label: Gmail
---
import DomainpagesFeatures from '@site/src/components/DomainpagesFeatures';


## Sommaire

1. [Installation](#1-installation)
2. [Configuration](#2-configuration)

## 1. Installation

### Sur Apple

Ce produit est censé être disponible dans l'app Store.

## 2. Configuration

### Automatique

1. Appuyer sur `Autre`.

![email mobile](/img/gmail_mobile/gmail1.png)

2. Ajouter votre adresse mail personnelle.

![email mobile](/img/gmail_mobile/gmail2.png)

3. Appuyer sur le bouton `Suivant`.
4. Sélectionner l'option `Personnel (IMAP)`.

![email mobile](/img/gmail_mobile/gmail3.png)

5. Se connecter grâce à son mot de passe personnel.

![email mobile](/img/gmail_mobile/gmail4.png)

6. Appuyer sur le bouton `Suivant`.
7. Renseigner dans le formulaire IMAP:
  * Nom d'utilisateur: Votre adresse mail personnelle
  * Mot de passe: Votre mot de passe personnel
  * Serveur: Voir le tableau ci-dessous
<DomainpagesFeatures></DomainpagesFeatures>

![email mobile](/img/gmail_mobile/gmail5.png)

! Ici, nous avons le message d'erreur `Nom d'utilisateur ou mot de passe incorrect.`. Il est discret, mais cela montre qu'il y a un souci avec vos identifiants.

7. Appuyer sur le bouton `Suivant`. 
8. Renseigner le formulaire SMTP: 
   * Nom d'utilisateur: Votre adresse mail personnelle
   * Mot de passe: Votre mot de passe personnel
   * Serveur: Voir le tableau ci-dessous
<DomainpagesFeatures></DomainpagesFeatures>

![email mobile](/img/gmail_mobile/gmail6.png)

9. Régler vos préférences

![email mobile](/img/gmail_mobile/gmail7.png)

10. Appuyer sur le bouton `Suivant`. 
11. Remplir vos coordonnées:
  * Nom de compte (facultatif): votre adresse mail personnelle
  * Votre nom

![email mobile](/img/gmail_mobile/gmail8.png)

12. Appuyer sur le bouton `Suivant`.
