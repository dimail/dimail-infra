---
title: K-9 Mail Mobile
sidebar_position: 2
sidebar_label: K-9 Mail
---

import DomainpagesFeatures from '@site/src/components/DomainpagesFeatures';


## Sommaire

1. [Installation](#1-installation)
2. [Configuration](#2-configuration)

## 1. Installation

### Sur Apple

Ce produit est censé être disponible dans l'app Store.

## 2. Configuration

1. Appuyer sur le bouton `Suivant`.

![email mobile](/img/k9_mobile/k1.png)

2. Renseigner votre adresse mail personnelle.

![email mobile](/img/k9_mobile/k2.png)

3. Appuyer sur le bouton `Configuration manuelle`. 
4. Renseigner dans le formulaire IMAP:
  * Serveur IMAP: voir tableau ci-dessous
  * Sécurité: sélectionner SSL/TLS
  * Port: voir tableau ci-dessous
  * Nom d'utilisateur: votre adresse mail personnelle
<DomainpagesFeatures></DomainpagesFeatures>

![email mobile](/img/k9_mobile/k3.png)

5. Dans `Authentification`, choisir `Mot de passe chiffré` dans le menu. 

![email mobile](/img/k9_mobile/k4.png)

6. Entrer votre mot de passe personnelle. 
7. Laisser le paramètre `Certificat client` sur `Aucun certificat client`. 
8. Décocher `Détection automatique de l'espace de nommage IMAP`

![email mobile](/img/k9_mobile/k5.png)

9. Renseigner le préfixe de chemin IMAP selon le tableau ci-dessous.
<DomainpagesFeatures></DomainpagesFeatures>

![email mobile](/img/k9_mobile/k6.png)

10. Décocher `Utiliser la compression`

    ![email mobile](/img/k9_mobile/k7.png)

11. Appuyer sur le bouton `Suivant`.
12. Appuyer sur le bouton `Continuer`.

![email mobile](/img/k9_mobile/k8.png)

13. Renseigner dans le formulaire SMTP:
    * Serveur SMTP: voir tableau ci-dessous
    * Sécurité: sélectionner SSL/TLS
    * Port: voir tableau ci-dessous
    * Authentification exigée: cocher le choix
    * Nom d'utilisateur: votre adresse mail personnelle
      <DomainpagesFeatures></DomainpagesFeatures>

![email mobile](/img/k9_mobile/k9.png)

14. Dans `Authentification`, choisir `Mot de passe chiffré` dans le menu.
15. Entrer votre mot de passe personnelle.
16. Laisser le paramètre `Certificat client` sur `Aucun certificat client`.
17. Appuyer sur le bouton `Suivant`. 
18. Appuyer sur le bouton `Continuer`.

![email mobile](/img/k9_mobile/k10.png)

19. Appuyer sur le bouton `Suivant`.

![email mobile](/img/k9_mobile/k11.png)

20. Donner un nom à votre compte.
21. Saisir votre nom qui s'affichera dans les courriels. 

![email mobile](/img/k9_mobile/k12.png)


22. Appuyer sur le bouton `Terminer`.
