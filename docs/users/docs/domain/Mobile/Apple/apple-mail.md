---
title: Mail iCloud Mobile
sidebar_position: 1
sidebar_label: Mail iCloud
---
import DomainpagesFeatures from '@site/src/components/DomainpagesFeatures';

![Work in progress](/img/wip.png)
## Sommaire

1. [Installation](#1-installation)
2. [Configuration](#2-configuration)

## 1. Installation

### Sur Apple

Ce produit devrait être déjà installé par défaut sur votre machine.

## 2. Configuration

### Automatique

![Work in progress](/img/wip.png)

Actuellement, ce n'est pas encore possible de configurer en automatique Mail iCloud.

### Manuel

1. Aller dans l'onglet `Compte` dans les réglages

![renseigner email](/img/apple_mail_mobile/step1.png)

2. Appuyer sur l'onglet `Ajouter un compte`.

![renseigner email](/img/apple_mail_mobile/step2.png)

3. Sélectionner `Autre`.

![renseigner email](/img/apple_mail_mobile/step3.png)

4. Appuyer sur le bouton `Ajouter un compte Mail`.

![renseigner email](/img/apple_mail_mobile/step4.png)

5. Remplir le formulaire ci-dessous avec vos informations personnelles.

![renseigner email](/img/apple_mail_mobile/step5.png)

![renseigner email](/img/apple_mail_mobile/step6.png)

6. Dans le formulaire `serveur de réception`, remplir le formulaire IMAP:
  * Nom d'hôte : Voir tableau ci-dessous
  * Nom d'utilisateur: votre adresse mail personnelle
  * Mot de passe: votre mot de passe personnel
<DomainpagesFeatures></DomainpagesFeatures>

![renseigner email](/img/apple_mail_mobile/step7.png)

7. Dans le formulaire `serveur d'envoi`, remplir le formulaire SMTP:
  * Nom d'hôte : Voir tableau ci-dessous
  * Nom d'utilisateur: votre adresse mail personnelle
  * Mot de passe: votre mot de passe personnel
<DomainpagesFeatures></DomainpagesFeatures>

  ![renseigner email](/img/apple_mail_mobile/step9.png)

8. Enfin, appuyer sur le bouton `Suivant` en haut à droite. 
