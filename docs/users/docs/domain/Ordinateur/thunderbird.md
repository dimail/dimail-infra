---
title: Thunderbird PC
sidebar_position: 1
sidebar_label: Thunderbird
---
import DomainpagesFeatures from '@site/src/components/DomainpagesFeatures';

## Sommaire

1. [Installation](#1-installation)
2. [Configuration](#2-configuration)

## 1. Installation

### Sur Linux

Suivez la documentation officiel de [mozilla firefox](https://support.mozilla.org/fr/kb/installer-thunderbird-sous-linux)

### Sur Windows

1. Télécharger thunderbird sur le lien suivant : [Télécharger](https://www.thunderbird.net/fr/)
2. Appuyer sur télécharger
3. Suivez les étapes d'installation
4. Lancer Thunderbird

### Sur Apple

Vous pouvez utiliser la commande : 

```
brew install thunderbird
```

Sinon suivez la documentation officiel de [Mozilla Firefox](https://support.mozilla.org/fr/kb/installation-thunderbird-mac)

## 2. Configuration

Pour configurer manuellement Thunderbird. 

1. Renseigner votre nom complet, votre email et votre mot de passe.

![Login thunderbird](/img/thunderbird-login.png)

Appuyer ensuite sur le bouton `Configure manually` 

![Configure manually location](/img/configure-manually-location.png)

Cela fera apparaître le formulaire suivant :

![Formulaire IMAP/SMTP](/img/form-imap-smtp.png)

Renseigner alors votre serveur imap et smtp avec les valeurs suivantes :

<DomainpagesFeatures></DomainpagesFeatures>
