---
title: Outlook PC
sidebar_position: 2
sidebar_label: Outlook
---
import DomainpagesFeatures from '@site/src/components/DomainpagesFeatures';

## Sommaire

1. [Installation](#1-installation)
2. [Configuration](#2-configuration)

## 1. Installation

### Sur Windows

L'application est installée de base dans votre machine

### Sur Apple

Présent sur l'[App Store](https://apps.apple.com/fr/app/microsoft-outlook/id951937596)

## 2. Configuration

1. Renseigner votre identifiant. 

![renseigner email](/img/outlook_pc/step7.png)

2. Appuyer sur le bouton `Continuer`.

![renseigner email](/img/outlook_pc/step8.png)

3. Sélectionner le fournisseur `IMAP`.

![renseigner email](/img/outlook_pc/step9.png)

4. Appuyer sur le bouton `Continuer`.

![renseigner email](/img/outlook_pc/step10.png)

5. Remplir le formulaire apparu avec les données suivantes. Attention, il se peut que certaines informations se remplissent par automatisme dans le formulaire :
  * Adresse de courrier: votre adresse mail personnelle
  * Nom d'utilisateur IMAP: votre adresse mail personnelle
  * Mot de passe IMAP: votre mot de passe personnel 
  * Serveur entrant IMAP: renseigner l'URI selon les données du tableau suivant
  * Port IMAP: renseigner le port IMAP selon les données du tableau suivant

<DomainpagesFeatures></DomainpagesFeatures>

![renseigner email](/img/outlook_pc/step11.png)

6. Sélectionner dans le menu `Démarrer TLS`

![renseigner email](/img/outlook_pc/step12.png)

7. Terminer les configurations `SMTP` toujours en se référant au tableau ci-dessus (Voir `5`.).

![renseigner email](/img/outlook_pc/step13.png)

   * Nom d'utilisateur SMTP: votre adresse mail personnelle
   * Mot de passe SMTP: votre mot de passe personnel
   * Serveur sortant SMTP: renseigner l'URI selon les données du tableau ci-dessus (voir `5`.)
   * Port SMTP: renseigner le port SMTP selon les données du tableau ci-dessus (voir `5`.)

8. Comme à l'étape ci-dessus, `6`, sélectionner dans le menu `Démarrer TLS`. 
9. Veuillez toujours laisser cocher `Afficher les paramètres avancés`.
10. Appuyer sur le bouton `Ajouter un compte`.

![renseigner email](/img/outlook_pc/step14.png)

11. Enfin, personnaliser Outlook selon vos goûts en sélectionnant vos préférences. 
    * Appuyer sur le bouton `Continuer` une fois la personnalisation faite.

![renseigner email](/img/outlook_pc/step15.png)
