---
title: Mail iCloud MAC
sidebar_position: 3
sidebar_label: Apple Mail
---
import DomainpagesFeatures from '@site/src/components/DomainpagesFeatures';


## Sommaire

1. [Installation](#1-installation)
2. [Configuration](#2-configuration)

## 1. Installation

### Sur Apple

Ce produit est censé être installé par défaut dans votre machine

## 2. Configuration

1. Choisir `Autre compte Mail...`

![renseigner email](/img/apple_mail_pc/step1.png)

2. Appuyer sur le bouton `Continuer`
3. Renseigner les informations liées à votre compte mail.

![renseigner email](/img/apple_mail_pc/step2.png)

4. Appuyer sur le bouton `Se connecter`
  * Comme le serveur ne possède pas de configuration automatique, le formulaire suivant devrait apparaître.

![renseigner email](/img/apple_mail_pc/step3.png)

5. Renseigner les informations suivantes:

* Type de compte: IMAP
* Serveur de réception: écrire l'information URI IMAP ci-dessous séparé par un `:` avec le port
* Serveur d'envoi: écrire l'information URI SMTP ci-dessous séparé par un `:` avec le port

<DomainpagesFeatures></DomainpagesFeatures>

Voici un exemple de résultat attendu (les ports n'ont pas été renseignés, car port par défaut)

![renseigner email](/img/apple_mail_pc/step4.png)

6. Appuyer sur le bouton `Se connecter`
7. Sélectionner les applications `Mail` et `Notes` en les cochant.

![renseigner email](/img/apple_mail_pc/step5.png)

8. Appuyer sur le bouton `Terminé`
9. Sélectionner l'option `Protéger votre activité dans Mail`

![renseigner email](/img/apple_mail_pc/step6.png)

10. Appuyer sur le bouton `Continuer`.
