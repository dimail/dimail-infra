---
title: Configurer votre client lourd préféré
sidebar_position: 1
sidebar_label: Sommaire
---

# Configurer votre client lourd préféré

## Pour cela, choisissez votre client ci-dessous

### Notre WebMail

* [Documentation Webmail](webmail) (Documentation à produire)

### Pour votre Ordinateur

* [Thunderbird](Ordinateur/thunderbird)
* [Outlook](Ordinateur/outlook) 
* [Mail iCloud](Ordinateur/apple-mail) (Mac Only)

### Pour vos Appareils Mobile

* [Android - K-9 Mail](Mobile/Android/k9mail)
* [Android - Outlook](Mobile/Android/outlook)
* [Apple - Mail iCloud](Mobile/Apple/apple-mail)
