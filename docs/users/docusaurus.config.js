// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import { themes } from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
export default {
  title: 'Documentation Open-Xchange',
  tagline: 'La messagerie collaborative de la Suite Numérique',
  favicon: 'img/favicon_RF.png',

  // Set the production url of your site here
  url: 'https://your-docusaurus-site.example.com',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: 'assistance/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'etalab', // Usually your GitHub org/user name.
  projectName: 'infra-mail-dgf', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'throw',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://github.com/numerique-gouv/dimail-infra',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'Documentation Open-Xchange DINUM',
        logo: {
          alt: 'RF Logo',
          src: 'img/favicon_RF.png',
        },
        items: [
          {
            type: 'doc',
            position: 'left',
            docId: 'welcome',
            label: 'Introduction',
          },
          {
            type: 'docSidebar',
            sidebarId: 'IMAPSidebar',
            position: 'left',
            label: 'IMAP',
          },
          {
            type: 'docSidebar',
            sidebarId: 'POP3Sidebar',
            position: 'left',
            label: 'POP3',
          },
          {
            type: 'docSidebar',
            sidebarId: 'SMTPSidebar',
            position: 'left',
            label: 'SMTP',
          },
          {
            type: 'docSidebar',
            sidebarId: 'domain',
            position: 'left',
            label: 'Votre Configuration'
          }
        ],
      },
      footer: {
        style: 'dark',
        links: [],
        copyright: `Copyright © ${new Date().getFullYear()} Documentation Open-Xchange DINUM`,
      },
      prism: {
        theme: themes.duotoneLight,
        darkTheme: themes.duotoneDark,
      },
    }),
};
