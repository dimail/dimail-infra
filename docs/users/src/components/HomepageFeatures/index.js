import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Restez connectés' ,
    Img: require('@site/static/img/ox-mail-only-devices.png').default,
    description: (
      <>
        Nous avons choisis Open Xchange afin que vous restiez connecter sur l'ensemble de vos appareils.
      </>
    ),
  },
  {
    title: 'Compatible' ,
    Img: require('@site/static/img/emailing.png').default,
    description: (
      <>
        Nous avons choisis Open Xchange pour une compatibilité avec votre client lourd préféré.
      </>
    ),
  },
];

function Feature({Img, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img className={styles.featureSvg} role="img" src={Img} />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className={styles.row}>
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
