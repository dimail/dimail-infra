import useIsBrowser from "@docusaurus/useIsBrowser";

function my_domains(hostname) {
  const host = hostname.split('.');
  host[0] = ""
  hostname = host.join('.');
  return {
    data: {
      webmail: 'https://webmail.data.gouv.fr',
      smtp: {
        url: 'smtp.data.gouv.fr',
        port: 995
      },
      imap: {
        url: 'imap.data.gouv.fr',
        port: 465
      }
    },
    numerique: {
      webmail: 'https://webmail.numerique.gouv.fr',
      smtp: {
        url: 'smtp.numerique.gouv.fr',
        port: 995
      },
      imap: {
        url: 'imap.numerique.gouv.fr',
        port: 465
      }
    },
    defaults: {
      webmail: 'https://webmail' + hostname,
      smtp: {
        url: 'smtp' + hostname,
        port: 465
      },
      imap: {
        url: 'imap' + hostname,
        port: 993
      }
    }
  }
}

function extractContext(hostname) {
  let context = hostname.split('.');
  if(context.length >= 1){
    return context;
  }
  return null;
}

export default function DomainpagesFeatures() {
  const hostname = useIsBrowser() ? location.hostname : "localhost";
  const domains = my_domains(hostname);
  const context = extractContext(hostname)
  const domain = domains.hasOwnProperty(context)? domains[context]: domains["defaults"];

  return (<table>
    <tr>
      <th></th>
      <th>URI</th>
      <th>PORT</th>
    </tr>
    <tr>
      <td>IMAP</td>
      <td>{domain.imap.url}</td>
      <td>{domain.imap.port}</td>
    </tr>
    <tr>
      <td>SMTP</td>
      <td>{domain.smtp.url}</td>
      <td>{domain.smtp.port}</td>
    </tr>
  </table>);
}
