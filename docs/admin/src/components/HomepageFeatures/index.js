import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Personnalisez cette documentation facilement !',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        Docusaurus vous permet de mettre à jour facilement cette documentation.
        Retrouvez le code Docusaurus du projet <a href="https://github.com/numerique-gouv/dimail-infra/tree/main/docs">ici-même.</a>
      </>
    ),
  },
  {
    title: 'Publiez votre progression',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        Des notes de patch à partager ? Faites connaître vos avancées dans le répertoire <code>docs/blog</code>.
      </>
    ),
  },
  {
    title: "Une flopée d'extensions",
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Implémentez des fonctionnalités grâce aux plugins Docusaurus. Soyez créatifs en faisant usage de composants React !
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
