# Architecture

## Architecture technique

Découpage technique des environnements : régions, zones réseaux, et instances.

![Schéma d'architecture technique](../../static/img/architecture-technique.excalidraw.png)

## Architecture fonctionnelle

Vue par composants et flux fonctionnels.
À noter que la quasi-totalité flux sont chiffrés (port TLS ou bien STARTTLS).

![Schéma d'architecture fonctionnelle](../../static/img/architecture-fonctionnelle.excalidraw.png)

## Architecture stockage et backups

Description des données stockées et de la gestion des sauvegardes.
Les services sans état ne sont pas représentés.

| Composant                      | Stockage           | Données                                                         | Backup                                                                 |
| ------------------------------ | ------------------ | --------------------------------------------------------------- | ---------------------------------------------------------------------- |
| Webmail backend (Open-Xchange) | Filesystem         | cache, upload PJ, drive                                         | ❌                                                                      |
| Webmail backend (Open-Xchange) | MySQL (2 réplicas) | Comptes locaux, autres service (agenda, carnet d'adresse, …), … | `mysqldump` et `rclone` 1/j vers un bucket S3                          |
| API                            | MySQL (1 réplica)  | Domaines                                                        | ❌                                                                      |
| cert-manager                   | Filesystem         | certificats, fragments de conf NGINX, postfix, dovecot          | ❌                                                                      |
| Mail server (Postfix)          | Filesystem         | Domaines                                                        | ❌                                                                      |
| Mail server (Postfix)          | MySQL (2 réplicas) | alias, relay                                                    | `mysqldump` et `rclone` 1/j vers un bucket S3                          |
| IMAP server (Dovecot)          | Filesystem         | emails, dossiers, …                                             | `rsync` 1/h vers une instance backup et `rclone` 1/j vers un bucket S3 |
| IMAP server (Dovecot)          | MySQL (2 réplicas) | comptes                                                         | `mysqldump` et `rclone` 1/j vers un bucket S3                          |

## Architecture migration de domaine

![Schéma d'architecture de migration de domaine](../../static/img/architecture-migration-domaine.excalidraw.png)

1. Accès aux mails d'un domaine en cours de migration;
2. Récupération des informations du compte (`username`, `domaine`, `password`, `imap_proxy`, etc.) ainsi que du status de migration. Se base sur ces information pour décider où récupérer les emails;
3. Si le compte n'est pas local, connexion au Dovecot autoritaire avec des identifiants admin et le nom du compte en question (username: `<compte>*master`, password: `<master_password>`).

## Architecture API

L'API Dimail centralise la gestion de la configuration de la plateforme et permet d’exécuter diverses actions (création, modification ou suppression de boîtes aux lettres, utilisateurs, groupes, domaines, etc.) via des appels HTTP, sans nécessiter de modifications du code Ansible.

L’utilisation d’appels HTTP facilite l’intégration avec d'autres outils ou scripts, permettant d'automatiser les tâches d'administration courantes. Par ailleurs, les journaux d'activité assurent la traçabilité des actions réalisées.

La spécification OpenAPI est disponible [ici](https://api.ox.numerique.gouv.fr/docs).

Responsabilité et flux fonctionnels de l'API:

![Schéma d'architecture API](../../static/img/architecture-api.excalidraw.png)
