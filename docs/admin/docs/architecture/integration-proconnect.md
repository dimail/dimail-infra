# Intégration ProConnect

## Connexion au webmail avec ProConnect

```mermaid
sequenceDiagram
title Séquence de connexion au webmail avec ProConnect

participant user as Resource owner<br/>(utilisateur)
participant rp as Relying Party ou Client<br/>(Open-Xchange)
%% participant proxy as Proxy Identity Provider<br/>(Keycloak)
participant idp as Identity Provider ou Authorization server<br/>(ProConnect)

user->>rp: Clique sur "se connecter avec ProConnect"<br/>GET https://webmail.numerique.gouv.fr/appsuite/api/oidc/init
rp->>rp: Génère un code_verifier aléatoire (protège contre l'interception du authorization_code)
rp->>rp: Génère un code_challenge en hashant (SHA-256) le code_verifier
rp->>rp: Génère un state aléatoire (protège contre les attaques CSRF)
rp->>rp: Génère un nonce aléatoire (protège contre les attaques "replay")
rp->>user: Redirige vers la page d'autorisation<br/>302 https://www.proconnect.gouv.fr/???/?client_id=messagerie<br/>&redirect_uri=https://webmail.numerique.gouv.fr/appsuite/api/oidc/auth<br/>&scope=openid&response_type=code&code_challenge=???<br/>&code_challenge_method=S256&state=???&nonce=???
user->>idp: …Suit la redirection vers la page d'autorisation<br/>GET https://www.proconnect.gouv.fr/…
idp->>idp: Génère un session_code aléatoire pour corréler<br/>différentes requêtes liées à un même flow de connexion
idp->>idp: Stocke le code_challenge
idp->>idp: Vérifie si une session existe déjà pour cet utilisateur (si oui sauter à l'étape ⏩)
idp->>user: Redirige vers la page de connexion<br/>302 https://www.proconnect.gouv.fr/???/?client_id=messagerie<br/>&client_id=messagerie&tab_id=???&session_code=???&client_data=???<br/>avec client_data contenant redirect_uri, state, et response_type en base64
user->>idp: …Suit la redirection vers la page de connexion<br/>GET https://www.proconnect.gouv.fr/…
idp->>user: Répond avec la page de connexion<br/>200 page de connexion
user->>idp: Renseigne ses identifiants<br/>POST https://www.proconnect.gouv.fr/???/
idp->>idp: Vérifie les identifiants
idp->>idp: Vérifie si le consentement a déjà été donné (si oui sauter à l'étape ⏩)
idp->>user: Répond avec la page de consentement<br/>200 page de consentement
user->>idp: Consent à l'accès à ses données personnelles<br/>POST https://www.proconnect.gouv.fr/???/
idp->>user: ⏩ Redirige vers la redirect_uri avec le authorization_code<br/>302 https://webmail.numerique.gouv.fr/appsuite/api/oidc/auth
user->>rp: …Suit la redirection vers l'URL de callback<br/>GET https://webmail.numerique.gouv.fr/appsuite/api/oidc/auth?…
rp->>idp: Récupère un ID Token et un Access Token<br/>POST https://www.proconnect.gouv.fr/???/?grant_type=authorization_code<br/>&redirect_uri=https://webmail.numerique.gouv.fr/appsuite/api/oidc/auth<br/>&code_verifier=???&client_id=messagerie&client_secret=???&code=???
idp->>idp: Vérifie que le code_verifier correspond au code_challenge
idp->>idp: Vérifie l'authorization_code
idp->>rp: Répond avec ID Token et Access Token<br/>200 avec ID Token et Access Token
rp->>rp: Stocke l'ID Token et l'Access Token associés à la session utilisateur
rp->>user: Connecte l'utilisateur au webmail et renvoie un cookie de session (?)<br/>200 page d'accueil du webmail (boite de réception vide)
```

## Chargement des emails d'un utilisateur connecté au webmail avec ProConnect

```mermaid
sequenceDiagram
title Séquence de chargement des emails d'un utilisateur connecté au webmail avec ProConnect

participant user as Resource owner<br/>(utilisateur)
participant rp as Relying Party ou Client<br/>(Open-Xchange)
%% participant proxy as Proxy Identity Provider<br/>(Keycloak)
participant idp as Identity Provider ou Authorization server<br/>(ProConnect)
participant api as Resource Server<br/>(Dovecot)

user->>rp: Charge ses mails (avec un clic ou bien en tâche de fond) avec le cookie de session attaché<br/>GET https://webmail.numerique.gouv.fr/appsuite/api/???
rp->>rp: Récupère l'ID Token et l'Access Token associés à la session utilisateur
rp->>idp: Récupère des informations supplémentaires sur l'utilisateur (claims) sur l'URL de user info<br/>POST https://www.proconnect.gouv.fr/??? avec Authorization: Bearer <access_token><br/>⚠️ Problème, avec ProConnect l'ID token est pauvre en claims et Open-Xchange<br/>ne sait pas faire cet appel
idp->>idp: Vérifie que l'access_token est valide
idp->>rp: Répond avec les claims<br/>200 avec les claims
rp->>api: Accède aux emails de l'utilisateur déterminé grâce aux claims et authentifié avec l'Access Token<br/>IMAP LOGIN, SELECT, SEARCH, …
api->>rp: Répond avec les emails de l'utilisateur
rp->>user: Répond avec les emails de l'utilisateur
```

## Envoi d'un email par un utilisateur connecté au webmail avec ProConnect

```mermaid
sequenceDiagram
title Séquence d'envoi d'un email par un utilisateur connecté au webmail avec ProConnect

participant user as Resource owner<br/>(utilisateur)
participant rp as Relying Party ou Client<br/>(Open-Xchange)
%% participant proxy as Proxy Identity Provider<br/>(Keycloak)
participant idp as Identity Provider ou Authorization server<br/>(ProConnect)
participant api as Resource Server<br/>(Dovecot)

user->>rp: Envoie un mail avec le cookie de session attaché<br/>POST https://webmail.numerique.gouv.fr/appsuite/api/???
rp->>rp: Récupère l'ID Token et l'Access Token associés à la session utilisateur
rp->>idp: Récupère des informations supplémentaires sur l'utilisateur (claims) sur l'URL de user info<br/>POST https://www.proconnect.gouv.fr/??? avec Authorization: Bearer <access_token><br/>⚠️ Problème, avec ProConnect l'ID token est pauvre en claims et Open-Xchange<br/>ne sait pas faire cet appel
idp->>idp: Vérifie que l'access_token est valide
idp->>rp: Répond avec les claims<br/>200 avec les claims
rp->>api: Envoie un email émis par l'utilisateur déterminé grâce aux claims et authentifié avec l'Access Token<br/>SMTP HELO, MAIL FROM, RCPT TO, DATA, QUIT, …
api->>rp: SMTP 250 OK
rp->>user: HTTP 200 OK
```

## Solution de contournement à la limitation Open-Xchange (⚠️)

* Une solution rapide consiste à mettre un keycloak en "proxy" qui se charge d'enrichir l'ID token avec des informations sur l'utilisateur
* Une solution pérenne consiste à modifier le module OIDC d'Open-Xchange pour qu'il gère ce cas d'usage

## TODO

* Ajouter Keycloak au diagramme séquence
* Expliciter où les `nonce` et `state` sont vérifiés
* Ajouter les séquences de refresh et de déconnexion (voir [documentation Open-Xchange](https://documentation.open-xchange.com/develop/middleware/login_and_sessions/openid_connect_1.0_sso.html#web-sso-login-flow))
* Clarifier les interractions avec les serveurs SMTP et IMAP, et le rôle que joue le "Password Credentials Grant"
