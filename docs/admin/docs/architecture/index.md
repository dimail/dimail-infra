# Dossier d'Architecture Technique

## Contexte

Dimail est la solution de webmail de la suite numérique. Il vise à fournir une solution moderne, sécurisée et flexible à destinations des collectivités locales. Dimail s'inscrit dans une démarche stratégique d'indépendance technologique et de maîtrise des coûts, en s'appuyant sur des technologies open-source reconnues.

## Objectifs du produit

- Offrir un service de messagerie électronique fiable et performant, adapté aux exigences actuelles.
- Garantir une souveraineté totale.
- Fournir une solution personnalisable et évolutive en fonction des besoins des utilisateurs.
- Assurer la sécurité des communications.
- Maîtriser les coûts par rapport à des solutions propriétaires comparables.

## Moyens mis en œuvre

### Hébergement

Dimail est hébergé sur l'offre IaaS SecNumCloud d'Outscale.
Plusieurs centres de données [certifiés](https://en.outscale.com/certificate/) sont utilisés dans les régions `cloudgouv-eu-west-1` et **TODO**.
Seuls les composants publics sont exposés sur internet, toutes les autres communications se font via un réseau privé.

### Infrastructure logicielle

- **Nagios** : Outil de surveillance IT permettant de superviser les services, systèmes et infrastructures.
- **MySQL** : Système de gestion de bases de données relationnelles rapide et fiable.
- **MariaDB** : Fork de MySQL open source, offrant des améliorations en termes de performance et de fonctionnalités.
- **Rclone** : Outil en ligne de commande pour synchroniser et sauvegarder des données vers des services de stockage cloud.
- **Prometheus** : Système de monitoring et d'alerting basé sur des métriques, idéal pour les infrastructures modernes.
- **Nginx** : Serveur web et proxy inverse performant, souvent utilisé pour le load balancing.
- **Certbot** : Client automatique pour obtenir et renouveler des certificats SSL/TLS avec Let's Encrypt.
- **Let's Encrypt** : Autorité de certification gratuite et automatisée pour sécuriser les sites web avec des certificats SSL/TLS.
- **Debian** : Distribution Linux stable et open source, largement utilisée pour les serveurs et les postes de travail.
- **Grafana** : Plateforme de visualisation de données, utilisée pour créer des tableaux de bord interactifs et surveiller les métriques.
- **Podman** : Moteur de conteneurs compatible avec Docker.
- **S3** : Service de stockage via API fourni en SaaS par Outscale.
- **GitLab** : Service de gestion de code source, d’intégration et de déploiement continu.

### Composants applicatifs

- **Open Xchange** : Suite collaborative open source offrant des fonctionnalités de messagerie, calendrier, contacts et gestion de documents.
- **Dovecot** : Serveur IMAP/POP performant et sécurisé pour gérer les boîtes mail.
- **Postfix** : Serveur de messagerie rapide et sécurisé, utilisé pour envoyer et recevoir des emails.
- **SpamAssassin** : Filtre anti-spam configurable pour analyser et marquer les emails indésirables.
- **Keycloak** : Solution de gestion d'identité et d'accès open source avec support d’authentification unique (SSO).
- **Dimail API** : Exposition via une de la configuration du webmail (gestion des utilisateurs, des boites aux lettres, etc.)

### Disponibilité et résilience

Pour assurer une disponibilité et une résilience optimales, nous avons mis en place une stratégie reposant sur plusieurs mécanismes complémentaires.

Tout d’abord, un système de réplication inter-région a été déployé.
Cette configuration permet de dupliquer nos services et nos données dans une autre région géographique du fournisseur.
En cas de panne ou d’incident affectant le système principal, cette réplication garantit la continuité du service en basculant rapidement sur l’infrastructure secondaire, minimisant ainsi l’impact sur les utilisateurs.

En parallèle, nous effectuons régulièrement des sauvegardes complètes de nos données sous forme de dumps.
Ces sauvegardes sont archivées de manière sécurisée, afin de permettre une restauration intégrale en cas de sinistre critique ou de perte totale des systèmes primaires et secondaires.
Ce mécanisme est conçu pour couvrir des scénarios où la réplication seule ne suffirait pas à protéger l’intégrité des données.

Finalement, l'infrastructure étant décrite dans du code versionné et le déploiement étant automatisé, il est possible de reconstruire une plateforme à partir de zéro en cas de défaillance majeure.

### Environnements de développement

L'environnement de production est complété par quatre environnements de développement, conçus pour tester les mises en production et dérisquer les migrations. Les environnements d'anté-production sont configurés pour se rapprocher au maximum de l'environnement de production, afin d'assurer la fiabilité des vérifications.

### Intégration continue et déploiement

Nous automatisons l'intégration et les déploiements pour réduire les bugs, minimiser le temps requis et éliminer les erreurs associées aux processus manuels. De plus nous pratiquons l'Infrastructure as Code (IaC) permettant l'automatisation, la reproductibilité et le versionnement.

Pour cela nous nous appuyons sur : GitLab-CI pour effectuer des vérifications à chaque changement de code, Terraform pour provisionner l'infrastructure Outscale, et Ansible pour configurer les machines et déployer les applications.

### Exploitation et surveillance

- Surveillance proactive grâce à des outils tels que Nagios, Prometheus, et Grafana.
- Mise en place d'exporteurs pour mettre à disposition des métriques métier ou techniques.
- Récupération des métriques, stockage, et requêtage avec Prometheus.
- Visualisation et tableaux de bord avec Grafana.
- Alertes remontées dans Tchap en cas d'incident.

### Souveraineté et communs numériques

- Mise à disposition de la plateforme et accompagnement des collectivités souhaitant l'héberger elles-mêmes.
- Choix exclusif d'outils sous licenses open source.
- Interopérabilité grace à des protocoles standards (S3, CalDav, …).
- Hébergement cloud français et certifié.
- Hébergement public du code sur l'instance GitLab interministérielle mutualisée, avec un dépôt miroir sur GitHub pour donner accès au plus grand nombre.
- Volonté de reverser à la communauté en contribuant aux logiciels que nous utilisons, en particulier Open-Xchange.

### Sécurité

- Utilisation de certificats SSL/TLS (avec Let's Encrypt) pour chiffrer les échanges HTTP, SMTP, IMAP, SSH, SQL, etc.
- Filtre anti-spam configurable pour analyser et marquer les emails indésirables.
- Intégration à ProConnect pour se connecter via OIDC.
- Rapport d’erreur envoyé sur un flux de discussion `Tchap`.

### Performance et scalabilité

- Découplage des différentes brique permetant de les répliquer indépendamment pour monter en charge.
- Répartition de charge grace à un load balancer.
- Tests de charge automatisés effectués régulièrement.
- Hébergement cloud offrant une forte élasticité à la fois horizontale et verticale.
- Surveillance continue des temps de réponse.

### Moyens humains

- Une équipe composée de 7 personnes assurant différents rôles : product owner, techlead, architecte, spécialiste courriel, support, UX, spécialiste infra et ops, développeur·se front et back.
- Un support accessible directement dans l'application et via Tchap.
- La capacité d'accompagner utilisateur·ices et collectivités.
- Un hébergeur capable d'intervenir sur des incidents 24/7 et une équipe de développement disponible en heure ouvrées.

### Conformité et réglementations

- Respect du RGPD.
- Hébergeur français [certifiés](https://en.outscale.com/certificate/) SecNumCloud, HDS, et ISO 27001 notament.
- Centres de données en France.
- Utilisation de logiciels sous licence libre et open source.
- Priorité donnée aux logiciels développés en France ou en Europe.

## Détail de la solution

Davantage d'informations sur l'architecture interne et son implémentation sont disponibles dans ces documents :

- [Architecture technique](./architecture.md#architecture-technique)
- [Architecture fonctionnelle](./architecture.md#architecture-fonctionnelle)
- [Architecture stockage et backups](./architecture.md#architecture-stockage-et-backups)
- [Architecture migration de domaine](./architecture.md#architecture-migration-de-domaine)
- [Architecture API](./architecture.md#architecture-api)

D'autres éléments sont documentés en détail :

- [Gestion des certificats](./certificats.md)
- [Intégration ProConnect](./integration-proconnect.md)
- [Gestion DNS](./dns.md)
- [Organisation du code](../practices/organisation-du-code.md)
- [Pratiques et conventions](../practices/pratiques-et-conventions.md)
