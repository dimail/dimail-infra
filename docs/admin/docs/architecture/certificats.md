# Gestion des certificats

## Génération et déploiement d'un nouveau certificat par l'API/Ansible

```mermaid
sequenceDiagram
title Séquence de génération d'un certificat pour un nouveau domaine

participant api as API ou Ansible
participant webmail as Webmail
participant imap as IMAP
participant smtp as SMTP
participant cm as cert-manager

api->>+webmail: Connexion SSH
webmail->>webmail: Ajoute un fragment de configuration<br/>NGINX pour résoudre le challenge<br/>HTTP et recharge le service NGINX
webmail-->>-api: ;
api->>+imap: Connexion SSH
imap->>imap: Ajoute un fragment de configuration<br/>NGINX pour résoudre le challenge<br/>HTTP et recharge le service NGINX
imap-->>-api: ;
api->>+smtp: Connexion SSH
smtp->>smtp: Ajoute un fragment de configuration<br/>NGINX pour résoudre le challenge<br/>HTTP et recharge le service NGINX
smtp-->>-api: ;
api->>+cm: Connexion SSH
cm->>cm: Exécute la CLI Certbot pour<br/>générer un nouveau certificat
cm-->>-api: ;
api->>+webmail: Connexion SSH
webmail->>+cm: Exécute le client rsync
cm-->>-webmail: Récupère les certificats;
webmail-->>-api: ;
api->>+imap: Connexion SSH
imap->>+cm: Exécute le client rsync
cm-->>-imap: Récupère les certificats;
imap-->>-api: ;
api->>+smtp: Connexion SSH
smtp->>+cm: Exécute le client rsync
cm-->>-smtp: Récupère les certificats;
smtp-->>-api: ;
api->>+webmail: Connexion SSH
webmail->>webmail: Supprime le fragment de configuration<br/>NGINX pour résoudre le challenge<br/>HTTP et recharge le service NGINX
webmail->>webmail: Ajoute un fragment de configuration<br/>NGINX pour utiliser le nouveau<br/>certificat et recharge le service NGINX
webmail-->>-api: ;
api->>+imap: Connexion SSH
imap->>imap: Supprime le fragment de configuration<br/>NGINX pour résoudre le challenge<br/>HTTP et recharge le service NGINX
imap->>imap: Ajoute un fragment de configuration<br/>IMAP pour utiliser le nouveau<br/>certificat et recharge le service IMAP
imap-->>-api: ;
api->>+smtp: Connexion SSH
smtp->>smtp: Supprime le fragment de configuration<br/>NGINX pour résoudre le challenge<br/>HTTP et recharge le service NGINX
smtp->>smtp: Ajoute un fragment de configuration<br/>SMTP pour utiliser le nouveau<br/>certificat et recharge le service SMTP
smtp-->>-api: ;
```
