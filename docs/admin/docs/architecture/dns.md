# Gestion DNS

Notes :

* Le sous-domaine `mail` est en cours de décomissionnement
* Les domaines sont définis dans les fichiers `ansible/group_vars/*/domains.yml` du dépôt `dimail-infra-config`

## Domaines clients

Il s'agit de `CNAME`s vers le domaine technique pour les service, d'un include pour l'enregistrement SPF, d'un enregistrement `MX` pour le serveur de réception SMTP.

### Format

* `webmail.<domain_client>`
* `smtp.<domain_client>`
* `imap.<domain_client>`
* `MX <domain_client> 1 mx.ox.numerique.gouv.fr.`
* `TXT <domain_client> v=spf1 include:_spf.ox.numerique.gouv.fr <autres_directives>`

### Exemples

* `MX andv.gouv.fr 1 mx.ox.numerique.gouv.fr.`
* `TXT andv.gouv.fr v=spf1 include:_spf.ox.numerique.gouv.fr -all`
* `webmail.andv.gouv.fr`
* `imap.andv.gouv.fr`

## Domaines clients par défaut

Sur chaque plateforme, un domaine client `system.<domaine_hosting>` est créé pour des besoins techniques, comme tester la plateforme et jouer les tests de charge.

* `imap.system.osdev2.dimail1.numerique.gouv.fr`
* ⚠️ pas encore déployé en prod, pas de `imap.system.mail.numerique.gouv.fr`

Sur chaque plateforme, un domaine client `testing.<domaine_hosting>` est créé pour des besoins techniques, comme tester la plateforme et jouer les tests de charge.

* `imap.testing.osdev2.dimail1.numerique.gouv.fr`
* `imap.testing.mail.numerique.gouv.fr`

## Domaines techniques

Utile pour faciliter les migrations.

### Format

* `<service>.ox.numerique.gouv.fr`

### Exemples

* `_spf.ox.numerique.gouv.fr`
* `smtp.ox.numerique.gouv.fr` (envoi)
* `mx.ox.numerique.gouv.fr` (réception)
* `imap.ox.numerique.gouv.fr`
* `webmail.ox.numerique.gouv.fr`

## Domaines hosting

Utile pour Ansible, l'API, les accès administrateurs, et les communications techniques entre applications.

### Format

* `<nom_de_machine>.<nom_de_plateforme>.dimail1.numerique.gouv.fr`

### Exemples

* `main.ovhdev.dimail1.numerique.gouv.fr`
* `backup.ovhdev.dimail1.numerique.gouv.fr`
* `monitor.ovhdev.dimail1.numerique.gouv.fr`

## Domaines hosting privés

Uniquement côté Outscale, pour se connecter aux instances en SSH en passant par le bastion.

### Format

* `<nom_de_machine>.priv.<nom_de_plateforme>.dimail1.numerique.gouv.fr`

### Exemples

* `main.priv.osdev2.dimail1.numerique.gouv.fr`
