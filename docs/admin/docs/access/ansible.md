---
sidebar_position: 5
sidebar_label: Ansible Vault
---

# Accéder aux données chiffrées par Ansible

Lorsque l'on développe de la configuration automatisée, il peut arriver que l'on ait besoin de passer des données sensibles,\
par exemple :
- Des mots de passe
- Des clés d'APIs

Afin que ces données ne soient pas transmises en clair, nous utilisons un plugin d'Ansible nommé Ansible Vault.
Ansible Vault permet de chiffrer les données sensibles, et est parfaitement intégré à Ansible.

Voici comment cette technologie est utilisée dans ce projet :

Dans ```40_ansible_single``` se trouve un fichier dans lequel se trouve un mot de passe en clair, ici ```vaultpasswd_file```, ce fichier est mentionné dans le ```.gitignore```, donc il ne se trouve qu'en local.

Les données sensibles à déchiffrer se trouvent dans ```40_ansible_single/group_vars/dev/passwords.yml``` pour l'environnement de développement, dans ```40_ansible_single/group_vars/prod/passwords.yml``` pour l'environnement de production.


![test](../../static/img/vault_filesystem.png)


Vous pouvez déchiffrer et éditer ces fichiers en vous positionnant dans ```group_vars/dev``` ou ```group_vars/prod``` puis utilisant la commande :

```bash
ansible-vault edit passwords.yml --vault-pass-file ../../vaultpasswd_file
```

Pour simplement voir le contenu déchiffré du fichier :

```bash
ansible-vault view passwords.yml --vault-pass-file ../../vaultpasswd_file
```

Lorsque vous lancerez un playbook, ces fichiers seront automatiquement déchiffrés, car la configuration ```ansible.cfg``` précise déjà comment les déchiffrer.

Il existe aussi d'autres commandes comme :
- `create`
- `encrypt`
- `decrypt`
- et bien d'autres

Vous pouvez retrouver la documentation sur Ansible Vault [ICI](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html)


## Pour nos usages

Pour pouvoir déchiffrer les fichiers déjà présent, il vous faudra un mot de passe. A demander à l'un des mainteneurs du projet.
Créer ensuite votre fichier ```vaultpasswd_file``` à la racine de ```40_ansible_single``` et mettez-y le mot de passe sur une ligne.

Dans le cadre du projet, nous déchiffrons manuellement ces fichiers pour les usages suivants :

- Se connecter à la base de données MariaDB avec un user ```support```, utilisez la valeur de la clé ```support_pwd```
--> Principalement pour effectuer des opérations de support.

- Se connecter à la base de données MariaDB avec un user ```root```, utilisez la valeur de la clé ```new_root_pwd```
--> Très rarement utilisé.






