---
sidebar_position: 2
sidebar_label: OVH / Openstack
---

# Accéder aux consoles du Cloud Provider

L'infrastructure étant hébergé sur le Cloud Public OVH, vous devez avoir les accès à la console 
ainsi qu'aux clés d'API si vous êtes contributeur du projet.

Le Cloud public d'OVH se divise en 2 parties :
- Une partie des services se trouve sur OVH
- La deuxième partie des services se trouve sur une plateforme Openstack

Alors oui, c'est particulier, étrange même, mais OVH fonctionne comme cela pour les machines virtuelles. 
Nous déployons une même infrastructure sur ces 2 plateformes qui en réalité derrière n'en sont qu'une seule.

Il vous faudra donc des credentials pour la plateforme OVH et des credentials pour la plateforme Openstack :

## Accéder à OVH

Voici l'[URL d'authentification](https://www.ovh.com/auth/)


1. Vous aurez besoin d'une application mobile pour l'authentification qui est double-facteur (mot de passe + code sur le téléphone), 
   choisissez comme bon vous semble : `1Password`, `FreeOTP+`, `Google Authenticator`, ...
2. Faites une demande à une personne de l'équipe pour récupérer un QRCode ou une clé pour activer votre MFA
3. Demandez les credentials de la console OVH à Christophe Ninucci.


![test](../../static/img/OVH_Login.png)

## Accéder à Openstack

Sur OVH vous n'avez besoin que d'un seul login/mot de passe pour accéder à tous les environnements.\
Sur Openstack, Vous devrez utiliser 1 credentials pour chaque environnement (Dév et production).

Pour vous les créer, il vous faudra les credentials OVH. Voici comment procéder :

1. Rendez-vous sur l'onglet ```Public Cloud```

    ![test](../../static/img/Onglets_OVH.png)

2. Sélectionnez l'environnement de développement (infra-mail-dgf-staging) ou de production 
   (infra-mail-dgf) en haut à gauche de la console.

       ![test](../../static/img/OVH_envs.png)

3. Rendez-vous dans le menu ```Project Management``` puis ```Users & Roles```

4. Cliquez sur ```Add user``` puis donnez-lui les droits d'administrateur.

5. Le mot de passe devrait apparaître dans un flash après avoir créé l'utilisateur, 
   si vous l'avez perdu, vous pouvez le re-générer dans les options de l'utilisateur.

       ![test](../../static/img/OVH_user_creation.png)

6. Vérifiez que vous pouvez vous connecter sur [Horizon](https://horizon.cloud.ovh.net/auth/login) une fois l'utilisateur créé, 
   puis réitérez pour l'environnement sur lequel vous n'avez pas fait cette opération.

