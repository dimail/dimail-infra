---
sidebar_position: 3
sidebar_label: Fichier d'environnement
---

# Écrire son fichier d'environnement

Prérequis :
- Avoir accès au code sur GitHub
- Avoir les accès OVH
- Avoir les accès OVH/Openstack (Horizon)

_Explication des variables à définir :_
- `OVH_APPLICATION_KEY`, `OVH_APPLICATION_SECRET`, `OVH_CONSUMER_KEY`: Clés API pour OVH Cloud.
- `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`: Clés d'accès AWS pour le backend S3.
- `OS_USERNAME`, `OS_PASSWORD`, `OS_PROJECT_NAME`: Informations d'identification pour Horizon/Openstack.


Voici les étapes à suivre :

1. Après avoir cloné le repository du projet, prenez connaissance du fichier ```.env-example```.
2. Créez votre de fichier d'environnement ```.env-prod``` ou ```.env-prod```.
3. le `Terraform State` du projet est stocké dans un bucket S3 sur OVH. 
   Pour y avoir accès, il vous faut des clés d'APIs. 
   Pour accéder au Bucket, renseigné dans les variables ```AWS_ACCESS_KEY_ID``` et ```AWS_SECRET_ACCESS_KEY```. 
   Pour récupérer ces identifiants rendez-vous dans la console `OVH > Cloud Public > Users & Roles`. 
   Enfin, créez un utilisateur en cliquant sur ```Add User``` puis lui donner les droits de  ```Objectstore Operator```. 
   Un flash affichera les Credentials. 
   Si vous l'avez raté, vous pouvez régénérer un mot de passe dans les options de l'utilisateur.
                            ![test](../../static/img/s3_flash.png)

4. Récupérez les informations d'OVH en vous rendant sur [cette URL](https://eu.api.ovh.com/createToken/) 
   en mettant pour vos accès, une étoile ```*``` pour toutes les méthodes
   (```GET```, ```POST```, ```PUT```, ```DELETE```).
5. Renseignez les informations concerant Openstack (Horizon) en téléchargeant le fichier RC de votre utilisateur `admin` créé précédemment 
   (Sélectionnez n'importe quelle région lors du téléchargement).
6. Reprenez le fichier ```.env.example``` et remplissez chacun des champs comportant des étoiles ```*```.

> ⚠️ **Faites bien attention à ce que vos fichiers d'environnement soient bien identifiés par le ```.gitignore```**
