---
sidebar_position: 4
sidebar_label: SSH
---

# Se connecter aux machines

Les connexions aux machines se font par clé SSH. Assurez-vous d'en avoir une que vous utiliserez au quotidien.

## Déployer sa clé publique SSH 

Pour accéder au serveur, vous devez ajouter votre clé `ssh` publique dans le répertoire [assets/ssh_keys](https://github.com/numerique-gouv/assets/ssh_keys)
Une fois votre clé ajoutée, à la racine du projet, lancez les commandes suivantes pour la déployer sur les serveurs :

```bash
make init
make plan
make apply_vms
```

## Liste des serveurs OVH

```bash
# PRODUCTION
ssh debian@main.ox.numerique.gouv.fr     # Serveur principal 
ssh debian@backup.ox.numerique.gouv.fr   # Serveur de backup
ssh debian@monitor.ox.numerique.gouv.fr  # Serveur de monitoring

# DEVELOPPEMENT
ssh debian@main.dev.ox.numerique.gouv.fr     # Serveur principal 
ssh debian@backup.dev.ox.numerique.gouv.fr   # Serveur de backup
ssh debian@monitor.dev.ox.numerique.gouv.fr  # Serveur de monitoring
```

## Liste des serveurs OUTSCALE

Il faut préalablement avoir installé et configuré un tunnel `Wireguard` à l'aide de Christophe Ninucci

```bash

# DEVELOPPEMENT
ssh debian@main.dev.dimail1.numerique.gouv.fr     # Serveur principal 
ssh debian@backup.dev.dimail1.numerique.gouv.fr   # Serveur de backup
ssh debian@monitor.dev.dimail1.numerique.gouv.fr  # Serveur de monitoring
```

