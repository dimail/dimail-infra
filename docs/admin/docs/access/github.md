---
sidebar_position: 1
sidebar_label: GitHub
---

# Accéder au GitHub

Sur GitHub vous trouverez 2 éléments importants du projet :

- Le Kanban --> [Ici](https://github.com/orgs/numerique-gouv/projects/3)
- Le code versionné --> [Là](https://github.com/numerique-gouv/dimail-infra)

Pour y accéder, il  faut avoir un compte GitHub et être invité.\
Pour celà, contactez Christophe Ninucci sur Tchap.
