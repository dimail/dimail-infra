# Migrer de `ovhprod` vers `osprod` :surfer:

Cette page a pour but de décrire le mode opératoire d'une migration de plateforme en prenant pour exemple la migration
qui sera de la plateforme `ovhprod` vers la plateforme `osprod`.

## Glossaire
### `tech_domain`
Le domaine DNS qui comporte les enregistrements publiés pour les domaines clients.
_Par exemple : `smtp.numerique.gouv.fr` est un enregistrement de type **`CNAME`** 
qui pointe vers l'enregistrement `smtp.ox.numerique.gouv.fr`_

### `host_domain`
La zone DNS qui comporte les enregistrements DNS des machines de la plateforme utilisée et de leurs IPs.
_Par exemple : `ovhprod` est géré sur la zone DNS `ovhprod.dimail1.numerique.gouv.fr` 
et contient un enregistrement `main.ovhprod.dimail1.numerique.gouv.fr` de type **`A`** avec l'IP publique 
de la machine `main` de la plateforme `ovhprod`_

## Prérequis
- [x] Renouveler les mots de passe de la plateforme `osprod` 
  _objectif :_ s'assurer qu'ils sont différents des autres plateformes, du fait d'une création, copier/coller)
  _moyen :_ générer des nouveaux nots de passe dans le fichier des variables de `40_ansible_single/group_vars/osprod/passwords.yml`
- [x] intégrer dans ansible le nettoyage du fichier `nginx/letsencrypt/cert-proxy.conf` lorsqu'ils ne correspondent pas au `host_domain` des variables
- [x] créer un script pour déplacer les bases de données.
- [x] Créer sur la plateforme `ovhprod` le domaine client qui sera utilisé pour les tests `testing.dimail.ovh` (`certname = testingdimailovh`)
- [x] Créer sur OVHcloud le domain `old.ox.numerique.gouv.fr`
- [x] Fixer la configuration de dovecot pour qu'il accepte les connexions standard malgré la configuration proxy.
- [x] Mesurer le temps que prend un playbook complet qui ne fait rien sur la plateforme: ~7 minutes
- [x] Répétition générale sur `ovhdev` pour la phase Ia/Ib
- [x] Faire en sorte que les mails reçus sur `from` continuent d'être délivrés sur la plateforme `to` 
- [x] Organiser la mise en attente des mails (`proxy='W'`)
- [x] Traiter l'état d'arrêt des services d'une plateforme
- [ ] Améliorer la circulation des clés DKIM pour permettre leur installation sur `to` quand elles sont stockées dans assets/dkim/`from`
- [ ] Traiter la modification de la table `db_pool` pour prendre en compte le changement de nom de base openXchange

## État `ovhprod`
- `tech_domain` = `ox.numerique.gouv.fr`
- `host_domain` = `ox.numerique.gouv.fr`
  - ce domain ne doit pas rester un `host_domain` puisque c'est le `tech_domain` cible d'osprod après bascule.

## Processus de migration
### PHASE Ia : Mise en conformité de la plateforme `ovhprod`
- [x] création des clés DKIM pour testing.ox.numerique.gouv.fr et system.ox.numerique.gouv.fr
- [x] nettoyage du state terraform pour retirer les délégations du state sans les supprimer en vrai
- [x] `make apply_vms` sur terraform (refacto des enregistrements DNS)
- [x] `ansible-playbook playbook.yml -t all,api`

### PHASE Ib : Migration du `host_domain` de la plateforme `ovhprod`
- [x] Créer le nouveau `host_domain` = `ovhprod.dimail1.numerique.gouv.fr` 
- [ ] Renommer les clés dkim
  - testing.ox.numerique.gouv.fr -> testing.ovhprod.dimail1.numerique.gouv.fr
  - system.ox.numerique.gouv.fr -> system.ovhprod.dimail1.numerique.gouv.fr
- [ ] Renommer les domains de service system et testing (`group_vars/all/domains.yml`)
  - testing.ox.numerique.gouv.fr -> testing.ovhprod.dimail1.numerique.gouv.fr
  - system.ox.numerique.gouv.fr -> system.ovhprod.dimail1.numerique.gouv.fr
- [ ] Migrer les enregistrements DNS du `host_domain` `ovhprod.dimail1.numerique.gouv.fr`
  - `make apply_network` -> supprime/crée les enregistrements DNS
  - `make apply_vms` -> met à jour les metadata des machines sur openstack
- [ ] *(ansible)* jouer tout le playbook ( -t all,api )
  - Applique les nouveaux hostnames
  - Recopie les nouvelles clés dkim
  - Régénère les certificats `tech_{hostname}` car ils changent de nom de domaine
  - Régénère les certificats `client_system_*` et `client_testing_*`
  - Reconfigure les `hosts` et `services` nagios avec les nouveaux noms.
  - Reconfigure les noms hosts publics/privés dans les configs pour la communication intra-plateforme.

## PHASE II : Préparation de la plateforme `osprod`.
- [x] Créer `host_domain` et `tech_domain` sur ovhcloud
  - `host_domain`: `osprod.dimail1.numerique.gouv.fr`
  - `tech_domain`: `new.ox.numerique.gouv.fr`
- [x] configurer `host_domain` et `tech_domain` dans terraform/ansible
  - dans terraform (env-osprod/outscale.tfvars)
  - dans ansible (`group_vars/osprod/domains.yml`)
- [ ] configurer les paramètres de proxy dans le `client_domain` `testing.dimail.ovh` (`group_vars/all/domains.yml`)
```yml
  - name: testing.dimail.ovh
    delivery: "virtual"
    features: ["mailbox", "webmail", "mx"]
    ox_cluster: "single-server"
    context_id: "2"
    cert_name: testingdimailovh
    dkim_selector: ox2024
    platform: ovhprod
    proxy_to: osprod
    proxy_context_id: "2"
    proxy_ox_cluster: oximap
```
- [ ] Changer dans la console OVH les enregistrements DNS de `testing.dimail.ovh` pour pointer sur `new.ox.numerique.gouv.fr`
- [x] Installer les machines/DNS: terraform `apply_outscale`
- [x] Transmettre la configuration reverse dns pour l'IP des machines `main` et `backup` de la plateforme `osprod` au support outscale (support@outscale.com CC mireille.mege@outscale.com)
- [ ] Copier le certificats `testingdimailovh` de `ovhprod` vers `osprod`
  - `move_cert.sh testingdimailovh`
- [ ] Installer l'environnement (tout le playbook ansible `-t all,api`) 
- [ ] Recopier les bases mysql de dovecot / postfix / openXchange d'`ovhprod` -> `osprod`
  - `move_db.sh ovhprod:dovecot osprod:dovecot`
    - sur ovhprod/main: `DELETE FROM dovecot.users WHERE domain != 'testing.dimail.ovh'`;
    - puis `UPDATE dovecot.users SET proxy = 'Y'`; 
- [ ] Appliquer le playbook ansible sur `ovhprod` pour prendre en compte la modification de la variable proxy (master user)
- [ ] Tester, tester, tester (pondeuse ?) avec le domaine client testing
  - [ ] Se connecter à un compte distant (connexion sur `to` pour accéder à un compte sur `from` = proxy='Y')
  - [ ] Se connecter à un compte local (connexion sur `to` pour accéder à un compte sur `to` = proxy='N')
  - [ ] Recevoir un mail sur un compte distant
  - [ ] Recevoir un mail sur un compte local
- [ ] Baisser les TTL des enregistrements ovhprod (variable ttl dans terraform)

## PHASE III : Migration vers la nouvelle plateforme (proxy mode)
- `host_domain`: **osprod.dimail1.numerique.gouv.fr**
- `tech_domain` sur osprod = `ox.numerique.gouv.fr**`
- `tech_domain` sur ovhprod = `old.ox.numerique.gouv.fr`
- [ ] Configurer la plateform ovhprod en `proxy_to` dans `group_vars/all/proxy.yml`:
```yml
  ovhprod:
    proxy_to: 'osprod'
    context_id_map:
        "1": "1"     # service context
        "2": "2"     # main context
        "3": "3"     # moana
        "4": "4"     # collectivite.fr
    ox_cluster_map:
        single-server: oximap
    tech_domain:
      name: ox.numerique.gouv.fr
      cert_name: mecol_etalab
      dkim_selector: ox2023
    host_domain:
      name: ovhprod.dimail1.numerique.gouv.fr
``` 
- [ ] Copier les certificats `ovhprod` -> `osprod` (mise à niveau préalable indispensable PHASE I) 
  via lancement manuel d'un script bash `move_certs.sh` *(2e fois)*   
  liste des certificats à copier :
  - [ ] `mecol_etalab` (le domaine technique)
  - [ ] `datagouv`
  - [ ] `mailnumeriquegouvfr`
  - [ ] `numeriquegouvfr`
  - [ ] `betagouvovh`
  - [ ] `betagouvfr`
  - [ ] `andv`
  - [ ] `varzycollectivite`
  - [ ] `loceguinercollectivite`
- [ ] Modifier le domaine DNS `testing.dimail.ovh` pour qu'il pointe sur `ox.numerique.gouv.fr`
- [ ] Modifier la variable `tech_domain`@'osprod'
  - `new.ox.numerique.gouv.fr` devient `ox.numerique.gouv.fr`
  - dans `env-osprod/outscale.tf`
  - dans `group_vars/all/domains.yml::platform`
- [ ] Modifier la variable `tech_domain`@`ovhprod`
  - `ox.numerique.gouv.fr` devient `old.ox.numerique.gouv.fr`
  - dans `env-osprod/outscale.tf`
  - dans `group_vars/all/domains.yml::platforms`
- [ ] Appliquer (`ansible-playbook -t all,api`) sur `osprod` pour créer toute la configuration (cela devrait marcher car les certificats existent déjà)
  - doit arrêter les services `postfix`, `api`, `openXchange`, `dovecot`

- ====== OFFLINE À PARTIR DE LA PROCHAINE ÉTAPE ========
- [ ] sur ovhprod: `systemctl stop postfix`, `systemctl stop dovecot`, `systemctl stop open-xchange` pour devancer les TTL paresseux
- [ ] `terraform apply_network` sur `env-ovhprod`
	- provoque la suppression des enregistrements de l'ancien `tech_domain` ox.numerique.gouv.fr
	- provoque la création des enregistrements du `tech_domain` old.ox.numerique.gouv.fr pour la plateforme ovhprod
- [ ] `terraform apply_vms` sur `env-ovhprod`
        - met à jour les metadata openstack
- [ ] Recopier les bases mysql `ovhprod` -> `osprod` :
  - `api` -> `api`
  - `dovecot` -> `dovecot`
  - `postfix` -> `postfix`
  - `configdb` -> `oximap_config`
  - `oxdatabase_5` -> `oximap_database_5`
- [ ] Appliquer (`ansible-playbook -t all,api`) sur `ovhprod`
  - Modifie la configuration sur les serveurs pour appliquer `tech_domain`=`old.ox.numerique.gouv.fr`
  - Génère les nouveaux certificats pour `tech_domain`=`old.ox.numerique.gouv.fr`
  - Conserve arrêté le service `postfix`
  - Conserve arrêté le service `api`
  - Conserve arrêté le service `openXchange`
  - Conserve arrêté le service `dovecot` 
- [ ] sur `osprod`: `UPDATE dovecot.users SET proxy='Y'`
- [ ] `terraform apply` sur `env-osprod`, recrée les enregistrements DNS
- [ ] passer la plateforme en state = running
- [ ] `ansible-playbook playbook.yml -t all,api,test_data` sur `osprod`
  - Reconfigure la configuration nginx  cert-user
  - Applique l'URL mysql dans la BDD config
  - Démarre les services 
- ====== LE SERVICE EST ONLINE ========
- [ ] Vérifier que les mails arrivent vers la nouvelle plateforme et sont distribués sur l'ancienne via LMTP (guess…), 
  les serveurs SMTP retardataires continuent de distribuer vers `ovhprod` 
- [ ] Tester, tester, tester (pondeuse ?) avec des domaines clients "véritables"
  - [ ] Se connecter à un compte distant
  - [ ] Se connecter à un compte local
  - [ ] Recevoir un mail sur un compte distant
  - [ ] Recevoir un mail sur un compte local
- À partir de ce moment, tous les nouveaux comptes ou nouvelles configurations devront être effectuées sur la nouvelle plateforme.

## Transfert des comptes de **ovhprod** vers **osprod**
> TODO: savoir mettre en attente le delivery des mails qui arrivent avec un proxy='W' (pour wait)

- Sélectionner un `$compte`@`$domain` à transférer
- `rsync` du maildir pour transférer l'essentiel du maildir (timing dépendant 
- Mettre en attente l'arrivée des mails pour ce compte (comment ? peut être avec une 3° valeur du champ proxy ?) 
  `update users set proxy = 'W' where username = '$compte' and domain = '$domain'`
- `rsync` du maildir entre ovhprod et osprod pour ce compte afin d'avoir les dernières modifications
- Update dovecot.users set proxy='N' where username = `$compte` and domain = `$domain`

## Extinction de la lumière côté **ovhprod**
- `terraform destroy_ovh`
- ...
- Profite :beer:
