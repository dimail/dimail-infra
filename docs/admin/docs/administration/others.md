---
sidebar_label: Autres concepts
sidebar_position: 5
---
# Autres

## Taille maximale des mails

La taille maximale d'un mail ou de ce qu'il s'y trouve est défini par des propriétés qui peuvent venir de ```postfix```, ```nginx``` ou ```open-xchange```.
La liste suivante est non exhaustive (à alimenter si on trouve d'autres paramètres importants):


- ```message_size_limit``` dans ```etc/postfix/main.cf``` (celui-ci a été positionné à 20000000, soit `~20 MB`) &rarr; 
Il s'agit de la taille maximale d'un mail autorisé par postfix.
- ```client_max_body_size``` positionné `~15 MO;` dans ```/etc/nginx/sites-available/ox_nginx.conf``` &rarr;
Il s'agit de la taille de la requête qui passe par Nginx, elle est à graduer également en fonction de la taille maximale d'un mail.
- ```com.openexchange.mail.maxMailSize``` dans ```/opt/open-xchange/etc/mail.properties``` &rarr;
Il s'agit de la taille maximale d'un mail autorisé par open-xchange (il est paramétré par défaut à ```-1``` c'est-à-dire, infini.)
- ```com.openexchange.mail.signature.maxImageSize``` &rarr;
La taille maximale d'une signature dans un mail (`1 MB` par défaut)
- ```com.openexchange.mail.signature.maxImageLimit``` &rarr;
Le nombre d'images maximum autorisé dans un mail (3 par défaut)
