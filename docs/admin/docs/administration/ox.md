---
sidebar_label: Administration Open-Xchange
sidebar_position: 4
---

# Administration Open-Xchange

Retrouvez ici les liens vers le documentation Open-Xchange des différentes fonctions OX.

## Gestion des users

- [createuser](https://oxpedia.org/wiki/index.php?title=AppSuite:User_management#createuser)
- [deleteuser](https://oxpedia.org/wiki/index.php?title=AppSuite:User_management#deleteuser)
- [listuser](https://oxpedia.org/wiki/index.php?title=AppSuite:User_management#listuser)
- [getusercapabilities](https://oxpedia.org/wiki/index.php?title=AppSuite:User_management#getusercapabilities) (connaître les accès et droits de l'utilisateur)
- [changeuser](https://oxpedia.org/wiki/index.php?title=AppSuite:User_management#changeuser)

## Gestion des contextes

- [createcontext](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#createcontext)
- [deletecontext](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#deletecontext)
- [listcontext](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#listcontext)
- [disablecontext](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#disablecontext)
- [enablecontext](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#enablecontext)
- [disableallcontexts](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#disableallcontexts)
- [enableallcontexts](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#enableallcontexts)
- [getcontextcapabilities](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#getcontextcapabilities)
- [changecontext](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#changecontext)
- [getadminid](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management#getAdminId)

## Autres commandes

Il existe des dizaines d'autres commandes que nous n'avons pas encore explorées : elles ne sont pas toutes listées à un seul endroit dans la documentation mais vous pouvez les rechercher dans cette doc en regardant les noms des fonctions existantes dans ```/opt/open-xchange/sbin```.

## Config et propriétés

Vous trouverez des dizaines de fichiers de configuration dans ```/opt/open-xchange/etc/``` pour customiser le fonctionnement du serveur mail et des fonctionnalités d'Open-Xchange. Jusqu'ici nous n'avons touché qu'aux fichiers :

- mail.properties (propriétés de fonctionnement générales du mail sur open-xchange)
- caldav.properties (propriétés concernant la fonctionnalité de calendrier)
- cardav.properties (propriétés concernant la fonctionnalité de carnet d'addresse)
- imapauth.properties (propriétés concernant l'authentification imap)
- mailfilter.properties (propriétés concernant les filtres et mails d'absence)

