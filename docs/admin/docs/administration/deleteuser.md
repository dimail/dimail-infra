---
sidebar_label: Suppression d'utilisateurs
sidebar_position: 2
---

# Suppression d'utilisateurs

## Usage

Dans `/home/debian`, retrouver le script deleteuserscript.sh

example : ```./deleteuserscript.sh -A $supportuserpwd -c $contextid -u $username -d $domain```

- ```-A``` &rarr; Le mot de passe mysql de l'utilisateur support. (Le user support est utilisé pour effectuer des opérations sur les utilisateurs mail). Le mot de passe est accessible via ansible-vault dans ```40_ansible_single/group_vars/$ENV/passwords```, variable ```support_pwd```.
- ```-c``` &rarr; Dans openxchange, un utilisateur se crée au sein d'un contexte. Un contexte est un espace virtuel dans lequel se trouve des utilisateurs, cet espace peut-être modulé pour autoriser/interdire des fonctionnalités pour tous les utilisateurs en son sein. Un contexte est aussi associé à un/des domaines mail.

Dans notre cas il existe aujourd'hui 3 contexte associés chacun à un id :

```1``` &rarr; le contexte par défault, créé lors de l'installation de l'application

```2``` &rarr; le contexte des utilisateurs data.gouv.fr

```3``` &rarr; le contexte de l'utilisateur système

Nous utilisons le contexte numéro ```2``` pour gérer les utilisateur donc rentrer ```-c 2``` en production.
Nous utilisons le contexte numéro ```1``` en développement

- ```-u``` le username de l'utilisateur à supprimer.

- ```-d``` &rarr; Le domaine associé à l'utilisateur, pour l'instant ```data.gouv.fr``` en production, ```test.ox.numerique.gouv.fr`` en développement.


## Comment ça fonctionne ?

Pour supprimer un user il faut :

1. Supprimer sa ligne correspondante en base de données dans ```dovecot.users```.
2. Supprimer l'utilisateur des base de données openXchange avec la commande native ```deleteuser```.
3. Déplacer sa boite mail de ```/var/mail/imap_mailboxes/$domain/$us/$username```, avec ```$us``` les 2 premières lettres du username, vers ```/var/mail/imap_mailboxes/$domain/deleted```.
4. Supprimer ses alias du fichier ```/etc/postfix/virtual_alias``` puis reloader ce fichier.

Ce script effectue ces 4 étapes.

