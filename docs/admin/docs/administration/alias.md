---
sidebar_label: Gestion des alias
sidebar_position: 3
---

# Gestion des alias

Les alias se trouvent dans  ```/etc/postfix/virtual_aliases```
Faire les modifs nécessaires dans ce fichier puis :
```bash
postmap hash:/etc/postfix/virtual_aliases
```
puis :
```bash
service postfix reload
```

> :warning: **Un alias doit obligatoirement être mappée à une addresse mail minimum, sans quoi le reload du service postfix ne fonctionne pas.**





