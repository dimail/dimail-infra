---
sidebar_label: Notre config rsync
sidebar_position: 7
---
# Documentation sur rsync

## Introduction

Dans le cas de la messagerie, nous utilisons rsync pour synchroniser les certificats entre les différents serveurs composant nos stacks techniques.
Ainsi que synchroniser les mails entre le serveur principal et ses copies.

Il est donc important de comprendre que le serveur principal possède la configuration serveur de rsync et que les deux autres ont une configuration cliente

## Configuration du serveur principal

### Les fichiers de config du serveur principal

Dans le fichier de config principal vous trouverez la config globale du serveur qui ouvre le port et inclut les configs non-globales :

```
[global]
port = 837

&include /etc/rsyncd.conf.d/
```

La première config non-globale est là pour synchroniser les certificats :

```text
[certs]
path = /opt/certs
read only = true
hosts deny = 0.0.0.0/0,::/0
hosts allow =  monitor.ox.numerique.gouv.fr  backup.ox.numerique.gouv.fr
reverse lookup = false
forward lookup = true
list = false
uid = 0
gid = 0
```

La seconde, quant à elle, a pour but de synchroniser les mails entre le serveur principal et ses copies :

```text
[mail]
path = /var/mail
read only = true
hosts deny = 0.0.0.0/0,::/0
hosts allow =  backup.ox.numerique.gouv.fr
reverse lookup = false
forward lookup = true
list = false
uid = 0
gid = 0
```

### La gestion au sein de Ansible

Les configs ci-dessus sont gérées avec Ansible, au travers de trois rôles distincts :

* **La configuration Globale** :
La configuration globale se gère dans le rôle `rsync_serveur` qui s'assure également de l'installation du soft rsync.
* **La configuration certificat** :
La configuration certificats se gère au sein du rôle `cert_manager` qui s'assure également que les certificats sont correctement installés.
* **La configuration mail** :
La configuration mail se gère au sein du rôle `imap_master`.

## Les configurations clientes

La configuration clientes se fait au sein de Ansible pour les serveurs de copies du serveur principal ainsi que pour le serveur de monitoring

### La configuration certificat

La configuration certificat se fait dans les serveurs de copies et dans le serveur de monitoring via un script Ansible

La configuration implique une tâche programmée afin que la synchronisation soit relancée régulièrement : 

```yaml
- name: Periodicaly update the certificates using rsync
  ansible.builtin.cron:
    name: rsync certs
    month: "*"
    day: "*"
    hour: "*"
    minute: 12,42
    job: rsync -av {{ hostvars[groups['cert_manager'].0].ansible_ssh_host }}::certs /opt/certs/
    cron_file: rsync-certs
    user: root
```

Ce qui aura pour résultat : 

```bash
# Ansible: rsync certs
# /etc/cron.d/rsync-certs
12,42 * * * * root rsync -av mail.ox.numerique.gouv.fr::certs /opt/certs/
```

### La configuration mail

La configuration certificat se fait dans les serveurs de copies via un script Ansible

La configuration implique une tâche programmée afin que la synchronisation soit relancée régulièrement : 

```yaml
- name: Periodicaly update the mail storage using rsync
  ansible.builtin.cron:
    name: rsync mail
    month: "*"
    day: "*"
    hour: "*"
    minute: 5,35
    job: rsync -av --delete {{ hostvars[groups['imap_master'].0].ansible_ssh_host }}::mail /var/mail/
    cron_file: rsync-mail
    user: root
```

ce qui aura pour résultat :

```bash
# Ansible: rsync certs
# /etc/cron.d/rsync-certs
5,35 * * * * root rsync -av mail.ox.numerique.gouv.fr::mail /var/mail/
```
