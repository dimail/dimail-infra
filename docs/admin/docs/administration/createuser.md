---
sidebar_label: Création d'un utilisateur
sidebar_position: 1
---

# Création d'un utilisateur

## Usage

Dans `/home/debian`, retrouver le script createuserscript.sh

example : ```./createuserscript.sh -A $supportuserpwd -c $contextid -p $password -u $username -d $displayname" -g $givenname -s $surname -D $domain```

- ```-A``` &rarr; Le mot de passe mysql de l'utilisateur support. (Le user support est utilisé pour effectuer des opérations sur les utilisateurs mail). Le mot de passe est accessible via ansible-vault dans ```40_ansible_single/group_vars/$ENV/passwords```, variable ```support_pwd```.
- ```-c``` &rarr; Dans openxchange, un utilisateur se crée au sein d'un contexte. Un contexte est un espace virtuel dans lequel se trouve des utilisateurs, cet espace peut-être modulé pour autoriser/interdire des fonctionnalités pour tous les utilisateurs en son sein. Un contexte est aussi associé à un/des domaines mail.

Dans notre cas il existe aujourd'hui 3 contexte associés chacun à un id :

```1``` &rarr; le contexte par défault, créé lors de l'installation de l'application

```2``` &rarr; le contexte des utilisateurs data.gouv.fr

```3``` &rarr; le contexte de l'utilisateur système

Nous utilisons le contexte numéro ```2``` pour gérer les utilisateur donc rentrer ```-c 2``` en production.
Nous utilisons le contexte numéro ```1``` en développement

- ```-p``` &rarr; Le mot de passe à fournir à l'utilisateur (fournir un mot de passe sans caractères spéciaux. L'utilitaire ne fonctionne pas pour le moment avec les caractères spéciaux - puis demander à l'utilisateur de changer son mot de passe).
- ```-u``` &rarr; Le username de l'utilisateur, plus précisément la partie avant "@" du mail de l'utilisateur, par exemple ```jean.dupont```.
- ```-d``` &rarr; Le displayname de l'utilisateur, par exemple ```"Jean Dupont```, ne pas oubliez les ```"``` sinon le script considérera 2 arguments. OpenXchange n'accepte pas les homonymes pour le moment, le displayname doit être unique.
- ```-g``` &rarr; Le givenname de l'utilisateur (le prénom), si composé, mettre des ```"```.
- ```-s``` &rarr; Le surname de l'utilisateur (le nom de famille), si composé, mettre des ```"```.
- ```-D``` &rarr; Le domaine associé à l'utilisateur, pour l'instant ```data.gouv.fr``` en production, ```test.ox.numerique.gouv.fr`` en développement.

## Comment ça fonctionne ?

Pour créer un utilisateur, il faut le créer dans plusieurs tables de la base de données MariaDB :

1. La table ```dovecot.users``` qui est la table utilisé par le service IMAP Dovecot
2. Les tables de la base oxdatabase_5, ces tables sont gérés par la commande native d'openXchange ```createuser```

Ainsi lorsque vous créer un user, il faut s'assurer qu'une commande d'insertion dans la base dovecot soit réalisé et que la commande createuser soit bien passée. Le script ```createuserscript.sh``` effectue les vérifications préalables sur les entrées que vous fournissez (l'user existe-t-il déjà dans nos bases ?) puis rajoute l'utilisateur.

Enfin, lorsqu'un utilisateur reçoit son premier mail, sa boite imap est créée : elle se trouve dans ```/var/mail/imap_mailboxes/$DOMAINE/$us/$username``` avec ```$us``` les 2 premières lettres du username.

