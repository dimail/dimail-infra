---
sidebar_label: Rsync utilisation générale
sidebar_position: 6
---

# Documentation sur l'utilisation de rsync en mode serveur/client

## Introduction

Rsync (Remote Sync) est un outil de synchronisation de fichiers qui permet de copier et de synchroniser des fichiers et des répertoires entre deux machines. Cette documentation explique comment utiliser rsync en mode serveur/client pour faciliter la copie sécurisée des données sur un réseau.

## Configuration du Serveur

### Étape 1 : Installation de rsync

Assurez-vous que rsync est installé sur le serveur. Si ce n'est pas le cas, vous pouvez l'installer en utilisant le gestionnaire de paquets de votre système.

```bash
# Exemple avec apt (pour les systèmes basés sur Debian/Ubuntu)
sudo apt-get install rsync
```

### Étape 2 : Création d'un fichier de configuration rsync

Créez un fichier rsyncd.conf dans le répertoire /etc (ou tout autre emplacement approprié) sur le serveur. Ce fichier contiendra les paramètres de configuration pour rsync.

```bash
sudo nano /etc/rsyncd.conf
```

Ajoutez les lignes suivantes en personnalisant selon vos besoins :

```text
uid = nobody
gid = nogroup
use chroot = yes
max connections = 10
pid file = /var/run/rsyncd.pid

[mon_repertoire]
    path = /chemin/vers/le/repertoire
    comment = Exemple de répertoire rsync
    read only = no
    list = yes
    auth users = utilisateur
    secrets file = /etc/rsyncd.secrets
```

### Étape 3 : Création du fichier des secrets

Créez le fichier /etc/rsyncd.secrets pour stocker le mot de passe de l'utilisateur autorisé à accéder au serveur rsync.

```bash
sudo nano /etc/rsyncd.secrets
```

Ajoutez une ligne au format suivant :

```text
utilisateur:mdp
```

Assurez-vous que ce fichier est sécurisé en réduisant ses permissions avec la commande :

```bash
sudo chmod 600 /etc/rsyncd.secrets
```

### Étape 4 : Démarrage du serveur rsync

Démarrez le serveur rsync en exécutant la commande suivante :

```bash
sudo rsync --daemon
```

## Utilisation du Client

### Étape 1 : Installation de rsync

Assurez-vous que rsync est installé sur le client de la même manière que sur le serveur.

### Étape 2 : Utilisation de rsync pour la synchronisation

Utilisez la commande suivante sur le client pour synchroniser les fichiers avec le serveur :

```bash
rsync -avz chemin/source utilisateur@serveur::mon_repertoire/destination
```

- **`avz`**: Options pour la copie récursive avec compression.
- **`chemin/source`**: Chemin local du répertoire source.
- **`utilisateur`**: Nom de l'utilisateur autorisé sur le serveur.
- **`serveur::mon_repertoire`**: Adresse du serveur et le module rsync configuré.
- **`/destination`**: Chemin sur le serveur où les fichiers seront copiés.

Vous serez invité à entrer le mot de passe associé à l'utilisateur sur le serveur.

## Conclusion

En suivant ces étapes, vous devriez être en mesure de configurer et d'utiliser rsync en mode serveur/client pour la synchronisation de fichiers sur votre réseau. N'oubliez pas d'ajuster les configurations selon vos besoins spécifiques.

