---
sidebar_label: Bienvenue sur le projet !
sidebar_position: 1
---

# Bienvenue

Bienvenue sur le projet de messagerie de la DINUM.

Si vous êtes contributeurs commencez par :

1. [Vous procurer vos accès](../access/github.md)
2. [Prendre connaissance des technologies et pratiques du projet](../practices/deploy.md)
3. [Prendre connaissance des pratiques d'administration](../administration/createuser.md)
