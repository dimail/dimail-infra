---
sidebar_position: 3
sidebar_label: Organisation du code
---

# Organisation du code

## Dépôts

Les dépôts sont hébergés sur [GitLab](https://gitlab.mim-libre.fr/dimail/).
Certains dépôts sont également synchronisés sur [GitHub](https://github.com/numerique-gouv/).

Pour favoriser la collaboration et contribuer à l’écosystème open-source, la majeure partie de notre code est publique. Cela inclut principalement les fichiers de configuration Terraform et les rôles Ansible, réutilisable pour déployer la plateforme dans un autre contexte. Ces éléments sont disponibles sur GitLab (plateforme libre) et GitHub (pour augmenter la visibilité et faciliter les contributions de la communauté)

Le dépôt [Dimail Infra Config](https://gitlab.mim-libre.fr/dimail/dimail-infra-config) contient les variables de configuration et les secrets liés à l'infrastructure de mail de la DINUM.
Bien que les valeurs sensibles soient chiffrées, le dépôt est privé afin de minimiser les risques de fuite.

Autres dépôts :

* [Dimail Api](https://gitlab.mim-libre.fr/dimail/dimail-api) : le code Python de l'API
* [dimail-infra](https://gitlab.mim-libre.fr/dimail/dimail-infra) : le modules Terraform et rôle Ansible génériques
* [dimail-login](https://gitlab.mim-libre.fr/dimail/dimail-login) : la landing page Open-Xchange aux couleurs de la DINUM
* [dimail-ui](https://gitlab.mim-libre.fr/dimail/dimail-ui) : les plugins frontend Open-Xchange
* [pondeuse](https://gitlab.mim-libre.fr/dimail/pondeuse) : la suite de tests E2E

## Code d'infrastructure

Le provisionnement de l’infrastructure sur Outscale (réseau, DNS, instances, stockage, …) est fait via Terraform.
La gestion de configuration des machines et le déploiement applicatif (Dovecot, Open-Xchange, Dimail API, Postfix, …) est assuré par Ansible.

Ces outils :

* Rendent possible une automatisation complète des processus, réduisant les erreurs humaines et assurant une meilleure cohérence dans la configuration des environnements;
* Facilitent la gestion des infrastructures complexes grace à leur approche déclarative et modulaire;
* Favorisent une documentation implicite par le code, améliorant la collaboration, et simplifiant les audits;
* Permettent une historisation des changements, assurant une traçabilité des modifications et offrant la possibilité de revenir en arrière.
