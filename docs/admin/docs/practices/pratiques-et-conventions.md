---
sidebar_position: 4
sidebar_label: Pratiques et conventions
---

# Pratiques et conventions

## Gestion de versions

L'usage est de passer par une _merge request_ pour modifier le code. La pertinence d'une demande de revue est laissée à l'appréciation de l'auteur, en fonction de la nature du changement.

Il n'existe pas de convention stricte pour les messages de commit. Il est cependant recommandé, en plus de se conformer aux commits précédents, de suivre les bonnes pratiques habituelles : un message en anglais clair et concis, commençant par un verbe à l'infinitif, et décrivant le _pourquoi_ du changement, pas seulement le _quoi_.

## Board

Le backlog et les tickets en cours sont suivis dans [un projet GitHub](https://github.com/orgs/numerique-gouv/projects/3), dont la mise à jour se fait sur la base de la bonne volonté.

## Documentation

Nous utilisons 2 outils pour créer, éditer et versionner nos diagrammes et autres dessins libres sur git :

* [Mermaid](http://mermaid.js.org/) pour les diagrammes dans un format supporté, en particulier les diagrammes séquence;
* [Excalidraw](https://excalidraw.com/) et son option de sauvegarde en `.png` avec sources intégrées (`Embed Source`) pour les diagrammes plus complexes et dessins libres.

Cette combinaison d'outils nous permet :

* De stocker les diagrammes dans notre dépôt de code, garantissant leur cohérence avec le reste de la documentation;
* D'éviter la dualité code source/image exportée grace à un fichier unique par diagramme, garantissant la cohérence et facilitant les modifications ultérieures;
* De couvrir la plupart des cas d'usage avec deux outils flexibles : un orienté diagram as code et l'autre dessin libre;
* De minimiser le temps de montée en compétence de l'équipe en utilisant des outils légers, intuitifs, et bien documentés;
* D'afficher les diagrammes dans GitLab (nativement) et dans Docusaurus (avec un plugin);
* De créer et d'éditer des diagrammes hors-ligne (en installant Excalidraw comme PWA) ou bien directement dans un éditeur au moyen de plugins, par exemple [markdown-mermaid](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid) et [excalidraw-editor](https://marketplace.visualstudio.com/items?itemName=pomdtr.excalidraw-editor) pour VSCode;

## Rituels

* Un Forum a lieu une fois par mois, sur toute une journée.
* Les responsables des différents projets MCE se réunissent le Mardi à 14h.
