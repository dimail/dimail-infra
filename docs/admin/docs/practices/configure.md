---
sidebar_position: 2
sidebar_label: Configurer les machines
---

# Configurer les Machines

La configuration des services au sein des machines se fait de manière automatisée via Ansible. Pour en savoir plus sur ce qu'exécute précisément Ansible sur quelles machines, rendez-vous dans la rubrique Système de fichier > Ansible.

Ici nous reviendrons seulement sur les commandes utilisées dans ce projet.

## Avant toute commande

Positionnez-vous dans ```40_ansible_single```

```bash
cd 40_ansible_single
```

Vérifiez bien que vous avez chargé les variables d'environnements adéquates.

Pour l'environnement de développement :

```bash
. ../.env-dev
```

Pour l'environnement de production :

```bash
. ../.env-prod
```


## Visualiser l'inventaire

Cette commande vous permettra de retrouver les machines auxquelles Ansible se connecte en SSH pour effectuer ses opérations.
Elle nous est utile pour identifier les propriétés et ressources associées aux machines telles que les tags ou les volumes. Ces données sont ensuite utilisées à travers les rôles Ansible.

```bash
ansible-inventory -i openstack.yml --list
```

## Tester un playbook

Cette commande nous permet de lancer une simulation de la configuration sur un serveur. On l'utilise généralement en production. En développement, on a plutôt tendance à directement lancer le playbook.

```bash
ansible-playbook playbook.yml --diff --check
```

```--check``` fait en sorte que le lancement soit une simulation.
```--diff```  renvoie en sortie standard le delta des modifications simulées.

## Lancer un playbook

Cette commande lance le playbook qui contient tous les rôles.

```bash
ansible-playbook playbook.yml
```

L'environnement dans lequel est lancé le playbook dépend de ce que vous avez sourcé plus tôt.

## Options utiles

Si vous souhaitez effectuer une simulation ou lancer le playbook sur seulement des rôles spécifiques, vous pouvez le paramétrer en rajoutant ```--tags="tag1,tag2"```. dans ce cas le playbook n'effectuera que les opérations associées aux rôles ayant les tags tag1 et/ou tag2 (Union).

