---
sidebar_position: 1
sidebar_label: Manipuler l'infrastructure
---

# Manipuler l'infrastructure

Nous utilisons Terraform pour manipuler notre infrastructure. Pour faciliter les changements d'environnements, nous nous appuyons sur un ```Makefile``` afin de ne pas avoir à renseigner systématiquement le backend à utiliser, les fichiers de variables d'environnement se trouvant dans les dossiers ```env-dev``` et ```env-prod```.

```Makefile
init: check
	terraform -chdir=./10_backend_ovh init -backend-config=../env-$(ENV)/backend.tfbackend -reconfigure
	terraform -chdir=./20_network init -backend-config=../env-$(ENV)/backend.tfbackend -reconfigure
	terraform -chdir=./30_vms init -backend-config=../env-$(ENV)/backend.tfbackend -reconfigure

plan:
	terraform -chdir=./10_backend_ovh plan -var-file=../env-$(ENV)/10_backend.tfvars
	terraform -chdir=./20_network plan -var-file=../env-$(ENV)/20_network.tfvars
	terraform -chdir=./30_vms plan -var-file=../env-$(ENV)/30_vms.tfvars

apply_backend: check
	terraform -chdir=./10_backend_ovh apply -var-file=../env-$(ENV)/10_backend.tfvars

apply_network: check
	terraform -chdir=./20_network apply -var-file=../env-$(ENV)/20_network.tfvars

apply_vms: check
	terraform -chdir=./30_vms apply -var-file=../env-$(ENV)/30_vms.tfvars

destroy: check
	terraform -chdir=./30_vms destroy -var-file=../env-$(ENV)/30_vms.tfvars

check:
	@if [ "$$ENV" != "prod" -a "$$ENV" != "dev" ]; then echo Tu te moques ENV=$$ENV n a aucun sens.; exit 1; fi
	@echo Environnement : $$ENV.
```

## Initialiser l'environnement Terraform

### Se positionner dans le bon environnement

Pour se positionner dans l'environnement de développement :
Lancer ```. .\$FICHIER_ENV_DEV``` Puis ```Make init``` pour initialiser le backend terraform de développement.

Pour se positionner dans l'environnement de production :
Lancer ```. .\$FICHIER_ENV_PROD``` Puis ```Make init``` pour initialiser le backend terraform de production.

> :warning: **Si vous ne savez pas dans quel environnement vous vous trouvez, lancez ```Make check```**

## Déployer le backend

Avant tout de chose il est préférable avant de directement déployer de lancer un ```plan``` de l'infrastructure.
pour cela, Lancer ```Make plan```.

> :warning: **Vous n'aurez probablement pas à le faire sauf cas d'exception, mais il est bon de savoir le faire**

Les ressources du backend se trouvent dans le dossier ```10_backend```.


Lancer ```Make apply_backend```.\
Puis au prompt de validation, répondez ```yes```.

## Déployer le réseau

Les ressources du réseau se trouvent dans le dossier ```20_network```.

Lancer ```Make apply_network```.\
Puis au prompt de validation, répondez ```yes```.

## Déployer les machines

Les ressources machines se trouvent dans le dossier ```30_vms```.

Lancer ```Make apply_vms```.\
Puis au prompt de validation, répondez ```yes```.

# Détruire l'infrastructure

Lancer ```Make destroy```.\
Puis au prompt de validation, répondez ```yes```.

> :warning: **Cette commande ne détruit que la partie machines, pas la partie backend ou network.**\
>  **ATTENTION A NE PAS LANCER CETTE COMMANDE EN PRODUCTION**
