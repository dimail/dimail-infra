variable "env_name" {
  description = "Name of the current environment"
  type        = string
}

variable "env_type" {
  description = "Type of the current environment (dev/prod)"
  type        = string
}

variable "main_region" {
  description = "Region of the project (main servers)"
  type        = string
}

variable "backup_region" {
  description = "Region of the project (backup servers)"
  type        = string
}

variable "additional_regions" {
  description = "Additional regions for the project"
  type        = set(string)
  default     = []
}

variable "ovh_openstack_tenant_name" {
  description = "Name of the project"
  type        = string
}

variable "ovh_openstack_tenant_id" {
  description = "ID of the project"
  type        = string
}

variable "technical_domain" {
  description = "Name of the technical domain"
  type        = string
}

variable "host_domain" {
  description = "Name of the host domain"
  type        = string
}

variable "ttl" {
  description = "Default TTL for DNS records"
  type        = number
}

# variable "client_domains" {
#   description = "Name of the test client domains"
#   type        = set(string)
#   default     = []
# }
# 
variable "google_postmaster_tool_verification_codes" {
  description = "Verification records for Google Postmaster Tools"
  type        = map(string)
  default     = {}
}

# variable "delegated_domains" {
#   description = "Name of the delegated domains"
#   type        = map(set(string))
#   default     = {}
# }
# 
variable "private_networks" {
  description = "List of private networks to create"
  type = map(object({
    vrack_vlan_id = number
    cidr          = string
    new_bits      = number
    dns_servers   = set(string)
  }))
  default = {}
}

variable "config_path" {
  description = "Secrets/config repository base path"
  type = string
}
