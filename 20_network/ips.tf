# C'est l'IP principale dans l'architecture single-server
module "main_ip" {
  source = "./modules/floating_ip_with_reverse"

  region    = var.main_region
  zone      = var.technical_domain
  subdomain = "smtp"
  ttl       = var.ttl
  tags      = ["role-main_server", "archi-mecol", var.env_type]
}

# L'IP de la VM de backup
module "backup_ip" {
  source = "./modules/floating_ip_with_reverse"

  region    = var.backup_region
  zone      = var.technical_domain
  subdomain = "backup"
  ttl       = var.ttl
  tags      = ["role-backup_server", "archi-mecol", var.env_type]
}

# L'IP de la VM de supervision
module "monitor_ip" {
  source = "./modules/floating_ip_with_reverse"

  region    = var.main_region
  zone      = var.technical_domain
  subdomain = "monitor"
  ttl       = var.ttl
  tags      = ["role-monitor", "archi-mecol", var.env_type]
}

# L'IP de la VM de supervision
module "ldap_ip" {
  source = "./modules/floating_ip_with_reverse"

  region    = var.main_region
  zone      = var.technical_domain
  subdomain = "ldap"
  ttl       = var.ttl
  tags      = ["role-ldap", "archi-mecol", var.env_type]
}

## ATTENTION MEP :
# les records DNS ne peuvent pas changés de zones donc il faut provoquer leur destruction de cette façon :
# terraform -chdir=./20_network apply -var-file=../env-prod/20_network.tfvars -replace=module.backup_ip.ovh_domain_zone_record.dns_record -replace=module.main_ip.ovh_domain_zone_record.dns_record -replace=module.monitor_ip.ovh_domain_zone_record.dns_record
