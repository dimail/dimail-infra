variable "ovh_public_cloud_project_id" {
  description = "ID of the OVH Public Cloud project"
  type        = string
}

variable "name" {
  description = "Name of the network"
  type        = string
}

variable "regions" {
  description = "Regions where to deploy the network"
  type        = set(string)
}

variable "vlan_id" {
  description = "VLAN ID of the network inside vRack"
  type        = string
}

variable "cidr" {
  description = "Global CIDR of the network"
  type        = string
}

variable "newbits" {
  description = "Number of bits to add to the CIDR for subnets"
  type        = number
  default     = 8
}

variable "dns_nameservers" {
  description = "DNS servers for the network"
  type        = set(string)
  default     = []
}

variable "enable_dhcp" {
  description = "Enable DHCP for the network"
  type        = bool
  default     = true
}

