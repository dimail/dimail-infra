terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }

    ovh = {
      source  = "ovh/ovh"
      version = "~> 0.51.0"
    }
  }

  required_version = ">= 1.5.3"
}
