# ovh_cloud_project_network_private (deployed on 15-02-2024)

moved {
  from = ovh_cloud_project_network_private.vpc
  to   = ovh_cloud_project_network_private.multi-region_network
}

# openstack_networking_network_v2 (deployed on 15-02-2024)

moved {
  from = data.openstack_networking_network_v2.vpc["sbg5"]
  to   = data.openstack_networking_network_v2.multi-region_network["sbg5"]
}

moved {
  from = data.openstack_networking_network_v2.vpc["sbg7"]
  to   = data.openstack_networking_network_v2.multi-region_network["sbg7"]
}

moved {
  from = data.openstack_networking_network_v2.vpc["gra7"]
  to   = data.openstack_networking_network_v2.multi-region_network["gra7"]
}

moved {
  from = data.openstack_networking_network_v2.vpc["gra9"]
  to   = data.openstack_networking_network_v2.multi-region_network["gra9"]
}

moved {
  from = data.openstack_networking_network_v2.vpc["rbx-a"]
  to   = data.openstack_networking_network_v2.multi-region_network["rbx-a"]
}

# openstack_networking_subnet_v2 (deployed on 15-02-2024)

moved {
  from = openstack_networking_subnet_v2.vpc_subnet["sbg5"]
  to   = openstack_networking_subnet_v2.multi-region_subnet["sbg5"]
}

moved {
  from = openstack_networking_subnet_v2.vpc_subnet["sbg7"]
  to   = openstack_networking_subnet_v2.multi-region_subnet["sbg7"]
}

moved {
  from = openstack_networking_subnet_v2.vpc_subnet["gra7"]
  to   = openstack_networking_subnet_v2.multi-region_subnet["gra7"]
}

moved {
  from = openstack_networking_subnet_v2.vpc_subnet["gra9"]
  to   = openstack_networking_subnet_v2.multi-region_subnet["gra9"]
}

moved {
  from = openstack_networking_subnet_v2.vpc_subnet["rbx-a"]
  to   = openstack_networking_subnet_v2.multi-region_subnet["rbx-a"]
}
