resource "ovh_cloud_project_network_private" "multi-region_network" {
  service_name = var.ovh_public_cloud_project_id

  name = var.name

  regions = [for region in var.regions : upper(region)]
  vlan_id = var.vlan_id
}
