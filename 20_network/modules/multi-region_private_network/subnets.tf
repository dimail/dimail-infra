data "openstack_networking_network_v2" "multi-region_network" {
  for_each = var.regions

  name   = ovh_cloud_project_network_private.multi-region_network.name
  region = upper(each.value)

  depends_on = [ovh_cloud_project_network_private.multi-region_network]
}

resource "openstack_networking_subnet_v2" "multi-region_subnet" {
  for_each = local.subnets_by_region

  name       = "${var.name}-${each.key}-subnet"
  network_id = data.openstack_networking_network_v2.multi-region_network[each.key].id
  region     = upper(each.key)

  ip_version      = 4
  dns_nameservers = var.dns_nameservers
  enable_dhcp     = var.enable_dhcp
  no_gateway      = true # You cannot have a gateway on a multi-region private network

  cidr = var.cidr # use the global to allow inter-region communication
  allocation_pool {
    start = cidrhost(each.value, 20) # 0 = network, 1 = gateway, and some more
    end   = cidrhost(each.value, -5) # -1 = broadcast
  }

  depends_on = [ovh_cloud_project_network_private.multi-region_network]
}

################################################
# Module for generating subnets for each region
################################################

module "subnet_addrs" {
  source = "hashicorp/subnets/cidr"

  base_cidr_block = var.cidr
  networks = [
    for region in var.regions : {
      name     = "${lower(region)}"
      new_bits = var.newbits
    }
  ]
}

locals {
  subnets_by_region = module.subnet_addrs.network_cidr_blocks
}
