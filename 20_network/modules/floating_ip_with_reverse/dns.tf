data "ovh_domain_zone" "dns_zone" {
  name = var.zone
}

resource "ovh_domain_zone_record" "dns_record" {
  zone      = data.ovh_domain_zone.dns_zone.name
  subdomain = var.subdomain
  fieldtype = "A"
  ttl       = var.ttl
  target    = openstack_networking_floatingip_v2.ip.address
}
