data "openstack_networking_network_v2" "ip_pool_network" {
  name   = var.ip_pool_network_name
  region = var.region
}

resource "openstack_networking_floatingip_v2" "ip" {
  pool   = data.openstack_networking_network_v2.ip_pool_network.name
  tags   = var.tags
  region = var.region

  lifecycle {
    prevent_destroy = true
  }
}
