variable "region" {
  description = "Region of the project (main servers)"
  type        = string
}

variable "zone" {
  description = "DNS Zone of the project"
  type        = string
}

variable "subdomain" {
  description = "Name of the technical domain"
  type        = string
}

variable "ttl" {
  description = "TTL of the DNS record"
  type        = number
  default     = 1800
}

variable "ip_pool_network_name" {
  description = "Name of the IP pool network"
  type        = string
  default     = "Ext-Net"
}

variable "tags" {
  description = "Tags of the DNS record"
  type        = set(string)
  default     = []
}
