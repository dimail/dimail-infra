locals {
  ip_fqdn = "${var.subdomain}.${var.zone}"
}

resource "ovh_ip_reverse" "reverse_ip" {
  ip         = "${openstack_networking_floatingip_v2.ip.address}/32"
  ip_reverse = openstack_networking_floatingip_v2.ip.address
  reverse    = "${local.ip_fqdn}."

  depends_on = [ovh_domain_zone_record.dns_record]
}
