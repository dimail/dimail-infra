output "ip" {
  description = "The floating IP"
  value       = openstack_networking_floatingip_v2.ip.address
}

output "reverse" {
  description = "Reverse DNS of the floating IP"
  # remove final dot
  value = replace(ovh_ip_reverse.reverse_ip.reverse, "\\.$", "")
}
