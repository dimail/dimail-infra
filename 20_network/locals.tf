locals {
  all_regions = setunion(
    [lower(var.main_region), lower(var.backup_region)],
    [for region in var.additional_regions : lower(region)]
  )
}
