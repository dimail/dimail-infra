terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }

    ovh = {
      source  = "ovh/ovh"
      version = "~> 0.51.0"
    }
  }

  backend "s3" {
    key = "20_network/terraform.tfstate"
  }
}


# Configure le fournisseur OpenStack hébergé par OVHcloud
provider "openstack" {
  auth_url    = "https://auth.cloud.ovh.net/v3/" # URL d'authentification
  domain_name = "default"                        # Nom de domaine - Toujours à "default" pour OVHcloud
  # alias       = "ovh"                            # Un alias
  region = var.main_region # Région du projet

  tenant_name = var.ovh_openstack_tenant_name # Nom du projet
  tenant_id   = var.ovh_openstack_tenant_id   # ID du projet

  # Il faut configurer les variables d'environnement suivantes :
  # OS_USERNAME=
  # OS_PASSWORD=
}

provider "ovh" {
  endpoint = "ovh-eu"
}
