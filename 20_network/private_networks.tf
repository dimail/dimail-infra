module "private_network" {
  source = "./modules/multi-region_private_network"

  for_each = var.private_networks

  name                        = each.key
  regions                     = local.all_regions
  ovh_public_cloud_project_id = var.ovh_openstack_tenant_id

  vlan_id         = each.value.vrack_vlan_id
  cidr            = each.value.cidr
  newbits         = each.value.new_bits
  dns_nameservers = each.value.dns_servers
}
