data "ovh_domain_zone" "technical" {
  name = var.technical_domain
}

data "ovh_domain_zone" "host" {
  name = var.host_domain
}

