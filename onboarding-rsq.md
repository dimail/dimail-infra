# Utilisation de ce repo 

Ce repo sert à provisionner une infrastructure + une suite logicielle pour héberger/transmettre/recevoir des mails.

## Infrastructure

A compléter quand on aura des cas d'usage 

## Suite logicielle

La suite logicielle comporte 
- un serveur IMAP `Dovecot`
- un serveur SMTP `Postfix`
- un webmail `Open-Xchange` apportant des solutions de calendrier et d'agenda partagé
- un système de backup
- un système de supervision `Nagios`
- une API permettant de provisionner des boîtes email
- une application `login`

Ces serveurs/applications seront installés sur les machines possédant le rôle adéquat :  
_exemple 1 : `Dovecot` sera installé sur la machine ayant le rôle `IMAP`_
_exemple 2 : sur une machine ayant les rôles `imap` et `webmail`,`Dovecot` et `Open-Xchange` seront installés_

### Authentification

Actuellement le webmail se connecte à ses boîtes via l'IMAP. Des travaux sont en cours pour gérer l'authentification OIDC.


## Les notions
- __un groupe de machine__ : Un groupe de machine correspond à un rôle.
- __une feature__ : 
- __un mode de transport (`delivery`)__: Un mode de transport est la façon dont les mails vont transiter entre les machines 
  - exemple : `virtual`, `relay`
- __un domaine technique__ : caractérisé dans le code par la variable `tech_domain`, il s'agit du nom de domaine 
  vers lequel sera redirigé un ou plusieurs noms de domaine client héberger plusieurs __*domaines clients*__
  - `tech_domain = "new.ox.numerique.gouv.fr"`
- __un domaine client__ : caractérisé dans le code par la variable `host_domain`, il s'agit du nom de domaine vers 
  lequel seront redirigés un ou plusieurs noms de domaines clients (enregistrement DNS `CNAME`)
  - `host_domain = "osprod.dimail1.numerique.gouv.fr"`



### Les rôles 

Les rôles sont assignés aux machines via les inventaires Ansible

#### Liste des rôles
- `bastion` : la machine de rebond qui permettra de se connecter en `ssh` à la plateforme
- `cert_manager` : la machine qui crée les certificats
- `cert_user` : la/les machine/s qui utilise les certificats
- `imap_master` : la machine qui gère les boîtes mails
- `imap_slave` : la machine qui peut proxyfier les boîtes mails de `imap_master`
- `monitor` : la machine de supervision
- `ox` : 
- `ox_imap` : 
- `ox_oidc` : 
- `smtp`: la machine qui configure postfix
- sql_master
- sql_slave

Les rôles peuvent être liés et l'un ne fonctionne pas sans l'autre 
- _exemple 1 :  `ox_imap` et `cert_user`, pour fonctionner correctement le webmail a besoin d'un reverse-proxy et des bons certificats_
- _exemple 2 :  `ox_imap` devrait normalement avoir aussi le rôle `ox`_

## Inventaire Ansible

L'inventaire utilisé par Ansible est défini par la variable `ANSIBLE_INVENTORY` fixée dans le fichier `.env.common`.
Actuellement nous utilisons des inventaires dynamiques construits à partir des données de nos fournisseurs IAAS.

### inventaire Outscale 

L'inventaire Outscale est mouliné par le script Python `inventory_outscale/__main__.py` pour calculer certains rôles
non écrits dans les tags. 
- _exemple : si une machine est dans le groupe `ox_imap` alors on l'ajoute au groupe `ox`_

Pour voir le résultat : `inventory_outscale/__main__.py | jq`

### inventaire OVH 

L'inventaire OVH est généré grâce au fichier `openstack.yml` et les rôles sont mappés depuis les tags Terraform
- _exemple_ : si une machine a le tag `role-sqlmaster` ou `role-sqlslave`, elle est ajoutée au groupe `mysql_server`

__ATTENTION actuellement le rôle `ox_oidc` n'existe pas depuis un inventaire OVH

## Les variables Ansible

Pour faire fonctionner les rôles correctement un ensemble de variables et de secrets.
Ces variables sont définies dans le répertoire `${DIMAIL_CONFIG_PATH}/ansible/group_vars`

Les secrets sont chi