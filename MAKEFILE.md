# Documentation d'onboarding pour Makefile

## Vue d'ensemble
Ce Makefile est conçu pour faciliter les opérations Terraform. Il utilise une directive `include` pour être modulaire et facilement extensible. Cela permet aux utilisateurs d'ajouter de nouveaux processus en ajoutant simplement leurs fichiers Makefile dans le répertoire `makefiles`.

## Structure
- **Makefile Principal**: Le Makefile principal qui inclut d'autres Makefiles du répertoire `makefiles`.
- **Répertoire makefiles**: Contient des Makefiles individuels pour cibler des providers pour l'infrastructure.

## Comment ajouter un nouveau processus
1. **Créer un nouveau Makefile**:
	- Naviguez vers le répertoire `makefiles`.
	- Créez un nouveau Makefile pour votre processus. Par exemple, `dimailhosting.mk`.
		- Créez à l'intérieur de celui-ci les commandes :
			- `init_<dimailhosting>`
			- `plan_<dimailhosting>`
			- `apply_<dimailhosting>`
			- `beauty_<dimailhosting>`
			- `destroy_<dimailhosting>`
		
		À l'intérieur de ces différentes tâches, donnez à Terraform les moyens de réaliser la tâche indiquée.


2. **Définir votre processus**:
	- Dans votre nouveau Makefile, définissez les cibles et les commandes nécessaires pour votre processus.
	- Exemple:
	  ```mk
	  new_process:
			@echo "Exécution du nouveau processus"
			# Ajoutez vos commandes ici
	  ```

3. **Inclure votre Makefile**:

	Nommer votre Makefile dans le dossier au nom de la valeur de votre `dimailhosting` ainsi ce dernier sera automatiquement inclus dans le `Makefile` principal

4. **Exécuter votre processus**:
	- Vous pouvez maintenant exécuter votre nouveau processus en utilisant la commande `make`.
	- Exemple:
	  ```sh
	  make new_process
	  ```

## Meilleures pratiques
- **Modularité**: Gardez chaque `dimail_hosting` dans un `Makefile` séparé pour maintenir la modularité.
- **Nommage**: Utilisez des noms descriptifs pour vos Makefiles et cibles afin d'éviter les conflits et d'améliorer la lisibilité.
- **Documentation**: Documentez chaque Makefile et ses cibles pour aider les autres utilisateurs à comprendre son objectif et son utilisation.

En suivant ces étapes, vous pouvez facilement étendre les fonctionnalités du Makefile et intégrer de nouveaux processus sans difficulté.