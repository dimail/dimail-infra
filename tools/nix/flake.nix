{
  description = "dimail-infra tooling";

  inputs = {
    nixpkgs-2405.url = "https://nixos.org/channels/nixos-24.05/nixexprs.tar.xz";
  };

  outputs = { self, nixpkgs-2405, nixpkgs }:
  let
    pkgs = import nixpkgs { system = "x86_64-linux"; config.allowUnfree = true; };
    dev = ( import ./dev.nix { inherit pkgs; });
    ansible = ( import ./ansible.nix { inherit pkgs; });
    terraform = ( import ./terraform.nix { inherit pkgs; });
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      nativeBuildInputs = dev ++ ansible ++ terraform;
      shellHook = ''
        export ANSIBLE_DISPLAY_OK_HOSTS=yes
        export PS1="\n\[\033[1;36m\][\[\e]0;\u@\h: \w\a\]\u@\h:\w]\$\[\033[0m\] "
      '';
    };
    ansible = pkgs.mkShell { nativeBuildInputs = ansible; };
  };
}
