{ pkgs }:
[
  pkgs.ansible
  pkgs.ansible-lint
  (pkgs.python3.withPackages (python-pkgs: [
    python-pkgs.openstacksdk
    python-pkgs.passlib
    python-pkgs.python-ldap
    python-pkgs.boto3
    python-pkgs.configparser
    python-pkgs.pyhcl
    python-pkgs.pytest
  ]))
]
