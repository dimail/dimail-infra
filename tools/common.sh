ssh_target_for_pf() {
    pf=$1
    if [ "$2" != "" ]; then
      host=$2
    else
      host=main
    fi

    case "$pf" in
        ovhprod)
            echo ${host}.ovhprod.dimail1.numerique.gouv.fr
            ;;
        ovhdev)
            echo ${host}.ovhdev.dimail1.numerique.gouv.fr
            ;;
        osdev2)
            echo ${host}.priv.osdev2.dimail1.numerique.gouv.fr
            ;;
	osdev)
	    echo ${host}.priv.osdev.dimail1.numerique.gouv.fr
	    ;;
        osprod)
            echo mail01.priv.osprod.dimail1.numerique.gouv.fr
            ;;
        *)
            echo "JE NE CONNAIS PAS CE '$pf' DONT TU PARLES" 1>&2
            exit 1
            ;;
    esac
}

mariadb_rootpwd_for_pf() {
  pf=$1
  case "$pf" in
    *)
	echo "JE NE CONNAIS PAS CE '$pf' DONT TU PARLES" 1>&2
	exit 1
	;;
  esac
}

RST="\033[0;0m"
BOLD="\033[1m"
VERT="\033[32m"
ROUGE="\033[31m"
JAUNE="\033[33m"
BLEU="\033[34m"
CYAN="\033[36m"

DEBUG=no

title() {
	MSG="$*"
	FIL=`echo $MSG | sed -e 's/./*/g'`
	MSG="$VERT$MSG$RST$BOLD"
	echo -e $BOLD"*****$FIL*****"$RST
	echo -e $BOLD"**** $MSG ****"$RST
	echo -e $BOLD"*****$FIL*****"$RST
}

step() {
	MSG="$*"
	echo -e $CYAN"** $MSG"$RST
}

ok() {
	MSG="$*"
	echo -e $VERT$MSG$RST
}

ko() {
	MSG="$*"
	echo -e $ROUGE"⚠   $MSG"$RST
}

note() {
    MSG="$*"
    echo -e $JAUNE"🛈   $MSG"$RST
}

debug() {
	MSG="$*"
	if [ "$DEBUG" == "yes" ]; then
		echo "  $MSG"
	fi
}

function log() {
    local message="${1}"
    local caller_file="${BASH_SOURCE[1]}"
    local caller_line="${BASH_LINENO[0]}"
    echo -e "${VERT}[$(date '+%Y-%m-%d %H:%M:%S')] $caller_file:$caller_line - $message${RST}"
#    printf '%s\n' "${SCRIPT_NAME} - $(date +%F_%T.%N) : $*"
}

function fatal() {
    local message="${1}"
    local caller_file="${BASH_SOURCE[1]}"
    local caller_line="${BASH_LINENO[0]}"
    echo -e "${ROUGE}[$(date '+%Y-%m-%d %H:%M:%S')] $caller_file:$caller_line - ⚠ $message${RST}"
    exit 1
#    printf '%s\n' "${SCRIPT_NAME} - $(date +%F_%T.%N) : $*"
}
