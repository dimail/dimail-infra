set -e

source ./common.sh

FROM=$1
TO=$2

if [ $# -lt 2 ]; then
  echo usage: move_db.sh pfsource:dbsource pfdest:dbdest
  exit 1
fi

ssh_target_for_location() {
  pf=$(pf_for_location $1)
  ssh_target_for_pf $pf
} 

pf_for_location () {
  location=$1
  echo ${location%:*}
}

db_for_location () {
  location=$1
  echo ${location##*:}
}


if [[ $FROM == *":"* && $TO == *":"* ]]; then
  pf_from=$(pf_for_location $FROM)
  pf_to=$(pf_for_location $TO)
  host_from=$(ssh_target_for_location $FROM)
  host_to=$(ssh_target_for_location $TO)
  db_from=$(db_for_location $FROM)
  db_to=$(db_for_location $TO)
else
  echo usage: move_db.sh pfsource:dbsource pfdest:dbdest
  echo erreur: un paramètre ne contient pas \':\'
  exit 2
fi

db_pwd_from=$(mariadb_rootpwd_for_pf $pf_from)
db_pwd_to=$(mariadb_rootpwd_for_pf $pf_to)

if [ -f /tmp/dump.$$ ]; then
	echo Le fichier /tmp/dump.$$ existe deja. Je ne joue pas.
	exit 1
fi

echo Je prends la base $db_from sur $host_from 
ssh $host_from mariadb-dump -u root -p\'"$db_pwd_from"\' "$db_from" > /tmp/dump.$$
ls -l /tmp/dump.$$

echo je drop et recrée la base $db_to sur $host_to
echo "DROP DATABASE if exists $db_to; CREATE DATABASE $db_to" | ssh $host_to mariadb -u root -p"$db_pwd_to"
ssh -T $host_to mariadb -u root -p"$db_pwd_to" $db_to < /tmp/dump.$$

