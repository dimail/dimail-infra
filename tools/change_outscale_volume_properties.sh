#! /usr/bin/env bash


# don't hide errors within pipes
set -o pipefail
set -o errexit

source common.sh

declare SCRIPT_NAME="${0}"

declare -a USAGES=(
  "${SCRIPT_NAME} <nom_du_volume_donné_par_le_tag_terraform> <taille_en_Go> <type_souhaité_du_volume> <nombre_souhaité_d_iops_pour_le_volume>"
  "${SCRIPT_NAME} volume_datapidb_preprod 5000 io1 3000"
  "${SCRIPT_NAME} volume_datapidb_preprod 2000 gp2"
  "${SCRIPT_NAME} volume_datapidb_preprod 1759 standard"
  )

cleanup() {
  log ""
  if [ -z "${VM_ID}" ]; then
      log "Pas de VM à redémarrer"
      exit 0;
  fi
  start_vm "${osc_config_file}" "${VM_ID}"
}

trap 'cleanup' EXIT
trap 'echo "Une erreur ou une interruption a été détectée, il faut redémarrer la vm ${VM_ID} !"; exit 1' SIGINT SIGTERM


function main() {
    # Vérifier si jq est installé
  if ! jq --version >/dev/null 2>&1; then
      fatal "jq n'est pas installé. Veuillez installer jq avant d'exécuter ce script."
  fi
  # Vérifier si osc-cli est installé
  if ! osc-cli --version >/dev/null 2>&1; then
      fatal "osc-cli n'est pas installé. Veuillez installer osc-cli avant d'exécuter ce script."
  fi
   # Vérifier si osc-cli est installé
   if ! osc-cli --version >/dev/null 2>&1; then
       fatal "osc-cli n'est pas installé. Veuillez installer osc-cli avant d'exécuter ce script."
   fi

  osc_config_file=$(configure_osccli)

  if [ "$#" == 4 ] && [ "${3}" = "io1" ]; then
    echo ""
  elif  [ "$#" == 3 ] && { [ "${3}" = "standard" ] || [ "${3}" = "gp2" ]; }; then
    echo ""
  else
    message="mauvais usage de ${SCRIPT_NAME}, exemples ci-dessous :"
    for usage in "${USAGES[@]}"; do
      message="${message}\n${usage}"
    done
    fatal "${message}"
  fi

  ARG_VOLUME_NAME=${1}
  ARG_VOLUME_SIZE=${2}
  ARG_VOLUME_TYPE=${3}
  ARG_VOLUME_IOPS=${4}

  log "fichier de configuration          : ${osc_config_file}"
  log "région concernée                  : ${OUTSCALE_REGION}"
  log "nom du volume concerné            : ${ARG_VOLUME_NAME}"
  log "taille souhaitée du volume        : ${ARG_VOLUME_SIZE}"
  log "type souhaité du volume           : ${ARG_VOLUME_TYPE}"
  log "nombre souhaité d'IOPS du volume  : ${ARG_VOLUME_IOPS}"

  VOLUME_INFOS=$(fetch_volumes_infos "${osc_config_file}" "${ARG_VOLUME_NAME}")

  # vérifie qu'il y a bien quelque chose à faire
  VOLUME_SIZE=$(echo  "${VOLUME_INFOS}" | jq --raw-output '.Size')
  VOLUME_TYPE=$(echo  "${VOLUME_INFOS}" | jq --raw-output '.VolumeType')
  VOLUME_IOPS=$(echo  "${VOLUME_INFOS}" | jq --raw-output '.Iops')
  VOLUME_ID=$(echo  "${VOLUME_INFOS}" | jq --raw-output '.VolumeId')

  if [ "${ARG_VOLUME_SIZE}" -lt "${VOLUME_SIZE}" ]; then
      log "Le volume a une taille actuelle supérieure à celle souhaitée. Ceci nécessite de supprimer le disque. On refuse. FIN"
      exit 0;
  elif [ "${ARG_VOLUME_TYPE}" = "io1" ] \
      && [ "${ARG_VOLUME_SIZE}" = "${VOLUME_SIZE}" ] \
      && [ "${ARG_VOLUME_TYPE}" = "${VOLUME_TYPE}" ] \
      && [ "${ARG_VOLUME_IOPS}" == "${VOLUME_IOPS}" ]; then
    log "Le volume est dans l'état attendu. Il n'y a rien à faire. Fin"
    exit 0;
  elif [ "${ARG_VOLUME_TYPE}" != "io1" ] \
      && [ "${ARG_VOLUME_SIZE}" = "${VOLUME_SIZE}" ] \
      && [ "${ARG_VOLUME_TYPE}" == "${VOLUME_TYPE}" ]; then
      log "le volume est dans l'état attendu. Il n'y a rien à faire. Fin"
      exit 0;
  fi

  # affiche quelques infos
  log "infos actuelles sur le volume :"
  echo "${VOLUME_INFOS}"
  VM_ID=$(echo "${VOLUME_INFOS}" | jq --raw-output '.LinkedVmIds')
  VM_INFOS=$(fetch_vm_infos "${osc_config_file}" "${VM_ID}")
  log "infos sur la vm : ${VM_ID}"
  echo "${VM_INFOS}"

  # les opérations
  stop_vm "${osc_config_file}" "${VM_ID}"

  modify_volume "${osc_config_file}" "${ARG_VOLUME_NAME}" "${ARG_VOLUME_TYPE}" "${ARG_VOLUME_IOPS}" "${ARG_VOLUME_SIZE}"

  start_vm "${osc_config_file}" "${VM_ID}"

  sync_tf_state "${VOLUME_ID}" "${ARG_VOLUME_SIZE}" "${ARG_VOLUME_TYPE}" "${ARG_VOLUME_IOPS}"

  rm "${osc_config_file}"
}

function fetch_volumes_infos() {
  osc-cli api ReadVolumes  --config_path="${1}" \
      --Filters '{"Tags": ["Name='"${2}"'", "env='"${DIMAIL_ENV}"'"]}' \
    | jq '.Volumes[] | {Name: .Tags[] | select(.Key == "Name") | .Value, Env: .Tags[] | select(.Key == "env") | .Value, VolumeId, VolumeType, Iops, Size, LinkedVmIds: .LinkedVolumes[].VmId }'
}

function fetch_vm_infos() {
  osc-cli api ReadVms --config_path="${1}" \
    --Filters '{"VmIds": ["'"${2}"'"], "Tags": ["env='"${DIMAIL_ENV}"'"]}' \
    | jq '.Vms[] | {Name: .Tags[] | select(.Key == "Name") | .Value, Env: .Tags[] | select(.Key == "env") | .Value, VmId, State, VmType }'
}

function fetch_vm_state() {
  VM_INFOS=$(fetch_vm_infos "${osc_config_file}" "${VM_ID}")
  echo  "${VM_INFOS}" | jq --raw-output '.State'
}

function wait_for_vm_state() {
  VM_STATE=$(fetch_vm_state "${osc_config_file}" "${VM_ID}")
  while [[ "${VM_STATE}" != "${3}" ]]; do
    sleep 5
    VM_STATE=$(fetch_vm_state "${osc_config_file}" "${VM_ID}")
    log "état de la vm ${2} : ${VM_STATE}"
  done
}

function start_vm() {
  VM_STATE=$(fetch_vm_state "${osc_config_file}" "${VM_ID}")
  if [[ "${VM_STATE}" != "running" ]]; then
    log "démarrage de la vm : ${VM_ID}"
    continuer
    osc-cli api StartVms --config_path="${1}" --VmIds '["'"${2}"'"]'
    wait_for_vm_state ${1} ${2} running
  fi
}

function stop_vm() {
  VM_STATE=$(fetch_vm_state "${osc_config_file}" "${VM_ID}")
  if [[ "${VM_STATE}" != "stopped" ]]; then
    log "arrêt de la vm : ${VM_ID}"
    continuer
    osc-cli api StopVms --config_path="${1}" --VmIds '["'"${2}"'"]'
    wait_for_vm_state "${1}" "${2}" stopped
  fi
}

function modify_volume() {
  local vol_type="${3}"
  local vol_iops="${4}"
  local vol_size="${5}"
  VOLUME_INFOS=$(fetch_volumes_infos "${osc_config_file}" "${ARG_VOLUME_NAME}")
  VOLUME_ID=$(echo  ${VOLUME_INFOS} | jq --raw-output '.VolumeId')
  log "le volume '"${VOLUME_ID}"' aura désormais le type '"${vol_type}"', les iops ${vol_iops} et la taille ${vol_size}"
  continuer
  if [ "${vol_type}" = "io1" ]; then
    osc-cli api UpdateVolume --config_path="${1}" \
     --VolumeId "'"${VOLUME_ID}"'" \
     --Size "${vol_size}" \
     --VolumeType "'"${vol_type}"'" \
     --Iops "${vol_iops}"
  else
    osc-cli api UpdateVolume --config_path="${1}" \
     --VolumeId "'"${VOLUME_ID}"'" \
     --VolumeType "'"${vol_type}"'" \
     --Size "${vol_size}"
  fi
}

function sync_tf_state() {
  local vol_id="${1}"
  local vol_size="${2}"
  local vol_type="${3}"
  local vol_iops="${4}"
  # resynchronisation tfstate
  log "Pensez à reporter les modifications dans le fichier terraform décrivant ce volume."
  log " -> volume_size    = ${vol_size}"
  log " -> volume_type    = '${vol_type}'"
  if [ "${vol_type}" = "io1" ]; then
    log " -> iops    = ${vol_iops}"
  fi
}

function configure_osccli() {
  if [ -z "${OUTSCALE_REGION}" ]; then
    fatal "La variable OUTSCALE_REGION est vide, les variables d'environnement ne sont pas configurées"
  fi
  if [ -z "${OUTSCALE_ACCESSKEYID}" ]; then
    fatal "La variable OUTSCALE_ACCESSKEYID est vide, les variables d'environnement ne sont pas configurées"
  fi
  if [ -z "${OUTSCALE_SECRETKEYID}" ]; then
    fatal "La variable OUTSCALE_SECRETKEYID est vide, les variables d'environnement ne sont pas configurées"
  fi

  local config_file
  config_file=$(mktemp)
  envsubst < osccli_config.tptl > "${config_file}"
  echo "${config_file}"
}

function continuer() {
    # Demander à l'utilisateur s'il souhaite continuer
  read -p "Voulez-vous continuer ? (oui) " reponse
  # Convertir la réponse en minuscule
  log "réponse -> ${reponse}"
  if [[ "${reponse}" != "oui" ]]; then
    log "arrêt du script."
    exit 1
  fi
  log "c'est parti ..."
}

main "${@}"
