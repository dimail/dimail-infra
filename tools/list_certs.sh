source common.sh

set -e

FROM=$1

if [ "$1" == "" ]; then
   echo "usage: list_certs.sh plateforme"
   exit 1
fi

HOST=$(ssh_target_for_pf $FROM)

ssh $HOST sudo ls -1 /etc/letsencrypt/live | grep -v README
