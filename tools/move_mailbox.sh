#!/usr/bin/env bash

source common.sh

set -e

me=$0
help() {
	title "Tu es perdu. Voilà de l'aide..."
	echo Syntax 1: $me [-h] [--from pfname_from] [--to pfname_to] [--user username] [--domain domain] [--overwrite] [--debug]
	echo Syntax 2: $me pfname_from pfname_to username domain
	echo "   -h: This help message"
	echo "   [--from|-f]   pfname_from: The source platform"
	echo "   [--to|-t]     pfname_to:   The destination platform"
	echo "   [--user|-u]   username:    The username of the mailbox"
	echo "   [--domain|-d] domain:      The domaine name of the mailbox"
	echo "   [--overwrite]:             Continue an interupted mailbox migration"
	echo "   [--empty]:                 Accept an empty mailbox directory migration"
	echo "   [--debug]:                 Show some debug messages during the migration"
	echo "Will migrate the mailbox username@domain from pfname_from to pfname_to"
	exit 0
}

if [ $# -eq 0 ]; then
	help
fi

TEMP=$(getopt -a -o hf:t:u:d:oeD --long help,from:,to:,user:,domain:,overwrite,empty,Debug,debug,force -n "$me" -- "$@")
if [ $? != 0 ]; then
	help
fi

eval set -- "$TEMP"

OVERWRITE=no
EMPTY=no
DEBUG=no
FORCE=no
while true; do
	case "$1" in
		-h | --help ) help; ;;
		-f | --from ) FROM="$2"; shift 2;;
		-t | --to ) TO="$2"; shift 2 ;;
		-u | --user ) USER="$2"; shift 2 ;;
		-d | --domain ) DOMAIN="$2"; shift 2 ;;
		-o | --overwrite ) OVERWRITE="yes"; shift ;;
		-e | --empty ) EMPTY="yes"; shift ;;
		-D | --Debug | --debug ) DEBUG="yes"; shift ;;
		--force ) FORCE=yes; OVERWRITE=yes; DEBUG=yes; shift ;;
		-- ) shift; break ;;
		* ) break ;;
	esac
done

if [ $# -eq 4 ]; then
	FROM=$1
	TO=$2
	USER=$3
	DOMAIN=$4
	shift 4
fi
if [ $# -ne 0 ]; then
	ko "Il reste les arguments '$*' que je ne comprend pas."
	echo
	help
fi


SSH_FROM=$(ssh_target_for_pf $FROM)
SSH_TO=$(ssh_target_for_pf $TO)
DBPWD_FROM=$(mariadb_rootpwd_for_pf $FROM)
DBPWD_TO=$(mariadb_rootpwd_for_pf $TO)

get_proxy() {
	ssh $SSH_TO mariadb -u root -p\'"$DBPWD_TO"\' dovecot -B -N -e "\"select proxy from users where username='$USER' and domain='$DOMAIN'\""
}

set_proxy() {
	PROXY=$1
	ssh $SSH_TO mariadb -u root -p\'"$DBPWD_TO"\' dovecot -B -N -e "\"update users set proxy = '$PROXY' where username='$USER' and domain='$DOMAIN'\""
}

get_active_on_from() {
        ssh $SSH_FROM mariadb -u root -p\'"$DBPWD_TO"\' dovecot -B -N -e "\"select active from users where username='$USER' and domain='$DOMAIN'\""
}

set_active_on_from() {
        ACTIVE=$1
        ssh $SSH_FROM mariadb -u root -p\'"$DBPWD_TO"\' dovecot -B -N -e "\"update users set active = '$ACTIVE' where username='$USER' and domain='$DOMAIN'\""
}


title Je déplace $USER@$DOMAIN depuis $FROM vers $TO

#
# Étape 1 - Vérifier que la mailbox n'est pas migrée
#
if [ "$FORCE" == "yes" ]; then
	echo Tu as dit de forcer.
	echo ... donc je vais forcer, tout écrabouiller la boîte, même si la migration est déjà faite
	echo ... tu es vraiment certain ?
	read val
	if [ "$val" != "bien entendu" ]; then
		ko "Tu n'es pas assez certain pour moi"
		exit 1
	fi
	set_proxy Y
fi

step "1. Je vérifie que la mailbox n'est pas migrée"
echo Je regarde la colonne proxy sur $TO pour ce compte
PROXY=$(get_proxy)
case "$PROXY" in
	"Y")
		ok "Ca dit '$PROXY', donc ce n'est pas encore migré, tout va bien."
		;;
	"N")
		ko "Ca dit '$PROXY', donc c'est déjà migré"
		exit 1
		;;
	"W")
		if [ "$OVERWRITE" = "yes" ]; then
			ok "C'est déjà en cours de migration (proxy='$PROXY')."
			note "Tu as dit --overwrite, donc on reprend une migration interrompue."
		else
			ko "Ca dit '$PROXY', donc c'est en cours de migration... Caisstufou ?"
			ko "Si tu veux reprendre en écrasant, ajoute --overwrite"
			exit 1
		fi
		;;
	*)
		ko "Je trouve proxy='$PROXY', ça n'a aucun sens."
		ko "Ca ne marche pas. Mourrons dignement."
		exit 1
		;;
esac

U=`echo $USER | cut -b 1-2`
DIR="imap_mailboxes/$DOMAIN/$U"
BOX="$DIR/$USER"
debug "La mailbox est dans le répertoire $BOX"

if ! ssh $SSH_FROM test -d /var/mail/$BOX; then
	if [ "$EMPTY" != "yes" ]; then
		ko "Je ne trouve pas le répertoire sur la plateforme de départ. C'est probablement très mauvais."
		ko "Si tu as bien vérifié que la mailbox est vide pour de vrai, alors tu peux utiliser --empty pour accepter de migrer en l'état."
		exit 1
	else
		note "Je ne trouve aucun répertoire sur la plateforme d'arrivée, mais tu m'as dit que c'était normal."
		step "Je transforme cette mailbox vide en mailbox vide migrée."
		set_proxy N
		PROXY=`get_proxy`
		if [ "$PROXY" != 'N' ]; then
			ko "J'ai écrit 'N' dans la colonne proxy, mais je trouve la valeur '$PROXY' à la place. C'est triste, j'avais tout fini..."
			exit 1
		fi
		ok "Migration terminée pour $USER@$DOMAIN"
		exit 0
	fi
else
	ok "Le répertoire existe sur la plateforme de départ, c'est bien."
fi

if ssh $SSH_TO test -d /var/mail/$BOX; then
	if [ "$OVERWRITE" != "yes" ]; then
		ko "Je trouve déjà une trace du répertoire sur la plateforme d'arrivée."
		ko "Tu peux ajouter --overwrite sur la ligne de commande pour forcer."
		exit 1
	else
		note "Je trouve déjà le répertoire sur la plateforme d'arrivée, mais on écrase."
	fi
else
	ok "Le répertoire n'existe pas sur la plateforme d'arrivée, c'est bien."
fi

#
# Étape 2 - Première copie via rsync
# 

step "2. Je fais une copie de la mailbox via rsync"

debug sudo rsync -av --delete $SSH_FROM::mail/$BOX /var/mail/$DIR
ssh $SSH_TO sudo rsync -av --delete $SSH_FROM::mail/$BOX /var/mail/$DIR

ok "La première copie est terminée"

#
# Étape 3 - On bloque l'accès au compte
#

step "3. Je passe le compte en W pour l'empêcher de se connecter"
set_proxy W
PROXY=`get_proxy`
if [ "$PROXY" != 'W' ]; then
	ko "J'ai écrit 'W' dans la colonne proxy, mais je trouve la valeur '$PROXY' à la place. J'arrête."
	exit 1
fi

#
# Étape 4 - On ferme toutes les sessions de l'utilisateur
#

conn() {
	ssh $SSH_TO sudo doveadm who -1 "$USER@$DOMAIN"
}

step "4. Je shoot toutes les connexions de l'utilisateur"
step "Je commence par lister les connexions actives"
CONN=$(conn)
COUNT=$(($(echo "$CONN"|wc -l)-1))

if [ $COUNT -eq 0 ]; then
	ok "Aucune connexion détectée, je continue"
else
	echo "$CONN"
	step "$COUNT connexion(s) détectée(s), je kick"
	ssh $SSH_TO sudo doveadm kick "$USER@$DOMAIN"
	step "Je vérifie que les connexions n'ont pas repoussé"
	CONN=$(conn)
	COUNT=$(($(echo "$CONN"|wc -l)-1))
	if [ $COUNT -ne 0 ]; then
		ko "Les connexions ne se ferment pas, il est préférable d'en rester là"
		exit 1
	else
		ok "Tout va bien, les connexions sont interrompues, je continue"
	fi
fi

#
# Étape 5 - Deuxième rsync
#

step "5. Deuxième rsync"
ssh $SSH_TO sudo rsync -av --delete $SSH_FROM::mail/$BOX /var/mail/$DIR

ok Le deuxième rsync est terminé.

#
# Étape 6 - On ré-ouvre le service
#

step "6. Je passe proxy à N pour dire que la migration de la boîte est terminée."

set_proxy N
PROXY=`get_proxy`
if [ "$PROXY" != 'N' ]; then
	ko "J'ai écrit 'N' dans la colonne proxy, mais je trouve la valeur '$PROXY' à la place. C'est triste, j'avais tout fini..."
	exit 1
fi

step "7. Je passe active à N sur FROM pour que les éventuels mails restants soient relayés"
set_active_on_from N
ACTIVE=`get_active_on_from`
if [ "$ACTIVE" != 'N' ]; then
	ko "J'ai écrit 'N' dans la colonne active, mais je trouve la valeur '$ACTIVE' à la place. C'est triste, j'avais tout fini..."
        exit 1
fi

ok "Migration terminée pour $USER@$DOMAIN"
