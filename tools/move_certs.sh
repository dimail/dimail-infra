#!/bin/bash
source common.sh

set -e

if [ "$#" -lt 3 ]; then
  echo usage: ./move_cert.sh origine destination cert_name
fi

FROM=$1
TO=$2
CERT=$3

SSH_FROM=$(ssh_target_for_pf $FROM)
SSH_TO=$(ssh_target_for_pf $TO)
DIR=$(mktemp -d)

echo Je prend les fichiers sur $SSH_FROM
ssh $SSH_FROM "sudo tar cvf - /etc/letsencrypt/live/$CERT /etc/letsencrypt/archive/$CERT /etc/letsencrypt/renewal/$CERT.conf" > $DIR/certs.tar
echo Je range ça dans $DIR
tar tvf $DIR/certs.tar
echo Je les pousse sur $SSH_TO
cat $DIR/certs.tar | ssh $SSH_TO "cd /; sudo tar xvf -"
echo "Je peux purger le fichier d'archive"
rm $DIR/certs.tar
echo Je retire mon repertoire temporaire
rmdir $DIR
