source common.sh

set -e

pf=$1

host_monitor=$(ssh_target_for_pf $pf monitor)
host_backup=$(ssh_target_for_pf $pf backup)
host_main=$(ssh_target_for_pf $pf main)

for host in $host_monitor $host_backup; do
  echo "je supprime /etc/nginx/letsencrypt/cert-proxy.conf sur ${host}"
  ssh $host sudo rm -v /etc/nginx/letsencrypt/cert-proxy.conf 
done
