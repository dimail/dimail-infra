help_ovh:
	@echo "  plan_backend      - Plan the changes to the backend in the OVH environment."
	@echo "  plan_network      - Plan the changes to the network in the OVH environment."
	@echo "  plan_vms          - Plan the changes to the VMs in the OVH environment."
	@echo "  apply_backend     - Apply the changes to the backend in the OVH environment."
	@echo "  apply_network     - Apply the changes to the network in the OVH environment."
	@echo "  apply_vms         - Apply the changes to the VMs in the OVH environment."

init_ovh: 
	terraform -chdir=./10_backend_ovh init -backend-config=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/backend.tfbackend -reconfigure
	terraform -chdir=./20_network init -backend-config=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/backend.tfbackend -reconfigure
	terraform -chdir=./30_vms init -backend-config=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/backend.tfbackend -reconfigure


init-upgrade_ovh: 
	terraform -chdir=./10_backend_ovh init -backend-config=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/backend.tfbackend -reconfigure -upgrade
	terraform -chdir=./20_network init -backend-config=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/backend.tfbackend -reconfigure -upgrade
	terraform -chdir=./30_vms init -backend-config=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/backend.tfbackend -reconfigure -upgrade

plan_ovh: plan_backend plan_network plan_vms

plan_backend: 
	terraform -chdir=./10_backend_ovh plan -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/10_backend.tfvars

plan_network: 
	terraform -chdir=./20_network plan -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/20_network.tfvars

plan_vms: 
	terraform -chdir=./30_vms plan -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/30_vms.tfvars

apply_ovh: apply_backend apply_network apply_vms

apply_backend: check
	terraform -chdir=./10_backend_ovh apply -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/10_backend.tfvars

apply_network: check
	terraform -chdir=./20_network apply -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/20_network.tfvars

apply_vms: check
	terraform -chdir=./30_vms apply -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/30_vms.tfvars

beauty_ovh: beauty_backend beauty_network beauty_vms

beauty_backend: 
	terraform fmt ./10_backend_ovh
	terraform fmt $(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/10_backend.tfvars
	tflint --chdir=./10_backend_ovh --format=compact --var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/10_backend.tfvars

beauty_network: 
	terraform fmt ./20_network
	terraform fmt $(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/20_network.tfvars
	tflint --chdir=./20_network --format=compact --var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/20_network.tfvars

beauty_vms: 
	terraform fmt ./30_vms
	terraform fmt $(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/30_vms.tfvars
	tflint --chdir=./30_vms --format=compact --var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/30_vms.tfvars

destroy_ovh: 
	terraform -chdir=./30_vms destroy -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/30_vms.tfvars
