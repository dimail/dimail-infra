help_outscale:
	@echo ""

init_outscale:
	terraform -chdir=./30_outscale init -backend-config=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/backend.tfbackend -reconfigure


init-upgrade_outscale:
	terraform -chdir=./30_outscale init -backend-config=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/backend.tfbackend -reconfigure -upgrade

plan_outscale:
	terraform -chdir=./30_outscale plan -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/outscale.tfvars

apply_outscale:
	terraform -chdir=./30_outscale apply -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/outscale.tfvars

destroy_outscale:
	terraform -chdir=./30_outscale destroy -var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/outscale.tfvars

#
# TF LINT
#
beauty_outscale:
	terraform fmt ./30_outscale
	terraform fmt $(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/outscale.tfvars
	tflint --chdir=./30_outscale --var-file=$(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV)/outscale.tfvars

