variable "env_name" {
  description = "Name of the current environment"
  type        = string
}
variable "main_region" {
  description = "Region of the project"
  type        = string
}

variable "ovh_openstack_tenant_name" {
  description = "Name of the project"
  type        = string
}

variable "ovh_openstack_tenant_id" {
  description = "ID of the project"
  type        = string
}

variable "ovh_public_cloud_project_name" {
  description = "Name of the public cloud project"
  type        = string
}

variable "ovh_public_cloud_project_id" {
  description = "ID of the public cloud project"
  type        = string
}

variable "s3_endpoint" {
  description = "endpoint of s3"
  type        = string
}

variable "bucket_name" {
  description = "name of the bucket"
  type        = string
}

variable "admin_docs_bucket_name" {
  description = "name of the admin docs bucket"
  type        = string
  nullable    = true
}

variable "users_docs_bucket_name" {
  description = "name of the users docs bucket"
  type        = string
  nullable    = true
}
