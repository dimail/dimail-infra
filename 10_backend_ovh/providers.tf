terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 2.1.0"
    }

    ovh = {
      source  = "ovh/ovh"
      version = "~> 0.51.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.5"
    }
  }


  backend "s3" {
    key = "15_backend/terraform.tfstate"
  }
}

# Configure le fournisseur OpenStack hébergé par OVHcloud
provider "openstack" {
  auth_url    = "https://auth.cloud.ovh.net/v3/" # URL d'authentification
  domain_name = "default"                        # Nom de domaine - Toujours à "default" pour OVHcloud
  # alias       = "ovh"                            # Un alias
  region = var.main_region # Région du projet

  tenant_name = var.ovh_openstack_tenant_name # Nom du projet
  tenant_id   = var.ovh_openstack_tenant_id   # ID du projet

  # Il faut configurer les variables d'environnement suivantes :
  # OS_USERNAME=
  # OS_PASSWORD=
}

provider "ovh" {
  # alias    = "ovh"
  endpoint = "ovh-eu"
  # Il faut configurer les variables d'environnement suivantes :
  # OVH_APPLICATION_KEY=
  # OVH_APPLICATION_SECRET=
  # OVH_CONSUMER_KEY=
}

provider "aws" {
  region = "gra"

  # OVH implementation has no STS service
  skip_credentials_validation = true
  skip_requesting_account_id  = true
  # the "gra" region is unknown to AWS hence skipping is needed.
  skip_region_validation = true
  endpoints {
    s3 = var.s3_endpoint
  }

  # Il faut configurer les variables d'environnement suivantes :
  # AWS_ACCESS_KEY_ID=
  # AWS_SECRET_ACCESS_KEY=
}
