module "docs_users" {
  source = "./modules/bucket_website"

  # only in prod
  for_each = toset(contains(["prod"], var.env_name) ? [var.env_name] : [])

  bucket_name = var.users_docs_bucket_name
}

moved {
  from = aws_s3_bucket.docs_users["prod"]
  to   = module.docs_users["prod"].aws_s3_bucket.website
}

moved {
  from = aws_s3_bucket_acl.docs_users["prod"]
  to   = module.docs_users["prod"].aws_s3_bucket_acl.website
}

moved {
  from = aws_s3_bucket_website_configuration.docs_users["prod"]
  to   = module.docs_users["prod"].aws_s3_bucket_website_configuration.website
}
