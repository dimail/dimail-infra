module "docs_admin" {
  source = "./modules/bucket_website"

  # only in prod
  for_each = toset(contains(["prod"], var.env_name) ? [var.env_name] : [])

  bucket_name = var.admin_docs_bucket_name
}

moved {
  from = aws_s3_bucket.docs_admin["prod"]
  to   = module.docs_admin["prod"].aws_s3_bucket.website
}

moved {
  from = aws_s3_bucket_acl.docs_admin["prod"]
  to   = module.docs_admin["prod"].aws_s3_bucket_acl.website
}

moved {
  from = aws_s3_bucket_website_configuration.docs_admin["prod"]
  to   = module.docs_admin["prod"].aws_s3_bucket_website_configuration.website
}
