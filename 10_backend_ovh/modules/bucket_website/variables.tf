variable "bucket_name" {
  description = "Name of the OVH S3 bucket that will be expose as a website"
  type        = string
}
