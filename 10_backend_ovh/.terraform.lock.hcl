# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "5.77.0"
  constraints = "~> 5.5"
  hashes = [
    "h1:1Bc6wmBW77rMZLIJ9sgqmmlwt+J7Slz1CTToLgpn5VE=",
    "h1:34ygrZFyB39KOd7jHpoEt1JvdHZxg93aKgT0m7zL+Kw=",
    "h1:7yv9NDANq8B0hKcxySR053tYoG8rKHC2EobEEXjUdDg=",
    "h1:9lQ7FpLAN1+Bt1eb/daWlyl2N9aWsBTw2tgLR/hxODE=",
    "h1:9tmqgF8scEl1MmBrNysovnEvxiZzpUfCBCnJldwzhVI=",
    "h1:FvD/IrL0iBhgdSzBZfVK30uZDEIbNdCfmDDJdyKfJ2E=",
    "h1:KUQxsXAQm4s4jAAJceu4ZUMy/DVmmLqAljO2D/JyKPk=",
    "h1:MQfEyNQ4Fzeqe0/Ay9lVqkeZSOpkokgxn/S4LTKKNC4=",
    "h1:NHBqOkWesBo4iIi19E9p4/BOW7GnDCos4KbApas5bZc=",
    "h1:UrcybYxy8D2lphT2FMTNYRQIKDZNWROgmvDnhI3xvkk=",
    "h1:Xq5YNuJtQpaJPkzK+M1Hvhx5h8zzSJMnsy5KRtxTgTI=",
    "h1:j60RglkZWyThGApFmIysI5ap5+kZMBYbxm8IizKXAGU=",
    "h1:koBS6VTK/a+wgxjhKW0EOTwUXquG77bGQpqSP82a9ts=",
    "h1:xs9dxelN9MFSeqI/nwASuTKwZbrqvSzIqM4gXvgvr8U=",
    "zh:0bb61ed8a86a231e466ceffd010cb446418483853aa7e35ecb628cf578fa3905",
    "zh:15d37511e55db46a50e703195858b816b7bbfd7bd6d193abf45aec1cb31cfc29",
    "zh:1cdaec2ca4408e90aee6ea550ff4ff01a46033854c26d71309541975aa6317bd",
    "zh:1dd2d1af44004b35a1597e82f9aa9d6396a77808371aa4dfd2045a2a144b7329",
    "zh:329bf790ef57b29b95eee847090bffb74751b2b5e5a4c23e07367cc0bf9cce10",
    "zh:40949e13342a0a738036e66420b7a546bda91ef68038981badbe454545076f16",
    "zh:5674eb93c8edd308abac408ae45ee90e59e171d45011f00f5036ff4d43a1de52",
    "zh:747624ce0e938dd773bca295df226d39d425d3805e6afe50248159d0f2ec6d3a",
    "zh:761795909c5cba10f138d276384fb034031eb1e8c5cdfe3b93794c8a78d909ce",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9b95901dae3f2c7eea870d57940117ef5391676689efc565351bb087816674e4",
    "zh:9bb86e159828dedc1302844d29ee6d79d6fee732c830a36838c359b9319ab304",
    "zh:9e72dfbd7c28da259d51af92c21e580efd0045103cba2bb01cd1a8acb4185883",
    "zh:a226b88521022598d1be8361b4f2976834d305ff58c8ea9b9a12c82f9a23f2c2",
    "zh:faabcdfa36365359dca214da534cfb2fd5738edb40786c2afd09702f42ad1651",
  ]
}

provider "registry.terraform.io/ovh/ovh" {
  version     = "0.51.0"
  constraints = "~> 0.51.0"
  hashes = [
    "h1:/RwSmGxkUoMeEkX7UqyC/JYA5Fioqt7A5Qvq1NrDgTQ=",
    "h1:7dOFLaLlkGzi+xsxSXV/CzmZP7n1kk/Fi3kiUuqwXAI=",
    "h1:DUVoJDMeVHXRkZIH7KaVGb/LAnI6pYaJsPd28Xa6Smg=",
    "h1:DepnYVFcpMFYViDeU3wvEqjNYSngQ1bbGQXLT6Py9Mw=",
    "h1:F+zEYn8hO9uHgBw5VkNUznHBQuC90pLP7xT3Z7yjkDw=",
    "h1:Q/S8VXeVgTj1KB98HNo0gabAJBxgmVxRLO0jSKFMO4E=",
    "h1:S7r5AFUQ8kl5MgsRNwFekD4qrnYnwVRcdSTeewp3T8Y=",
    "h1:WM9PK2XT3/6RxEADkPOPGCL6by7CO1PawcSLzlwO4HE=",
    "h1:hSJH2YseGHYVoGSrRgdBqwwz6oZVGssnbJ2hjtS/Gqs=",
    "h1:mfUEYqr5G9UW/cobcRneQhUFvJvGNjcZHnHCdF2fAlA=",
    "h1:nj9ug1tGCA4TMj1wYSZc+W8Qyt/DXQLhCbdtWmcc1Xo=",
    "h1:oFnnUFkxcAx3MoJ7uumnudwzVaNP033bG2O9Lh0LbiY=",
    "h1:pzwHQ4/iiNeMbvqeT+Mo8N3C5yiLMobVAP9xAbSiI6g=",
    "h1:qKVyUEhgej/ccqQy1+z+SSjreAYAmFaIRx5ApKweLz0=",
    "zh:13c2e4ea4a0f8348f7556862df5ff17733d06cf898c2a646f0c235e4fa87d210",
    "zh:52db2b0371ffaf52cfdacbf8fa6e931c35b5b2c8a65e8f3cb70155f33beda85f",
    "zh:5824a22dd83699f78a4d7689eb905f895999491debb730ba2d15e0f0082c66b0",
    "zh:67426b082025ccf08685f757cca94fc34b8b930e97d17d896b7a4da38ccad00d",
    "zh:6e7b2a62118c969acef1c867dd97db880398f44243145cd5feb2e1935696d0c9",
    "zh:84248f5e3b53fdd175c5c232fcb5b4a6e59f4d33a161b90d70f38bccae6e2823",
    "zh:937c3d56f32e7aaa2fe05d58fc54ff8c348a8ab3718462dc2ea1f9a9ba739bd9",
    "zh:9630cf1e8c65efb7e7a9ccab3a152c03f002ba47a8ce058bd98be34419137746",
    "zh:b7d51f82ea1f73dbdabdd82d346015e9c1cde0dd334ed49150b35ca1843ba0b7",
    "zh:bd7771ff7da38f6a7772cf774a573a184010a01de170d8bb30740c7d3d45ddd3",
    "zh:c4bffcfc33b82765003e8f9ce22b3bc100ec28c2380eacb5a38a272c6bd2636e",
    "zh:cd08c5165ed1366d2c84b8e01005b7e42cceb3b4229d3cf31ea9da910029fe15",
    "zh:ea2424e1f8848da2283db82e93b5c8fb5f44170a0e62ae991a935d4aa38ce4b0",
    "zh:f9bf537f8be3626858d944b25995adfa513f9fc9f73003a7ce0461dbec0e78b4",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "2.1.0"
  constraints = "~> 2.1.0"
  hashes = [
    "h1:2TcmfEzBOGQPALErrXTaL6v+k/WAL40adao4izRYmdw=",
    "h1:MGK7NYaE2Gt1sTueR4fO59P2jtpp0oARNWfNS66JL9k=",
    "zh:113661750398bf21c8fe36aade9fb6f5eb82b5bcd3bcd30bd37ac805d83398f4",
    "zh:1b3c26347b9cd61e413ee93c2f422cc3278a77f55fd3516eaabb3e2a85f65281",
    "zh:1b751bbf1e4152829a643b532fd3f5967a2e89a41fac381257e0b41665be3306",
    "zh:1b967bbfd9b344419c0e0df0c3a15fcbd731e91f19a18955a55aace8d9ec039a",
    "zh:1bc0fc7c0a21e568db043b654501ce668ba19bf7628d37a7d2aaa512fd6e5aeb",
    "zh:425cbf61757d4b503e7bf0f409ea59835ca3afbd2432d56ad552c2e5d234a572",
    "zh:67d4f059cb4d73bf6c060313ec32962c4e5bd8dc7be2542a6f2098ab32575cd9",
    "zh:7fe841ac5b68a4f52fb3cf45070828f3845de44746679d434e4349f3c23e3ef2",
    "zh:ac1ed4c6ef0b6a3410568a05d3f9933d184497f065988503c43da0b2f0786ab2",
    "zh:c5c0d14c86fabd9ab6a5d555e6a8d511942665fb5fa948dd452b0d1934068344",
    "zh:c9ae5c210192275185d6823566a9421983e8e64c2665a4cae00b92dd0706bd19",
    "zh:ee9865ccc053e7f345e532654fb628d1cf1e81cd2e929643c1691bebffcf7b98",
    "zh:f3416d2f666095e740522c4964e436470bb9ec17bd53aaae8169ad93297d07bd",
    "zh:fbca85457dd49e17168989d64f7cfc4a519d55ef4e00e89cea2859e87ad87f83",
  ]
}
