## Gestion des alias :

Les alias se trouvent dans  /etc/postfix/virtual_aliases
Faire les modifs nécessaires dans ce fichier puis :

postmap hash:/etc/postfix/virtual_aliases
service postfix reload

## Création d'un utilisateur :

Dans `/home/debian`, retrouver le script createuserscript.sh

example : ```./createuserscript.sh -c $contextid -p $password -u $username -d $displayname" -g $givenname -s $surname -D $domain```

- ```-c``` &rarr; Dans openxchange, un utilisateur se crée au sein d'un contexte. Un contexte est un espace virtuel dans lequel se trouve des utilisateurs, cet espace peut-être modulé pour autoriser/interdire des fonctionnalités pour tous les utilisateurs en son sein. Un contexte est aussi associé à un domaine mail.

Dans notre cas il existe aujourd'hui 3 contexte associés chacun à un id :

1 &rarr; le contexte par défault, créé lors de l'installation de l'application

2 &rarr; le contexte des utilisateurs data.gouv.fr

3 &rarr; le contexte de l'utilisateur système

- ```-p``` &rarr; Le mot de passe à fournir à l'utilisateur (fournir un mot de passe sans caractères spéciaux. L'utilitaire ne fonctionne pas pour le moment avec les caractères spéciaux - puis demander à l'utilisateur de changer son mot de passe).
- ```-u``` &rarr; Le username de l'utilisateur, plus précisément la partie avant "@" du mail de l'utilisateur, par exemple ```jean.dupont```.
- ```-d``` &rarr; Le displayname de l'utilisateur, par exemple ```"Jean Dupont```, ne pas oubliez les ```"``` sinon le script considérera 2 arguments. OpenXchange n'accepte pas les homonymes pour le moment, le displayname doit être unique.
- ```-g``` &rarr; Le givenname de l'utilisateur (le prénom), si composé, mettre des ```"```.
- ```-s``` &rarr; Le surname de l'utilisateur (le nom de famille), si composé, mettre des ```"```.
- ```-D``` &rarr; Le domaine associé à l'utilisateur, pour l'instant ```data.gouv.fr```.

## Suppression d'un utilisateur

Dans `/home/debian`, retrouver le script deleteuserscript.sh

example : ```./deleteuserscript.sh -c $contextid -u $username```

- ```-c``` le contexte associé à l'utilisateur à supprimer

1 &rarr; le contexte par défault, créé lors de l'installation de l'application 

2 &rarr; le contexte des utilisateurs data.gouv.fr

3 &rarr; le contexte de l'utilisateur système

- ```-u``` le username de l'utilisateur à supprimer.

## Lister les utilisateurs d'un contexte

```.\listuser -A oxadmin -P ox_admin -c $contextid``` 

## Lister les contextes

```listcontext -A $masterusername -P $masterpassword```

## Taille maximale des mails

La taille maximale d'un mail ou de ce qu'il s'y trouve est défini par des propriétés qui peuvent venir de ```postfix```, ```nginx``` ou ```open-xchange```.
La liste suivante est non exhaustive (à alimenter si on trouve d'autres paramètres importants):


- ```message_size_limit``` dans ```etc/postfix/main.cf``` (celui-ci a été positionné à 20000000, soit ~20 MB)  &rarr; Il s'agit de la taille maximale d'un mail autorisé par postfix.
- ```client_max_body_size``` positionné ~15 MB; dans ```/etc/nginx/sites-available/ox_nginx.conf``` &rarr; Il s'agit de la taille de la requête qui passe par Nginx, elle est à graduer également en fonction de la taille maximale d'un mail.
- ```com.openexchange.mail.maxMailSize``` dans ```/opt/open-xchange/etc/mail.properties``` &rarr; Il s'agit de la taille maximale d'un mail autorisé par open-xchange (il est paramétré par défaut à ```-1``` c'est-à-dire, infini.)
- ```com.openexchange.mail.signature.maxImageSize``` &rarr; La taille maximale d'une signature dans un mail (1 MB par défaut)
- ```com.openexchange.mail.signature.maxImageLimit``` &rarr; Le nombre d'image maximum autorisé dans un mail (3 par défaut)
