
help:
	@echo "Makefile Usage:"
	@echo ""
	@echo "Variables:"
	@echo "  DIMAIL_ENV         - The environment (e.g., ovhprod, ovhdev, osdev, osdev2, osprod)."
	@echo "  DIMAIL_CONFIG_PATH - The configuration path for the environment."
	@echo "  DIMAIL_ENVTYPE     - The type of environment (e.g., prod)."
	@echo "  DIMAIL_HOSTING     - The hosting type."
	@echo ""
	@echo "Targets:"
	@echo "  help          - Display this help message."
	@echo "  init          - Initialize the environment based on the hosting type."
	@echo "  init-upgrade  - Initialize and upgrade the environment based on the hosting type."
	@echo "  plan          - Plan the changes for the environment based on the hosting type."
	@echo "  apply         - Apply the changes to the environment based on the hosting type."
	@echo "  destroy       - Destroy the environment based on the hosting type."
	@echo "  beauty        - Beautify the environment based on the hosting type."
	@echo "  test          - Run tests for the environment."
	@$(MAKE) --no-print-directory help_$(DIMAIL_HOSTING)


default: 
	@echo "Usage: make <init|plan|apply|destroy|beauty|test>"

init: check init_$(DIMAIL_HOSTING)

init-upgrade: check init_$(DIMAIL_HOSTING)

plan: check plan_$(DIMAIL_HOSTING)

apply: check apply_$(DIMAIL_HOSTING)

destroy: check destroy_$(DIMAIL_HOSTING)

beauty: beauty_$(DIMAIL_HOSTING)

test: python_test

python_test:
	cd 40_ansible/filter_plugins/ && ./run_tests.sh

check:
#	@if [ "$$DIMAIL_ENV" != "ovhprod" -a "$$DIMAIL_ENV" != "ovhdev" -a "$$DIMAIL_ENV" != "osdev" -a "$$DIMAIL_ENV" != "osdev2" -a "$$DIMAIL_ENV" != "osprod" ]; then echo Tu te moques DIMAIL_ENV=$$DIMAIL_ENV n a aucun sens.; exit 1; fi
	@echo $$DIMAIL_CONFIG_PATH - $$DIMAIL_ENV
	@if [ ! -d $(DIMAIL_CONFIG_PATH)/env-$(DIMAIL_ENV) ]; then echo Il manque le repertoire env-$$DIMAIL_ENV pour ton environnement...; exit 1; fi
	@echo Environnement : $$DIMAIL_ENV.
	@echo Env. type: $$DIMAIL_ENVTYPE.
	@echo Hoster: $$DIMAIL_HOSTING.
	@if [ "$$DIMAIL_ENVTYPE" = "prod" ]; then echo "Prod, waiting 5 seconds, please think about what you're doing..."; sleep 5; echo "Go."; fi

-include ./makefiles/$(DIMAIL_HOSTING).mk
